//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AspNetIdentity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class PAY_TRANSACTION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAY_TRANSACTION()
        {
            this.PAY_BATCH_TRANSACTION = new HashSet<PAY_BATCH_TRANSACTION>();
            this.PAY_TRANSACTION_CANCEL_REASON = new HashSet<PAY_TRANSACTION_CANCEL_REASON>();
            this.PAY_TRANSACTION_DETAIL = new HashSet<PAY_TRANSACTION_DETAIL>();
            this.PAY_TRANSACTION_NOTES = new HashSet<PAY_TRANSACTION_NOTES>();
        }
    
        public int ID { get; set; }
        public string UUID { get; set; }
        public string PREFIX { get; set; }
        public Nullable<int> CMN_INSTITUTE_ID { get; set; }
        public string USER_ID { get; set; }
        public Nullable<int> MAS_COUNTRY_ID { get; set; }
        public Nullable<int> CMN_CURRENCY_ID { get; set; }
        public Nullable<int> CMN_PAYMENT_METHOD { get; set; }
        public Nullable<double> AMOUNT_PAYING { get; set; }
        public Nullable<double> MARKET_RATE { get; set; }
        public Nullable<double> EXCHANGE_RATE { get; set; }
        public Nullable<double> AMOUNT_FX { get; set; }
        public string STATUS { get; set; }
        public Nullable<int> MAS_BANK_ID { get; set; }
        public Nullable<int> MAS_OWNER_ID { get; set; }
        public Nullable<System.DateTime> EXPIRY_DATE { get; set; }
        public Nullable<System.DateTime> PAYMENT_SENT_DATE { get; set; }
        public Nullable<bool> NEED_DAYS { get; set; }
        public string SUB_STATUS { get; set; }
        public Nullable<System.DateTime> POST_DATE { get; set; }
        public Nullable<System.DateTime> RECEIVED_DATE { get; set; }
        public Nullable<bool> IS_RATE { get; set; }
        public Nullable<bool> IS_FX_OFFICE { get; set; }
        public string OWNER_MTFX { get; set; }
        public string COUNTER_NUMBER { get; set; }
        public string JSON { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public string UPDATED_BY { get; set; }
        public Nullable<bool> IS_DELETED { get; set; }
        public Nullable<bool> IS_APPROVED { get; set; }
        public Nullable<int> VERSION_NO { get; set; }

        [ForeignKey("CMN_CURRENCY_ID")]
        public virtual CMN_CURRENCY CMN_CURRENCY { get; set; }
        [ForeignKey("CMN_INSTITUTE_ID")]
        public virtual CMN_INSTITUTE CMN_INSTITUTE { get; set; }
        [ForeignKey("CMN_PAYMENT_METHOD")]
        public virtual CMN_PAYMENT_METHOD CMN_PAYMENT_METHOD1 { get; set; }
        [ForeignKey("MAS_COUNTRY_ID")]
        public virtual MAS_COUNTRY MAS_COUNTRY { get; set; }
        [ForeignKey("MAS_OWNER_ID")]
        public virtual MAS_OWNER MAS_OWNER { get; set; }
        [ForeignKey("MAS_BANK_ID")]
        public virtual PAY_BANK_DETAILS PAY_BANK_DETAILS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAY_BATCH_TRANSACTION> PAY_BATCH_TRANSACTION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAY_TRANSACTION_CANCEL_REASON> PAY_TRANSACTION_CANCEL_REASON { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAY_TRANSACTION_DETAIL> PAY_TRANSACTION_DETAIL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAY_TRANSACTION_NOTES> PAY_TRANSACTION_NOTES { get; set; }
    }
}
