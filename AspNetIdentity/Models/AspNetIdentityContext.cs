﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspNetIdentity.Models
{
    public class AspNetIdentityContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public AspNetIdentityContext() : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_STATE> MAS_STATE { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_CITY> MAS_CITY { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_BATCH> PAY_BATCH { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_BATCH_TRANSACTION> PAY_BATCH_TRANSACTION { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_INSTITUE_PAYMENT_METHODS> CMN_INSTITUE_PAYMENT_METHODS { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_INSTITUTE> CMN_INSTITUTE { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_PAYMENT_METHOD> CMN_PAYMENT_METHOD { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_COUNTRY> MAS_COUNTRY { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_BANK_DETAILS> PAY_BANK_DETAILS { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_OWNER> MAS_OWNER { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_STUDENT> MAS_STUDENT { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_TRANSACTION_CANCEL_REASON> PAY_TRANSACTION_CANCEL_REASON { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.EMAIL_QUEUE> EMAIL_QUEUE { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.RESET_QUEUE> RESET_QUEUE { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.FAQ_CATEGORY> FAQ_CATEGORY { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.FAQ_QUESTION> FAQ_QUESTION { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.FAQ_ANSWER> FAQ_ANSWER { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_FEEDBACK> MAS_FEEDBACK { get; set; }
        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_SAFE> PAY_SAFE { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_PROGRAM_OF_STUDY> MAS_PROGRAM_OF_STUDY { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_LABELS> MAS_LABELS { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.TRANSACTION_RESPONSE> TRANSACTION_RESPONSE { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.BCK_RATES> BCK_RATES { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_SETTINGS> MAS_SETTINGS { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_STATE_CODES> MAS_STATE_CODES { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_TRANSACTION_NOTES> PAY_TRANSACTION_NOTES { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_TRANSACTION> PAY_TRANSACTION { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.AspNetUser_NOTES> AspNetUser_NOTES { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_LANGUAGE> MAS_LANGUAGE { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_PAYMENT_METHOD_TRANSLATIONS> CMN_PAYMENT_METHOD_TRANSLATIONS { get; set; }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_BANK_DETAILS_TRANSLATIONS> PAY_BANK_DETAILS_TRANSLATIONS { get; set; }
    }
}
