//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AspNetIdentity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class PAY_BATCH
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAY_BATCH()
        {
            this.PAY_BATCH_TRANSACTION = new HashSet<PAY_BATCH_TRANSACTION>();
        }
    
        public int ID { get; set; }
        public Nullable<int> CMN_INSTITUE_ID { get; set; }
        public string REF_NO { get; set; }
        public string STATUS { get; set; }
        public string SUB_STATUS { get; set; }
        public Nullable<System.DateTime> SETTLEMENT_DATE { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public string UPDATED_BY { get; set; }
        public Nullable<bool> IS_DELETED { get; set; }
        public Nullable<bool> IS_APPROVED { get; set; }
        public Nullable<int> VERSION_NO { get; set; }

        [ForeignKey("CMN_INSTITUE_ID")]
        public virtual CMN_INSTITUTE CMN_INSTITUTE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAY_BATCH_TRANSACTION> PAY_BATCH_TRANSACTION { get; set; }
    }
}
