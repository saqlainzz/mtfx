﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AspNetIdentity.Controllers
{

    [RoutePrefix("api/claims")]
    public class ClaimsController : BaseApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Authorize]
        [Route("")]
        public IHttpActionResult GetClaims()
        {
            var identity = User.Identity as ClaimsIdentity;

            var claims = from c in identity.Claims
                         select new
                         {
                             subject = c.Subject.Name,
                             type = c.Type,
                             value = c.Value
                         };

            Log.Error("Error");
            return Ok(claims);
        }

        [Authorize]
        [Route("{id:guid}")]
        public async Task<IHttpActionResult> GetClaimUser(string Id)
        {
            var user = await this.AppUserManager.FindByIdAsync(Id);

            var claims = from c in user.Claims
                         select new
                         {
                             subject = c.ClaimType,
                             type = c.ClaimType,
                             value = c.ClaimValue
                         };

            Log.Error("Error");
            return Ok(claims);
        }

    }
}