﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.IO;
using System.Configuration;
using log4net;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/transactiondetail")]
    public class TransactionDetailController : ApiController
    {
        private TransactionDetailContext db = new TransactionDetailContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        

        [Route("transactiondetails")]
        public IHttpActionResult GetCountries()
        {
            return Ok(db.PAY_TRANSACTION_DETAIL);
        }

        [HttpGet]
        [Authorize]
        public IHttpActionResult GetPAY_TRANSACTION_DETAIL(int id)
        {
            PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILMethod = db.PAY_TRANSACTION_DETAIL.Include(s=>s.MAS_STUDENT).Include(s => s.MAS_PAYMENT_TYPE).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_SEMESTER).Include(s => s.PAY_TRANSACTION.MAS_COUNTRY).Include(s=>s.MAS_COUNTRY).Where(s => s.ID == id).FirstOrDefault();
            if (PAY_TRANSACTION_DETAILMethod == null)
            {
                return NotFound();
            }

            return Ok(PAY_TRANSACTION_DETAILMethod);
        }

        public class TRANSACTION_DETAIL
        {
            public string PAY_TRANSACTION_DETAIL { get; set; }
            public string STUDENT_ID { get; set; }
        }

        [Route("create")]
        //[ResponseType(typeof(PAY_TRANSACTION_DETAIL))]
        public IHttpActionResult PostPAY_TRANSACTION_DETAIL([FromBody] TRANSACTION_DETAIL PAY_TRANSACTION_DETAILMethod)
        {
            try
            {
                var PAY_TRANSACTION_DETAILObj = JsonConvert.DeserializeObject<PAY_TRANSACTION_DETAIL>(PAY_TRANSACTION_DETAILMethod.PAY_TRANSACTION_DETAIL);

                AspNetIdentityContext Studentcontext = new AspNetIdentityContext();
              
                AspNetIdentity.Infrastructure.TransactionContext trandb = new Infrastructure.TransactionContext();

                PAY_TRANSACTION tranObj = trandb.PAY_TRANSACTION.Include(s=>s.CMN_PAYMENT_METHOD1).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.MAS_COUNTRY).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.ID == PAY_TRANSACTION_DETAILObj.PAY_TRANSACTION_ID).FirstOrDefault();

                MAS_STUDENT studentObj = Studentcontext.MAS_STUDENT.Where(s => s.STUDENT_ID == PAY_TRANSACTION_DETAILMethod.STUDENT_ID && s.MAS_INSTITUTE_ID == tranObj.CMN_INSTITUTE_ID).FirstOrDefault();

                if (studentObj == null)
                {
                    studentObj = new MAS_STUDENT();
                    studentObj.MAS_INSTITUTE_ID = tranObj.CMN_INSTITUTE_ID;
                    studentObj.STUDENT_ID = PAY_TRANSACTION_DETAILMethod.STUDENT_ID;
                    studentObj.STATUS = "PENDING";
                    studentObj.CREATED_ON = DateTime.Now;
                    studentObj.CREATED_BY = User.Identity.GetUserId();
                    studentObj.UPDATED_BY = User.Identity.GetUserId();
                    studentObj.UPDATED_ON = DateTime.Now;

                    Studentcontext.MAS_STUDENT.Add(studentObj);
                    Studentcontext.SaveChanges();
                }

                PAY_TRANSACTION_DETAILObj.STUDENT_ID = studentObj.ID;
                PAY_TRANSACTION_DETAILObj.CREATED_ON = DateTime.Now;
                PAY_TRANSACTION_DETAILObj.CREATED_BY = User.Identity.GetUserId();
                PAY_TRANSACTION_DETAILObj.UPDATED_BY = User.Identity.GetUserId();
                PAY_TRANSACTION_DETAILObj.UPDATED_ON = DateTime.Now;
                db.Entry(PAY_TRANSACTION_DETAILObj).State = EntityState.Added;
                db.PAY_TRANSACTION_DETAIL.Add(PAY_TRANSACTION_DETAILObj);
                db.SaveChanges();

                // PAY_TRANSACTION_DETAIL obj = db.PAY_TRANSACTION_DETAIL.Include(s=>s.MAS_CITY).Include(s=>s.MAS_CITY1).Include(s=>s.MAS_STATE).Include(s=>s.MAS_STATE1).Include(s=>s.MAS_COUNTRY).Include(s=>s.MAS_COUNTRY1).Include(s=>s.MAS_PERSON_FILLING).Include(s=>s.MAS_PAYMENT_TYPE).Where(s=>s.ID == PAY_TRANSACTION_DETAILMethod.ID).FirstOrDefault();
                int tranID = PAY_TRANSACTION_DETAILObj.ID;

                transactionFormat obj = new transactionFormat();
                obj.PAY_TRANSACTION = tranObj;
                obj.PAY_TRANSACTION_DETAIL = db.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_COUNTRY).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Where(s => s.ID == tranID).FirstOrDefault();

                if (tranObj.CMN_PAYMENT_METHOD1.METHOD_NAME.ToLower().Contains("visa") || tranObj.CMN_PAYMENT_METHOD1.METHOD_NAME.ToLower().Contains("master"))
                {

                }
                else
                {
                    try
                    {
                        string ID = User.Identity.GetUserId();
                        ApplicationDbContext context = new ApplicationDbContext();
                        ApplicationUser user = context.Users.Where(s => s.Id == ID).FirstOrDefault();

                        string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/PendingEmail.html"));
                        Body = Body.Replace("#NAME#", user.FirstName + " " + user.LastName);
                        string amount = String.Format("{0:n}", tranObj.AMOUNT_PAYING);
                        string amountfx = String.Format("{0:n}", tranObj.AMOUNT_FX);
                        Body = Body.Replace("#AMOUNTFX#", amount);
                        Body = Body.Replace("#INSTITUTECURRENCY#", tranObj.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL);
                        Body = Body.Replace("#INSTITUTION#", tranObj.CMN_INSTITUTE.INSTITUTE_NAME);
                        Body = Body.Replace("#AMOUNT#", amountfx);
                        Body = Body.Replace("#PAYMENTMETHOD#", tranObj.CMN_PAYMENT_METHOD1.METHOD_NAME);
                        Body = Body.Replace("#CURRENCY#", tranObj.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL);
                        Body = Body.Replace("#RATE#", tranObj.EXCHANGE_RATE.ToString());
                        Body = Body.Replace("#LINK#", ConfigurationManager.AppSettings["mainlink"].ToString());
                        DateTime dt = tranObj.EXPIRY_DATE ?? DateTime.Now;
                        string now = dt.ToString("MM/dd/yyyy");
                        Body = Body.Replace("#EXPIRY#", now);
                        Body = Body.Replace("#PAYMENTID#", tranObj.PREFIX);

                        EMAIL_QUEUE eq = new EMAIL_QUEUE();
                        eq.SUBJECT = "Payment Pending for " + tranObj.CMN_INSTITUTE.INSTITUTE_NAME;
                        eq.BODY = Body;
                        eq.DATE_CREATED = DateTime.Now;
                        eq.EMAIL_TO = user.Email;
                        eq.BCC = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                        Studentcontext.Entry(eq).State = EntityState.Added;
                        Studentcontext.EMAIL_QUEUE.Add(eq);
                        Studentcontext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.Message);
                    }
                }

                return Ok(obj);
            }catch(Exception ex){
                return BadRequest(ex.Message);
            }
         
        }

        [Authorize]
        [HttpGet]
        [Route("updateTransactionDetail")]
        public IHttpActionResult updateTransactionDetail([FromUri] string fieldname, [FromUri] int transactionid, [FromUri] string value)
        {
            PAY_TRANSACTION_DETAIL transactionObject = db.PAY_TRANSACTION_DETAIL.Where(s => s.PAY_TRANSACTION_ID == transactionid).FirstOrDefault();
            
            if (transactionObject == null)
            {
                return NotFound();
            }


            int intValue = 0;

            try
            {
                intValue = Int32.Parse(value);
                typeof(PAY_TRANSACTION_DETAIL).GetProperty(fieldname).SetValue(transactionObject, intValue);
                db.Entry(transactionObject).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(transactionObject);
            }
            catch
            {
                typeof(PAY_TRANSACTION_DETAIL).GetProperty(fieldname).SetValue(transactionObject, value);
                db.Entry(transactionObject).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(transactionObject);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("updateNotes")]
        public IHttpActionResult updateNotes([FromUri] string notes, [FromUri] int transactionid)
        {
            PAY_TRANSACTION_DETAIL transactionObject = db.PAY_TRANSACTION_DETAIL.Where(s => s.PAY_TRANSACTION_ID == transactionid).FirstOrDefault();

            if (transactionObject == null)
            {
                return NotFound();
            }

            transactionObject.NOTES = notes;
            db.Entry(transactionObject).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(transactionObject);
        }

        public class updateFormatDetail
        {
            public int ID { get; set; }
            public string STUDENT_ID { get; set; }
            public int INSTITUTE_ID { get; set; }
            public string PAYEE_FIRST_NAME { get; set; }
            public string PAYEE_LAST_NAME { get; set; }
            public string PAYEE_ADDRESS_LINE1 { get; set; }
            public string PAYEE_ADDRESS_LINE2 { get; set; }
            public string PAYEE_MAS_CITY { get; set; }
            public string PAYEE_MAS_STATE { get; set; }
            public string PAYEE_POSTAL_CODE { get; set; }
            public string PAYEE_PHONE_NUMBER { get; set; }
            public int PAYEE_MAS_COUNTRY_ID { get; set; }
            public string PAYEE_EMAIL { get; set; }
            public int MAS_PAYMENT_TYPE_ID { get; set; }
            public string PROGRAM_OF_STUDY { get; set; }
            public int MAS_SEMESTER_ID { get; set; }
            public string INVOICE_NUMBER { get; set; }
            public string STUDENT_FIRST_NAME { get; set; }
            public string STUDENT_LAST_NAME { get; set; }
            public DateTime STUDENT_DATE_OF_BIRTH { get; set; }
            public string STUDENT_EMAIL { get; set; }
            public string STUDENT_IDENTIFICATION_TYPE { get; set; }
            public DateTime STUDENT_IDENTIFICATION_EXPIRY { get; set; }
            public string STUDENT_IDENTIFICATION_NUMBER { get; set; }
            public string STUDENT_IDENTIFICATION_FILE { get; set; }
            public string PARENT_IDENTIFICATION_TYPE { get; set; }
            public DateTime PARENT_IDENTIFICATION_EXPIRY { get; set; }
            public string PARENT_IDENTIFICATION_NUMBER { get; set; }
            public string PARENT_IDENTIFICATION_FILE { get; set; }
            public string PAYER_SENDER_NAME_NICN { get; set; }
            public string PAYER_SENDER_IDENTIFICATION_NUMBER { get; set; }
            public string ACCEPTANCE_LETTER { get; set; }
            public string BANK_INSTRUCTIONS_DOCUMENT_FILE { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("updateAllTransactionDetail")]
        public IHttpActionResult updateAllTransactionDetail(updateFormatDetail updateFormatDetail)
        {
            PAY_TRANSACTION_DETAIL transactionObject = db.PAY_TRANSACTION_DETAIL.Where(s => s.ID == updateFormatDetail.ID).FirstOrDefault();

            if (transactionObject == null)
            {
                return NotFound();
            }

            transactionObject.PAYEE_FIRST_NAME = updateFormatDetail.PAYEE_FIRST_NAME;
            transactionObject.PAYEE_LAST_NAME = updateFormatDetail.PAYEE_LAST_NAME;
            transactionObject.PAYEE_ADDRESS_LINE1 = updateFormatDetail.PAYEE_ADDRESS_LINE1;
            transactionObject.PAYEE_ADDRESS_LINE2 = updateFormatDetail.PAYEE_ADDRESS_LINE2;
            transactionObject.PAYEE_MAS_CITY = updateFormatDetail.PAYEE_MAS_CITY;
            transactionObject.PAYEE_MAS_STATE = updateFormatDetail.PAYEE_MAS_STATE;
            transactionObject.PAYEE_POSTAL_CODE = updateFormatDetail.PAYEE_POSTAL_CODE;
            transactionObject.PAYEE_PHONE_NUMBER = updateFormatDetail.PAYEE_PHONE_NUMBER;
            transactionObject.PAYEE_MAS_COUNTRY_ID = updateFormatDetail.PAYEE_MAS_COUNTRY_ID;
            transactionObject.PAYEE_EMAIL = updateFormatDetail.PAYEE_EMAIL;
            transactionObject.MAS_PAYMENT_TYPE_ID = updateFormatDetail.MAS_PAYMENT_TYPE_ID;
            transactionObject.PROGRAM_OF_STUDY = updateFormatDetail.PROGRAM_OF_STUDY;
            transactionObject.MAS_SEMESTER_ID = updateFormatDetail.MAS_SEMESTER_ID;
            transactionObject.INVOICE_NUMBER = updateFormatDetail.INVOICE_NUMBER;
            transactionObject.STUDENT_FIRST_NAME = updateFormatDetail.STUDENT_FIRST_NAME;
            transactionObject.STUDENT_LAST_NAME = updateFormatDetail.STUDENT_LAST_NAME;
            transactionObject.STUDENT_DATE_OF_BIRTH = updateFormatDetail.STUDENT_DATE_OF_BIRTH;
            transactionObject.STUDENT_EMAIL = updateFormatDetail.STUDENT_EMAIL;
            transactionObject.STUDENT_DOCUMENT_TYPE = updateFormatDetail.STUDENT_IDENTIFICATION_TYPE;
            transactionObject.STUDENT_DOCUMENT_EXPIRY = updateFormatDetail.STUDENT_IDENTIFICATION_EXPIRY;
            transactionObject.STUDENT_DOCUMENT_NUMBER = updateFormatDetail.STUDENT_IDENTIFICATION_NUMBER;
            transactionObject.STUDENT_DOCUMENT_FILE = updateFormatDetail.STUDENT_IDENTIFICATION_FILE;
            transactionObject.PAYEE_DOCUMENT_TYPE = updateFormatDetail.PARENT_IDENTIFICATION_TYPE;
            transactionObject.PAYEE_DOCUMENT_EXPIRY = updateFormatDetail.PARENT_IDENTIFICATION_EXPIRY;
            transactionObject.PAYEE_DOCUMENT_NUMBER = updateFormatDetail.PARENT_IDENTIFICATION_NUMBER;
            transactionObject.PAYEE_DOCUMENT_FILE = updateFormatDetail.PARENT_IDENTIFICATION_FILE;
            transactionObject.PAYER_SENDER_NAME_NICN = updateFormatDetail.PAYER_SENDER_NAME_NICN;
            transactionObject.PAYER_SENDER_IDENTIFICATION_NUMBER = updateFormatDetail.PAYER_SENDER_IDENTIFICATION_NUMBER;
            transactionObject.ACCEPTANCE_LETTER = updateFormatDetail.ACCEPTANCE_LETTER;
            transactionObject.BANK_INSTRUCTIONS_DOCUMENT_FILE = updateFormatDetail.BANK_INSTRUCTIONS_DOCUMENT_FILE;

            AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

            MAS_STUDENT studentObj = Studentcontext.MAS_STUDENT.Where(s => s.STUDENT_ID == updateFormatDetail.STUDENT_ID && s.MAS_INSTITUTE_ID == updateFormatDetail.INSTITUTE_ID).FirstOrDefault();

            if (studentObj == null)
            {
                studentObj = new MAS_STUDENT();
                studentObj.MAS_INSTITUTE_ID = updateFormatDetail.INSTITUTE_ID;
                studentObj.STUDENT_ID = updateFormatDetail.STUDENT_ID;
                studentObj.STATUS = "PENDING";
                studentObj.CREATED_ON = DateTime.Now;
                studentObj.CREATED_BY = User.Identity.GetUserId();
                studentObj.UPDATED_BY = User.Identity.GetUserId();
                studentObj.UPDATED_ON = DateTime.Now;

                Studentcontext.MAS_STUDENT.Add(studentObj);
                Studentcontext.SaveChanges();
            }

            transactionObject.STUDENT_ID = studentObj.ID;
 
            db.Entry(transactionObject).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(transactionObject);
        }

        [Authorize]
        [HttpGet]
        [Route("updateTransactionDetailStudent")]
        public IHttpActionResult updateTransactionDetailStudent([FromUri] int instituteid, [FromUri] int transactionid, [FromUri] string value)
        {
            PAY_TRANSACTION_DETAIL transactionObject = db.PAY_TRANSACTION_DETAIL.Where(s => s.ID == transactionid).FirstOrDefault();
            
            if (transactionObject == null)
            {
                return NotFound();
            }

            AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

            MAS_STUDENT studentObj = Studentcontext.MAS_STUDENT.Where(s => s.STUDENT_ID == value && s.MAS_INSTITUTE_ID == instituteid).FirstOrDefault();

            if (studentObj == null)
            {
                studentObj = new MAS_STUDENT();
                studentObj.MAS_INSTITUTE_ID = instituteid;
                studentObj.STUDENT_ID = value;
                studentObj.STATUS = "PENDING";
                studentObj.CREATED_ON = DateTime.Now;
                studentObj.CREATED_BY = User.Identity.GetUserId();
                studentObj.UPDATED_BY = User.Identity.GetUserId();
                studentObj.UPDATED_ON = DateTime.Now;

                Studentcontext.MAS_STUDENT.Add(studentObj);
                Studentcontext.SaveChanges();
            }

            transactionObject.STUDENT_ID = studentObj.ID;
            db.Entry(transactionObject).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(transactionObject);
        }

        public class transactionFormat
        {
            public PAY_TRANSACTION PAY_TRANSACTION { get; set; }
            public PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAIL { get; set; }
        }

        [Authorize(Roles = "Admin")]
        public IHttpActionResult PutPAY_TRANSACTION_DETAIL(int id, PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != PAY_TRANSACTION_DETAILMethod.ID)
            {
                return BadRequest();
            }

            db.Entry(PAY_TRANSACTION_DETAILMethod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CMN_COUNTRYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE api/Default1/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(PAY_TRANSACTION_DETAIL))]
        public IHttpActionResult DeletePAY_TRANSACTION_DETAIL(int id)
        {
            PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILMethod = db.PAY_TRANSACTION_DETAIL.Find(id);
            if (PAY_TRANSACTION_DETAILMethod == null)
            {
                return NotFound();
            }

            db.PAY_TRANSACTION_DETAIL.Remove(PAY_TRANSACTION_DETAILMethod);
            db.SaveChanges();

            return Ok(PAY_TRANSACTION_DETAILMethod);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.PAY_TRANSACTION_DETAIL.Count(e => e.ID == id) > 0;
        }
    }
}