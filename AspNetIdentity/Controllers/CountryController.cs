﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using System.Security.Cryptography;
using System.Text;

namespace AspNetIdentity.Controllers
{
    public class CountryController : ApiController
    {
        private CountryContext db = new CountryContext();

        [HttpGet]
        [Route("api/Country")]
        public IHttpActionResult GetCountry()
        {
            var countryList = db.MAS_COUNTRY.Include(s=>s.CMN_CURRENCY).OrderBy(s => s.COUNTRY_NAME).ToList();

            return Ok(countryList);;
        }

        [HttpGet]
        [Route("api/Country")]
        public IHttpActionResult GetMASCountry([FromUri] int id)
        {
            MAS_COUNTRY mAS_COUNTRY = db.MAS_COUNTRY.Include(s => s.CMN_CURRENCY).Where(s=>s.ID == id).FirstOrDefault();
            if (mAS_COUNTRY == null)
            {
                return NotFound();
            }

            return Ok(mAS_COUNTRY);
        }

        // PUT: api/Country/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMAS_COUNTRY(int id, MAS_COUNTRY mAS_COUNTRY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mAS_COUNTRY.ID)
            {
                return BadRequest();
            }

            db.Entry(mAS_COUNTRY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MAS_COUNTRYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Country
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(MAS_COUNTRY))]
        public IHttpActionResult PostMAS_COUNTRY(MAS_COUNTRY mAS_COUNTRY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAS_COUNTRY.Add(mAS_COUNTRY);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mAS_COUNTRY.ID }, mAS_COUNTRY);
        }

        // DELETE: api/Country/5
        [ResponseType(typeof(MAS_COUNTRY))]
        public IHttpActionResult DeleteMAS_COUNTRY(int id)
        {
            MAS_COUNTRY mAS_COUNTRY = db.MAS_COUNTRY.Find(id);
            if (mAS_COUNTRY == null)
            {
                return NotFound();
            }

            db.MAS_COUNTRY.Remove(mAS_COUNTRY);
            db.SaveChanges();

            return Ok(mAS_COUNTRY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_COUNTRYExists(int id)
        {
            return db.MAS_COUNTRY.Count(e => e.ID == id) > 0;
        }
    }
}