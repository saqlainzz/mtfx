﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using System.Text.RegularExpressions;
using log4net;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Configuration;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/institute")]
    public class InstituteController : ApiController
    {
        private InstituteContext db = new InstituteContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("institutes")]
        public List<CMN_INSTITUTE> GetInstitutes()
        {
            //var accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
            //var authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

            //TwilioClient.Init(accountSid, authToken);

            //var message = MessageResource.Create(
            //    to: new PhoneNumber("+923232110299"),
            //    from: new PhoneNumber(ConfigurationManager.AppSettings["TwilioPhoneNumber"].ToString()),
            //    body: "Hello from C#");

            //string sid = message.Sid;

            try
            {
                List<CMN_INSTITUTE> obj = db.CMN_INSTITUTE.Include(s => s.CMN_CURRENCY).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_CITY).Include(s => s.MAS_STATE).OrderBy(s => s.INSTITUTE_NAME).ToList();
                return obj;
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return null;
            }
        }

        [HttpGet]
        [Route("GetInstitute")]
        public IHttpActionResult GettingInstitute([FromUri] string instituteSlug)
        {
            try{
                CMN_INSTITUTE CMN_INSTITUTEMethod = db.CMN_INSTITUTE.Include(s => s.CMN_CURRENCY).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_CITY).Include(s => s.MAS_STATE).Where(s => s.INSTITUTE_SLUG == instituteSlug).FirstOrDefault();

                if (CMN_INSTITUTEMethod == null)
                {
                    return NotFound();
                }

                return Ok(CMN_INSTITUTEMethod);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }


        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("prefix")]
        public IHttpActionResult checkprefix([FromUri] string prefix)
        {
            try{
                var data = db.CMN_INSTITUTE.Where(s => s.INSTITUTE_PREFIX == prefix).FirstOrDefault();

                if (data != null)
                {
                    return Content(HttpStatusCode.BadRequest, "Already exist");
                }

                return Content(HttpStatusCode.OK, "No match");
             }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("slug")]
        public IHttpActionResult checkslug([FromUri] string slug)
        {
            try{
                var data = db.CMN_INSTITUTE.Where(s => s.INSTITUTE_SLUG == slug).FirstOrDefault();

                if (data != null)
                {
                    return Content(HttpStatusCode.BadRequest, "Already exist");
                }

                return Content(HttpStatusCode.OK, "No match");
             }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [ResponseType(typeof(CMN_INSTITUTE))]
        public IHttpActionResult GetInstitutesByID(int id)
        {
            CMN_INSTITUTE CMN_INSTITUTEMethod = db.CMN_INSTITUTE.Include(s => s.CMN_CURRENCY).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_CITY).Include(s => s.MAS_STATE).Where(s=>s.ID==id).FirstOrDefault();

            if (CMN_INSTITUTEMethod == null)
            {
                return NotFound();
            }

            return Ok(CMN_INSTITUTEMethod);
        }

        public formatting formatResponse(CMN_INSTITUTE institueObject, string code)
        {
            return new formatting
            {
                institue = institueObject,
                currencyCode = code,
            };
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [ResponseType(typeof(CMN_INSTITUTE))]
        public IHttpActionResult PostCMN_INSTITUTE(CMN_INSTITUTE CMN_INSTITUTEMethod)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

               // string slug = GenerateSlug(CMN_INSTITUTEMethod.INSTITUTE_NAME);

             //   CMN_INSTITUTE obj = db.CMN_INSTITUTE.Where(s => s.INSTITUTE_SLUG == slug).FirstOrDefault();

               // if (obj != null)
               // {
               //     String UUID = Guid.NewGuid().ToString();
                    //slug = slug + "-" + UUID;

                    //if (obj.INSTITUTE_SLUG[obj.INSTITUTE_SLUG.Length - 1] > 0)
                    //{
                    //    int num = Convert.ToInt32(obj.INSTITUTE_SLUG[obj.INSTITUTE_SLUG.Length - 1]) + 1;
                    //    slug = obj.INSTITUTE_SLUG.Remove(obj.INSTITUTE_SLUG.Length - 1, 1).Insert(obj.INSTITUTE_SLUG.Length - 1, num.ToString()); ;
                    //}
                    //else
                    //{
                       
                    //}
               // }
                //CurrencyContext cc = new CurrencyContext();
                //CMN_CURRENCY cmnCurr = cc.CMN_CURRENCY.Find(CMN_INSTITUTEMethod.INSTITUTE_CURR_ID);
                //CMN_INSTITUTEMethod.CMN_CURRENCY = cmnCurr;
               // CMN_INSTITUTEMethod.INSTITUTE_SLUG = slug;
                CMN_INSTITUTEMethod.CREATED_ON = DateTime.Now;
                CMN_INSTITUTEMethod.UPDATED_ON = DateTime.Now;
                CMN_INSTITUTEMethod.CREATED_BY = User.Identity.GetUserId();
                CMN_INSTITUTEMethod.UPDATED_BY = User.Identity.GetUserId();
                db.Entry(CMN_INSTITUTEMethod).State = EntityState.Added;
                db.CMN_INSTITUTE.Add(CMN_INSTITUTEMethod);
                db.SaveChanges();

                return Ok(CMN_INSTITUTEMethod);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return Content(HttpStatusCode.NotFound, ex.Message.ToString());
            }
        }

        public static string GenerateSlug(string phrase)
        {
            string str = RemoveAccent(phrase).ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveAccent(string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [Route("update")]
        [HttpPost]
        public IHttpActionResult updateingCMN_INSTITUTE(int id, CMN_INSTITUTE CMN_INSTITUTEMethod)
        {
            if (id != CMN_INSTITUTEMethod.ID)
            {
                return BadRequest();
            }
            CMN_INSTITUTE obj = db.CMN_INSTITUTE.Where(s => s.ID == id).FirstOrDefault();
            obj.INSTITUTE_ADDRESS_LINE1 = CMN_INSTITUTEMethod.INSTITUTE_ADDRESS_LINE1;
            obj.INSTITUTE_ADDRESS_LINE2 = CMN_INSTITUTEMethod.INSTITUTE_ADDRESS_LINE2;
            obj.INSTITUTE_CITY_ID = CMN_INSTITUTEMethod.INSTITUTE_CITY_ID;
            obj.INSTITUTE_COUNTRY_ID = CMN_INSTITUTEMethod.INSTITUTE_COUNTRY_ID;
            obj.INSTITUTE_EMAIL = CMN_INSTITUTEMethod.INSTITUTE_EMAIL;
            obj.INSTITUTE_LOGO = CMN_INSTITUTEMethod.INSTITUTE_LOGO;
            obj.INSTITUTE_NAME = CMN_INSTITUTEMethod.INSTITUTE_NAME;
            obj.INSTITUTE_PHONE = CMN_INSTITUTEMethod.INSTITUTE_PHONE;
            obj.INSTITUTE_POSTAL_CODE = CMN_INSTITUTEMethod.INSTITUTE_POSTAL_CODE;
            obj.INSTITUTE_STATE_ID = CMN_INSTITUTEMethod.INSTITUTE_STATE_ID;
            obj.INSTITUTE_WEB = CMN_INSTITUTEMethod.INSTITUTE_WEB;
            obj.UPDATED_BY = User.Identity.GetUserId();
            obj.UPDATED_ON = DateTime.Now;

            db.Entry(obj).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CMN_COUNTRYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(CMN_INSTITUTEMethod);
        }

        // DELETE api/Default1/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(CMN_INSTITUTE))]
        public IHttpActionResult DeleteCMN_INSTITUTE(int id)
        {
            CMN_INSTITUTE CMN_INSTITUTEMethod = db.CMN_INSTITUTE.Find(id);
            if (CMN_INSTITUTEMethod == null)
            {
                return NotFound();
            }

            db.CMN_INSTITUTE.Remove(CMN_INSTITUTEMethod);
            db.SaveChanges();

            return Ok(CMN_INSTITUTEMethod);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.CMN_INSTITUTE.Count(e => e.ID == id) > 0;
        }
    }

    public class formatting
    {
        public CMN_INSTITUTE institue { get; set; }

        public string currencyCode { get; set; }
    }
}