﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Microsoft.AspNet.Identity;
using AspNetIdentity.Infrastructure;
using System.IO;
using System.Configuration;
using log4net;

namespace AspNetIdentity.Controllers
{
    public class BatchController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
        
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        //// GET api/Batch
        //public IQueryable<PAY_BATCH> GetPAY_BATCH()
        //{
        //    return db.PAY_BATCH;
        //}

        //// GET api/Batch/5
        //[ResponseType(typeof(PAY_BATCH))]
        //public IHttpActionResult GetPAY_BATCH(int id)
        //{
        //    PAY_BATCH pay_batch = db.PAY_BATCH.Find(id);
        //    if (pay_batch == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(pay_batch);
        //}

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("api/GetInstituteBatch")]
        public IHttpActionResult AllTransactions([FromUri] int instituteID)
        {
            try
            {
                List<PAY_BATCH> batchObject = null;

                batchObject = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.CMN_INSTITUE_ID == instituteID && (s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST")).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();

                if (batchObject == null)
                {
                    return NotFound();
                }

                List<BatchModel> batchModel = new List<BatchModel>();
                AspNetIdentity.Infrastructure.TransactionContext transactionObj = new AspNetIdentity.Infrastructure.TransactionContext();
                AspNetIdentityContext contextObj = new AspNetIdentityContext();

                for (int i = 0; i < batchObject.Count(); i++)
                {
                    int batchID = batchObject[i].ID;
                    List<PAY_BATCH_TRANSACTION> batchTranObj = contextObj.PAY_BATCH_TRANSACTION.Where(s => s.PAY_BATCH_ID == batchID).ToList();

                    BatchModel batchObj = new BatchModel();
                    double totalAmount = 0;
                    int totalCount = 0;

                    batchObj.ID = batchObject[i].ID;
                    batchObj.RefNo = batchObject[i].REF_NO;
                    batchObj.DateCreated = (DateTime)batchObject[i].CREATED_ON;
                    batchObj.Status = batchObject[i].STATUS;
                    batchObj.InstituteID = (int)batchObject[i].CMN_INSTITUE_ID;
                    batchObj.CMN_INSTITUTE = batchObject[i].CMN_INSTITUTE;
                    batchObj.SubStatus = batchObject[i].SUB_STATUS;
                    batchObj.SettlementDate = batchObject[i].SETTLEMENT_DATE ?? DateTime.Now;
                    batchObj.Currency = batchObject[i].CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;

                    for (int j = 0; j < batchTranObj.Count(); j++)
                    {
                        int batchTranID = (int)batchTranObj[j].TRANSACTION_ID;
                        PAY_TRANSACTION payTran = transactionObj.PAY_TRANSACTION.Where(s => s.ID == batchTranID).FirstOrDefault();
                        totalAmount = totalAmount + Double.Parse(payTran.AMOUNT_PAYING.ToString());
                        totalCount = totalCount + 1;

                        if (j == 0)
                        {
                            CurrencyContext currContext = new CurrencyContext();

                            CMN_CURRENCY currObj = currContext.CMN_CURRENCY.Where(s => s.ID == payTran.CMN_CURRENCY_ID).FirstOrDefault();
                        }
                    }

                    batchObj.TotalAmount = totalAmount;
                    batchObj.TotalCount = totalCount;
                    batchModel.Add(batchObj);
                }

                return Ok(batchModel);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("api/GetFilterInstituteBatch")]
        public IHttpActionResult GetFilterInstituteBatch([FromUri] string status, [FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try
            {
                if (ToDate != null)
                {
                    ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                string UserID = User.Identity.GetUserId();
                ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                int instituteID = 0;
                instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return BadRequest("Invalid Token");
                }

                List<PAY_BATCH> batchObject = null;

                batchObject = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.CMN_INSTITUE_ID == instituteID).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();

                if (batchObject == null)
                {
                    return NotFound();
                }

                status = status.Substring(1, status.Length - 2);

                string[] substatusarray = status.Split(',').ToArray();

                if (substatusarray[0] == "" && FromDate == null && ToDate == null)
                {
                    batchObject = db.PAY_BATCH.Where(s => 2 == 1).ToList();
                    return Ok(batchObject);
                }
                else
                {
                    if (substatusarray.Count() != 0)
                    {
                        if (substatusarray[0] != "")
                        {
                            batchObject = new List<PAY_BATCH>();
                        }

                        for (int i = 0; i < substatusarray.Count(); i++)
                        {
                            if (substatusarray[i] == "FUNDS_IN_TRANSIT")
                            {
                                var tranList = db.PAY_BATCH.Where(s => s.STATUS == "ADMIN_POST" && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED" && s.CMN_INSTITUE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    batchObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "PENDING_RELEASE")
                            {
                                var tranList = db.PAY_BATCH.Where(s => s.STATUS == "ADMIN_UNPOST" && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED" && s.CMN_INSTITUE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    batchObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "RECEIPT_CONFIRMED")
                            {
                                var tranList = db.PAY_BATCH.Where(s => s.STATUS == "UNIVERSITY_POST" && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED" && s.CMN_INSTITUE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    batchObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "ON_HOLD")
                            {
                                var tranList = db.PAY_BATCH.Where(s => (s.STATUS == "UNIVERSITY_POST" || s.STATUS == "ADMIN_POST" || s.STATUS == "ADMIN_UNPOST") && s.SUB_STATUS == "ON_HOLD" && s.CMN_INSTITUE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    batchObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "CANCELLED")
                            {
                                var tranList = db.PAY_BATCH.Where(s => (s.STATUS == "UNIVERSITY_POST" || s.STATUS == "ADMIN_POST" || s.STATUS == "ADMIN_UNPOST") && s.SUB_STATUS == "CANCELLED" && s.CMN_INSTITUE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    batchObject.Add(tranList[k]);
                                }
                            }
                        }
                    }

                    if (FromDate != null)
                    {
                        batchObject = batchObject.Where(s => s.CREATED_ON > FromDate).ToList();
                    }

                    if (ToDate != null)
                    {
                        batchObject = batchObject.Where(s => s.CREATED_ON < ToDate).ToList();
                    }

                    List<BatchModel> batchModel = new List<BatchModel>();
                    AspNetIdentity.Infrastructure.TransactionContext transactionObj = new AspNetIdentity.Infrastructure.TransactionContext();
                    AspNetIdentityContext contextObj = new AspNetIdentityContext();

                    for (int i = 0; i < batchObject.Count(); i++)
                    {
                        int batchID = batchObject[i].ID;
                        List<PAY_BATCH_TRANSACTION> batchTranObj = contextObj.PAY_BATCH_TRANSACTION.Where(s => s.PAY_BATCH_ID == batchID).ToList();

                        BatchModel batchObj = new BatchModel();
                        double totalAmount = 0;
                        int totalCount = 0;

                        batchObj.ID = batchObject[i].ID;
                        batchObj.RefNo = batchObject[i].REF_NO;
                        batchObj.DateCreated = (DateTime)batchObject[i].CREATED_ON;
                        batchObj.Status = batchObject[i].STATUS;
                        batchObj.InstituteID = (int)batchObject[i].CMN_INSTITUE_ID;
                        batchObj.CMN_INSTITUTE = batchObject[i].CMN_INSTITUTE;
                        batchObj.SubStatus = batchObject[i].SUB_STATUS;
                        batchObj.SettlementDate = batchObject[i].SETTLEMENT_DATE ?? DateTime.Now;
                        batchObj.Currency = batchObject[i].CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;

                        for (int j = 0; j < batchTranObj.Count(); j++)
                        {
                            int batchTranID = (int)batchTranObj[j].TRANSACTION_ID;
                            PAY_TRANSACTION payTran = transactionObj.PAY_TRANSACTION.Where(s => s.ID == batchTranID).FirstOrDefault();
                            totalAmount = totalAmount + Double.Parse(payTran.AMOUNT_PAYING.ToString());
                            totalCount = totalCount + 1;

                            if (j == 0)
                            {
                                CurrencyContext currContext = new CurrencyContext();

                                CMN_CURRENCY currObj = currContext.CMN_CURRENCY.Where(s => s.ID == payTran.CMN_CURRENCY_ID).FirstOrDefault();
                            }
                        }

                        batchObj.TotalAmount = totalAmount;
                        batchObj.TotalCount = totalCount;
                        batchModel.Add(batchObj);
                    }

                    return Ok(batchModel);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("api/GetAllBatches")]
        public IHttpActionResult GetAllBatches([FromUri] int instituteID, [FromUri] string substatus)
        {
            try
            {
                List<PAY_BATCH> batchObject = null;
                int count = 0;

                substatus = substatus.Substring(1, substatus.Length - 2);

                List<string> substatusarray = substatus.Split(',').ToList();

                if (substatusarray[0] == "")
                {
                    substatusarray.Remove(substatusarray[0]);
                }

                if (substatusarray.Count() != 0)
                {
                    batchObject = new List<PAY_BATCH>();

                    for (int i = 0; i < substatusarray.Count(); i++)
                    {
                        if (substatusarray[i] == "PENDING_RELEASED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.STATUS == "ADMIN_UNPOST" && s.SUB_STATUS != "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "DELIVERED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.STATUS == "UNIVERSITY_POST" && s.SUB_STATUS != "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "RELEASED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.STATUS == "ADMIN_POST" && s.SUB_STATUS != "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "CANCELLED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.SUB_STATUS == "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        count = batchObject.Count;
                        batchObject = batchObject.OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();
                    }
                }
                else
                {
                    if (instituteID == 0)
                    {
                        batchObject = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();
                        count = db.PAY_BATCH.Count();
                    }
                    else
                    {
                        batchObject = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.CMN_INSTITUE_ID == instituteID).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();
                        count = db.PAY_BATCH.Where(s => s.CMN_INSTITUE_ID == instituteID).Count();
                    }
                }

                if (batchObject == null)
                {
                    return NotFound();
                }

                List<BatchModel> batchModel = new List<BatchModel>();
                AspNetIdentity.Infrastructure.TransactionContext transactionObj = new AspNetIdentity.Infrastructure.TransactionContext();
                AspNetIdentityContext contextObj = new AspNetIdentityContext();

                for (int i = 0; i < batchObject.Count(); i++)
                {
                    int batchID = batchObject[i].ID;
                    List<PAY_BATCH_TRANSACTION> batchTranObj = contextObj.PAY_BATCH_TRANSACTION.Where(s => s.PAY_BATCH_ID == batchID).ToList();

                    BatchModel batchObj = new BatchModel();
                    double totalAmount = 0;
                    int totalCount = 0;

                    batchObj.ID = batchObject[i].ID;
                    batchObj.RefNo = batchObject[i].REF_NO;
                    batchObj.DateCreated = (DateTime)batchObject[i].CREATED_ON;
                    batchObj.Status = batchObject[i].STATUS;
                    batchObj.InstituteID = (int)batchObject[i].CMN_INSTITUE_ID;
                    batchObj.CMN_INSTITUTE = batchObject[i].CMN_INSTITUTE;
                    batchObj.SubStatus = batchObject[i].SUB_STATUS;
                    batchObj.SettlementDate = batchObject[i].SETTLEMENT_DATE ?? DateTime.Now;
                    batchObj.Currency = batchObject[i].CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;

                    for (int j = 0; j < batchTranObj.Count(); j++)
                    {
                        int batchTranID = (int)batchTranObj[j].TRANSACTION_ID;
                        PAY_TRANSACTION payTran = transactionObj.PAY_TRANSACTION.Where(s => s.ID == batchTranID).FirstOrDefault();
                        totalAmount = totalAmount + Double.Parse(payTran.AMOUNT_PAYING.ToString());
                        totalCount = totalCount + 1;

                        if (j == 0)
                        {
                            CurrencyContext currContext = new CurrencyContext();

                            CMN_CURRENCY currObj = currContext.CMN_CURRENCY.Where(s => s.ID == payTran.CMN_CURRENCY_ID).FirstOrDefault();
                        }
                    }

                    batchObj.TotalAmount = totalAmount;
                    batchObj.TotalCount = totalCount;

                    batchModel.Add(batchObj);
                }

                return Ok(formatObject(count, batchModel));
            }
            catch(Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("api/ExportGetAllBatches")]
        public IHttpActionResult ExportGetAllBatches([FromUri] int instituteID, [FromUri] string substatus)
        {
            try
            {
                List<PAY_BATCH> batchObject = null;

                substatus = substatus.Substring(1, substatus.Length - 2);

                List<string> substatusarray = substatus.Split(',').ToList();

                if (substatusarray[0] == "")
                {
                    substatusarray.Remove(substatusarray[0]);
                }

                if (substatusarray.Count() != 0)
                {
                    batchObject = new List<PAY_BATCH>();

                    for (int i = 0; i < substatusarray.Count(); i++)
                    {
                        if (substatusarray[i] == "PENDING_RELEASED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.STATUS == "ADMIN_UNPOST" && s.SUB_STATUS != "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "DELIVERED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.STATUS == "UNIVERSITY_POST" && s.SUB_STATUS != "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "RELEASED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.STATUS == "ADMIN_POST" && s.SUB_STATUS != "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "CANCELLED")
                        {
                            var tranList = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.SUB_STATUS == "CANCELLED").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                batchObject.Add(tranList[k]);
                            }
                        }

                        batchObject = batchObject.OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();
                    }
                }
                else
                {
                    if (instituteID == 0)
                    {
                        batchObject = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();
                    }
                    else
                    {
                        batchObject = db.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.CMN_INSTITUE_ID == instituteID).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();
                    }
                }

                if (batchObject == null)
                {
                    return NotFound();
                }

                List<BatchModel> batchModel = new List<BatchModel>();
                AspNetIdentity.Infrastructure.TransactionContext transactionObj = new AspNetIdentity.Infrastructure.TransactionContext();
                AspNetIdentityContext contextObj = new AspNetIdentityContext();

                for (int i = 0; i < batchObject.Count(); i++)
                {
                    int batchID = batchObject[i].ID;
                    List<PAY_BATCH_TRANSACTION> batchTranObj = contextObj.PAY_BATCH_TRANSACTION.Where(s => s.PAY_BATCH_ID == batchID).ToList();

                    BatchModel batchObj = new BatchModel();
                    double totalAmount = 0;
                    int totalCount = 0;

                    batchObj.ID = batchObject[i].ID;
                    batchObj.RefNo = batchObject[i].REF_NO;
                    batchObj.DateCreated = (DateTime)batchObject[i].CREATED_ON;
                    batchObj.Status = batchObject[i].STATUS;
                    batchObj.InstituteID = (int)batchObject[i].CMN_INSTITUE_ID;
                    batchObj.CMN_INSTITUTE = batchObject[i].CMN_INSTITUTE;
                    batchObj.SubStatus = batchObject[i].SUB_STATUS;
                    batchObj.SettlementDate = batchObject[i].SETTLEMENT_DATE ?? DateTime.Now;
                    batchObj.Currency = batchObject[i].CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;

                    for (int j = 0; j < batchTranObj.Count(); j++)
                    {
                        int batchTranID = (int)batchTranObj[j].TRANSACTION_ID;
                        PAY_TRANSACTION payTran = transactionObj.PAY_TRANSACTION.Where(s => s.ID == batchTranID).FirstOrDefault();
                        totalAmount = totalAmount + Double.Parse(payTran.AMOUNT_PAYING.ToString());
                        totalCount = totalCount + 1;

                        if (j == 0)
                        {
                            CurrencyContext currContext = new CurrencyContext();

                            CMN_CURRENCY currObj = currContext.CMN_CURRENCY.Where(s => s.ID == payTran.CMN_CURRENCY_ID).FirstOrDefault();
                        }
                    }

                    batchObj.TotalAmount = totalAmount;
                    batchObj.TotalCount = totalCount;

                    batchModel.Add(batchObj);
                }

                return Ok(batchModel);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public formatingClass formatObject(int COUNT, List<BatchModel> tranObject)
        {
            return new formatingClass
            {
                total_count = COUNT,
                data = tranObject
            };
        }

        public class formatingClass
        {
            public int total_count { get; set; }
            public List<BatchModel> data { get; set; }
        }


        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [Authorize]
        [HttpGet]
        [Route("api/BatchStatusChange")]
        public IHttpActionResult ChangeStatus([FromUri] int batchID, [FromUri] string status)
        {
            try
            {
                if (batchID == 0)
                {
                    return NotFound();
                }

                PAY_BATCH batchObject = db.PAY_BATCH.Where(s => s.ID == batchID).FirstOrDefault();

                if (status == "ADMIN_POST")
                {
                    batchObject.SETTLEMENT_DATE = DateTime.Now.Date;
                }

                batchObject.SUB_STATUS = "";
                batchObject.STATUS = status;
                db.Entry(batchObject).State = EntityState.Modified;
                db.SaveChanges();

                AspNetIdentity.Infrastructure.TransactionContext transactionObj = new AspNetIdentity.Infrastructure.TransactionContext();
                List<PAY_BATCH_TRANSACTION> batchTranObj = db.PAY_BATCH_TRANSACTION.Where(s => s.PAY_BATCH_ID == batchObject.ID).ToList();

                for (int j = 0; j < batchTranObj.Count(); j++)
                {
                    int batchTranID = (int)batchTranObj[j].TRANSACTION_ID;
                    PAY_TRANSACTION payTran = transactionObj.PAY_TRANSACTION.Include(s => s.CMN_INSTITUTE).Where(s => s.ID == batchTranID).FirstOrDefault();
                    payTran.STATUS = status;

                    if (status == "ADMIN_POST")
                    {
                        AspNetIdentityContext Studentcontext = new AspNetIdentityContext();
                        ApplicationDbContext context = new ApplicationDbContext();
                        string userid = payTran.USER_ID;
                        ApplicationUser user = context.Users.Where(s => s.Id == userid).FirstOrDefault();

                        if (user != null)
                        {
                            string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/TransactionConfirmEmail.html"));
                            Body = Body.Replace("#NAME#", user.FirstName + " " + user.LastName);
                            Body = Body.Replace("#PAYMENTID#", payTran.PREFIX);
                            Body = Body.Replace("#INSTITUTE#", payTran.CMN_INSTITUTE.INSTITUTE_NAME);
                            Body = Body.Replace("#LINK#", ConfigurationManager.AppSettings["mainlink"].ToString());

                            EMAIL_QUEUE eq = new EMAIL_QUEUE();
                            eq.SUBJECT = "Your payment to " + payTran.CMN_INSTITUTE.INSTITUTE_NAME + " has been funded";
                            eq.BODY = Body;
                            eq.DATE_CREATED = DateTime.Now;
                            eq.EMAIL_TO = user.Email;

                            Studentcontext.Entry(eq).State = EntityState.Added;
                            Studentcontext.EMAIL_QUEUE.Add(eq);
                            Studentcontext.SaveChanges();
                        }
                    }

                    transactionObj.Entry(payTran).State = EntityState.Modified;
                    transactionObj.SaveChanges();
                }

                return Ok(formingFunc("success"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("api/BatchSubStatusChange")]
        public IHttpActionResult BatchSubStatusChange([FromUri] int batchID, [FromUri] string status)
        {
            try{
                if (batchID == 0)
                {
                    return NotFound();
                }

                PAY_BATCH batchObject = db.PAY_BATCH.Where(s => s.ID == batchID).FirstOrDefault();

                batchObject.SUB_STATUS = status;
                db.Entry(batchObject).State = EntityState.Modified;
                db.SaveChanges();

                AspNetIdentity.Infrastructure.TransactionContext transactionObj = new AspNetIdentity.Infrastructure.TransactionContext();
                List<PAY_BATCH_TRANSACTION> batchTranObj = db.PAY_BATCH_TRANSACTION.Where(s => s.PAY_BATCH_ID == batchObject.ID).ToList();

                for (int j = 0; j < batchTranObj.Count(); j++)
                {
                    int batchTranID = (int)batchTranObj[j].TRANSACTION_ID;
                    PAY_TRANSACTION payTran = transactionObj.PAY_TRANSACTION.Where(s => s.ID == batchTranID).FirstOrDefault();
                    payTran.SUB_STATUS = status;
                    transactionObj.Entry(payTran).State = EntityState.Modified;
                    transactionObj.SaveChanges();
                }

                return Ok(formingFunc("success"));
             }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class BatchModel
        {
            public int ID { get; set; }
            public string RefNo { get; set; }
            public int InstituteID { get; set; }
            public CMN_INSTITUTE CMN_INSTITUTE { get; set; }
            public string SubStatus { get; set; }
            public double TotalAmount { get; set; }
            public DateTime DateCreated { get; set; }
            public int TotalCount { get; set; }
            public string Status { get; set; }
            public string Currency { get; set; }
            public DateTime SettlementDate { get; set; }
        }

        public forming formingFunc(string state)
        {
            return new forming
            {
                status = state
            };
        }

        public class forming
        {
            public string status { get; set; }
        }

        // PUT api/Batch/5
        //public IHttpActionResult PutPAY_BATCH(int id, PAY_BATCH pay_batch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != pay_batch.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(pay_batch).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PAY_BATCHExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST api/Batch

        [Authorize(Roles = "Admin, SuperAdmin")]
        [ResponseType(typeof(PAY_BATCH))]
        public IHttpActionResult PostPAY_BATCH(PAY_BATCH pay_batch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            pay_batch.STATUS = "ADMIN_UNPOST";
            pay_batch.CREATED_ON = DateTime.Now;
            pay_batch.UPDATED_ON = DateTime.Now;
            pay_batch.CREATED_BY = User.Identity.GetUserId();
            pay_batch.UPDATED_BY = User.Identity.GetUserId();

            db.PAY_BATCH.Add(pay_batch);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pay_batch.ID }, pay_batch);
        }

        //// DELETE api/Batch/5
        //[ResponseType(typeof(PAY_BATCH))]
        //public IHttpActionResult DeletePAY_BATCH(int id)
        //{
        //    PAY_BATCH pay_batch = db.PAY_BATCH.Find(id);
        //    if (pay_batch == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PAY_BATCH.Remove(pay_batch);
        //    db.SaveChanges();

        //    return Ok(pay_batch);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PAY_BATCHExists(int id)
        {
            return db.PAY_BATCH.Count(e => e.ID == id) > 0;
        }
    }
}