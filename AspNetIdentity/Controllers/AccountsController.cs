﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using iTextSharp.text.pdf;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using AspNetIdentity.Providers;
using Microsoft.Owin;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/accounts")]
    public class AccountsController : BaseApiController
    {
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //To store extension of file/image uploaded
        public static string extension = "";

        ApplicationDbContext db = new ApplicationDbContext();
        public AspNetIdentity.Infrastructure.TransactionContext context = new AspNetIdentity.Infrastructure.TransactionContext();
      
        //Get all users
        [Authorize(Roles = "Admin")]
        [Route("users")]
        [HttpGet]
        public IHttpActionResult GetUsers()
        {
            return Ok(this.AppUserManager.Users.ToList().Select(u => this.TheModelFactory.Create(u)));
        }

        [Authorize(Roles = "Admin")]
        [Route("getNewUsers")]
        [HttpGet]
        public IHttpActionResult getNewUsers([FromUri] int instituteid)
        {
            //IQueryable<ApplicationUser> ApplicationUserObject = null;
            int count = 0;
            var roles = this.AppRoleManager.Roles.Where(s => s.Name == "Visitor").FirstOrDefault();
           
            var usersList = new List<ApplicationUser>();
            if (instituteid == 0)
            {
                usersList = db.Users.Where(s=>s.Roles.FirstOrDefault().RoleId == roles.Id).ToList();
            }
            else
            {
                usersList = db.Users.Where(s => s.InstituteID == instituteid && s.Roles.FirstOrDefault().RoleId == roles.Id).ToList();
            }

            var tranList = context.PAY_TRANSACTION.GroupBy(x => x.USER_ID).Select(y => y.FirstOrDefault()).Distinct().ToList();

            var finalUsersList = new List<ApplicationUser>();

            for (int i = 0; i < usersList.Count(); i++)
            {
                var checkUser = tranList.Where(s => s.USER_ID == usersList[i].Id).FirstOrDefault();
                if (checkUser == null)
                {
                    AspNetIdentityContext notesContext = new AspNetIdentityContext();
                    string str =usersList[i].Id;
                    AspNetUser_NOTES AspNetUser_NOTES = notesContext.AspNetUser_NOTES.Where(s => s.USER_ID == str).FirstOrDefault();

                    if (AspNetUser_NOTES != null)
                    {
                        usersList[i].Level = 1;
                    }
                    else
                    {
                        usersList[i].Level = 0;
                    }

                    finalUsersList.Add(usersList[i]);
                }
            }

            count = finalUsersList.Count;
            finalUsersList = finalUsersList.OrderByDescending(s => s.JoinDate).ToList();

            return Ok(formatObject(count, finalUsersList));
        }

        public formatingClass formatObject(int COUNT, List<ApplicationUser> tranObject)
        {
            return new formatingClass
            {
                total_count = COUNT,
                data = tranObject
            };
        }

        public class formatingClass
        {
            public int total_count { get; set; }
            public List<ApplicationUser> data { get; set; }
        }

        //Get Institute Users
        [Authorize(Roles = "Admin")]
        [Route("instituteUsers")]
        [HttpGet]
        public IHttpActionResult GetUsers([FromUri] int instituteid)
        {
            InstituteContext instituteContext = new InstituteContext();
            
            if (instituteid == 0)
            {
                List<userModel> userModelList = new List<userModel>();

                var roles = this.AppRoleManager.Roles.Where(s => s.Name == "Visitor").FirstOrDefault();
                List<ApplicationUser> appUserList = this.AppUserManager.Users.Where(s => s.Roles.FirstOrDefault().RoleId == roles.Id).ToList();

                for (int i = 0; i < appUserList.Count; i++)
                {
                    userModel userModelObj = new userModel();
                    userModelObj.User = appUserList[i];

                    int eyeD = appUserList[i].InstituteID;

                    CMN_INSTITUTE instituteObj = instituteContext.CMN_INSTITUTE.Where(s => s.ID == eyeD).FirstOrDefault();

                    userModelObj.INSTITUTE_NAME = instituteObj.INSTITUTE_NAME;

                    userModelList.Add(userModelObj);
                }

                return Ok(userModelList);
            }
            else
            {
                List<userModel> userModelList = new List<userModel>();

                var roles = this.AppRoleManager.Roles.Where(s => s.Name == "Visitor").FirstOrDefault();
                List<ApplicationUser> appUserList = this.AppUserManager.Users.Where(s => s.InstituteID == instituteid && s.Roles.FirstOrDefault().RoleId == roles.Id).ToList();

                for (int i = 0; i < appUserList.Count; i++)
                {
                    userModel userModelObj = new userModel();
                    userModelObj.User = appUserList[i];

                    int eyeD = appUserList[i].InstituteID;

                    CMN_INSTITUTE instituteObj = instituteContext.CMN_INSTITUTE.Where(s => s.ID == eyeD).FirstOrDefault();

                    userModelObj.INSTITUTE_NAME = instituteObj.INSTITUTE_NAME;

                    userModelList.Add(userModelObj);
                }

                return Ok(userModelList);
            }
        }

        public class userModel
        {
            public ApplicationUser User { get; set; }
            public string INSTITUTE_NAME { get; set; }
        }

        //To get all the institutes from token at the login of university admin
        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [Route("getUserInstitutes")]
        [HttpGet]
        public IHttpActionResult getUserInstitutes()
        {
            try
            {
                string UserID = User.Identity.GetUserId();
                ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                ApplicationUser ApplicationUserObj = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                string userEmail = ApplicationUserObj.Email;
                List<ApplicationUser> ApplicationUserList = ApplicationDbContext.Users.Where(s => s.Email == userEmail).ToList();

                if (ApplicationUserList.Count() == 0)
                {
                    return BadRequest();
                }

                InstituteContext instituteContext = new InstituteContext();

                List<CMN_INSTITUTE> instituteList = new List<CMN_INSTITUTE>();

                for (int i = 0; i < ApplicationUserList.Count(); i++)
                {
                    int ID = ApplicationUserList[i].InstituteID;
                    CMN_INSTITUTE CMN_INSTITUTEObj = instituteContext.CMN_INSTITUTE.Where(s => s.ID == ID).FirstOrDefault();
                    instituteList.Add(CMN_INSTITUTEObj);
                }

                return Ok(instituteList);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("checkDate")]
        [HttpGet]
        public IHttpActionResult checkDate()
        {
            try
            {
                DateTime dt = DateTime.Now;
                //dt = dt.AddDays(2);
                for (int i = 0; i < 5; i++)
                {
                    dt = dt.AddDays(1);

                    if (dt.DayOfWeek.ToString().ToLower() == "saturday")
                    {
                        dt = dt.AddDays(2);
                    }

                    if (dt.DayOfWeek.ToString().ToLower() == "sunday")
                    {
                        dt = dt.AddDays(1);
                    }
                }
                
                return Ok(dt);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("currentDate")]
        [HttpGet]
        public IHttpActionResult currentDate()
        {
            try
            {
                DateTime dt = DateTime.Now;
                string str = dt.ToString("yyyyMMddHHmmss");

                return Ok(str);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //When University admin changes university this method generates new token
        //[Route("getGeoSwiftLogin")]
        //[HttpGet]
        //public async Task<IHttpActionResult> getGeoSwiftLogin([FromUri] int transactionID)
        //{
        //    try
        //    {
        //        AspNetIdentity.Infrastructure.TransactionContext tranContext = new AspNetIdentity.Infrastructure.TransactionContext();
        //        PAY_TRANSACTION PAY_TRANSACTIONObj = tranContext.PAY_TRANSACTION.Where(s => s.ID == transactionID).FirstOrDefault();

        //        if (PAY_TRANSACTIONObj == null)
        //        {
        //            return BadRequest();
        //        }

        //        string UserID = PAY_TRANSACTIONObj.USER_ID;
        //        ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
        //        ApplicationUser AppUserObj = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
        //        ApplicationUser ApplicationUserObj = ApplicationDbContext.Users.Where(s => s.Email == AppUserObj.Email && s.InstituteID == PAY_TRANSACTIONObj.CMN_INSTITUTE_ID).FirstOrDefault();

        //        if (ApplicationUserObj == null)
        //        {
        //            return BadRequest();
        //        }

        //        ApplicationUserManager ApplicationUserManagerObj = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

        //        ClaimsIdentity oAuthIdentity = await ApplicationUserObj.GenerateUserIdentityAsync(ApplicationUserManagerObj, "JWT");
        //        oAuthIdentity.AddClaims(ExtendedClaimsProvider.GetClaims(ApplicationUserObj));
        //        oAuthIdentity.AddClaims(RolesFromClaims.CreateRolesBasedOnClaims(oAuthIdentity));

        //        InstituteContext instituteContext = new InstituteContext();
        //        CMN_INSTITUTE CMN_INSTITUTEMethod = instituteContext.CMN_INSTITUTE.Where(s => s.ID == ApplicationUserObj.InstituteID).FirstOrDefault();

        //        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        //        dictionary.Add("id", ApplicationUserObj.Id);
        //        dictionary.Add("firstname", ApplicationUserObj.FirstName);
        //        dictionary.Add("lastname", ApplicationUserObj.LastName);
        //        dictionary.Add("email", ApplicationUserObj.Email);
        //        dictionary.Add("joinDate", ApplicationUserObj.JoinDate.ToString());
        //        dictionary.Add("instituteID", ApplicationUserObj.InstituteID.ToString());
        //        if (CMN_INSTITUTEMethod != null)
        //        {
        //            dictionary.Add("slug", CMN_INSTITUTEMethod.INSTITUTE_SLUG);
        //        }
        //        else
        //        {
        //            dictionary.Add("slug", "");
        //        }

        //        dictionary.Add("phonenumber", ApplicationUserObj.PhoneNumber.ToString());

        //        var props = new AuthenticationProperties(dictionary);
        //        props.IssuedUtc = DateTime.UtcNow;
        //        props.ExpiresUtc = DateTime.UtcNow.AddDays(365);
        //        var ticket = new AuthenticationTicket(oAuthIdentity, props);

        //        OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
        //        {
        //            //For Dev enviroment only (on production should be AllowInsecureHttp = false)
        //            AllowInsecureHttp = true,
        //            TokenEndpointPath = new PathString("/oauth/token"),
        //            AccessTokenExpireTimeSpan = TimeSpan.FromDays(365),
        //            Provider = new CustomOAuthProvider(),
        //            AccessTokenFormat = new CustomJwtFormat(ConfigurationManager.AppSettings["WebsiteLink"].ToString())
        //        };

        //        var accessToken = OAuthServerOptions.AccessTokenFormat.Protect(ticket);

        //        tokenResponse tokenResponseObj = new tokenResponse();
        //        tokenResponseObj.access_token = accessToken;
        //        tokenResponseObj.id = ApplicationUserObj.Id;
        //        tokenResponseObj.firstname = ApplicationUserObj.FirstName;
        //        tokenResponseObj.lastname = ApplicationUserObj.LastName;
        //        tokenResponseObj.email = ApplicationUserObj.Email;
        //        tokenResponseObj.joinDate = ApplicationUserObj.JoinDate;
        //        tokenResponseObj.instituteID = ApplicationUserObj.InstituteID;
        //        tokenResponseObj.slug = CMN_INSTITUTEMethod.INSTITUTE_SLUG;
        //        tokenResponseObj.phonenumber = ApplicationUserObj.PhoneNumber;

        //        return Ok(tokenResponseObj);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Debug(ex.Message);
        //        return BadRequest(ex.Message);
        //    }
        //}

        //When University admin changes university this method generates new token
        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [Route("getInstituteToken")]
        [HttpGet]
        public async Task<IHttpActionResult> getInstituteToken([FromUri] int instituteID)
        {
            try
            {
                string UserID = User.Identity.GetUserId();
                ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                ApplicationUser AppUserObj = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                ApplicationUser ApplicationUserObj = ApplicationDbContext.Users.Where(s => s.Email == AppUserObj.Email && s.InstituteID == instituteID).FirstOrDefault();

                if (ApplicationUserObj == null)
                {
                    return BadRequest();
                }

                ApplicationUserManager ApplicationUserManagerObj = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

                ClaimsIdentity oAuthIdentity = await ApplicationUserObj.GenerateUserIdentityAsync(ApplicationUserManagerObj, "JWT");
                oAuthIdentity.AddClaims(ExtendedClaimsProvider.GetClaims(ApplicationUserObj));
                oAuthIdentity.AddClaims(RolesFromClaims.CreateRolesBasedOnClaims(oAuthIdentity));

                InstituteContext instituteContext = new InstituteContext();
                CMN_INSTITUTE CMN_INSTITUTEMethod = instituteContext.CMN_INSTITUTE.Where(s => s.ID == ApplicationUserObj.InstituteID).FirstOrDefault();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("id", ApplicationUserObj.Id);
                dictionary.Add("firstname", ApplicationUserObj.FirstName);
                dictionary.Add("lastname", ApplicationUserObj.LastName);
                dictionary.Add("email", ApplicationUserObj.Email);
                dictionary.Add("joinDate", ApplicationUserObj.JoinDate.ToString());
                dictionary.Add("instituteID", ApplicationUserObj.InstituteID.ToString());
                if (CMN_INSTITUTEMethod != null)
                {
                    dictionary.Add("slug", CMN_INSTITUTEMethod.INSTITUTE_SLUG);
                }
                else
                {
                    dictionary.Add("slug", "");
                }

                dictionary.Add("phonenumber", ApplicationUserObj.PhoneNumber.ToString());

                var props = new AuthenticationProperties(dictionary);
                props.IssuedUtc = DateTime.UtcNow;
                props.ExpiresUtc = DateTime.UtcNow.AddDays(365);
                var ticket = new AuthenticationTicket(oAuthIdentity, props);

                OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
                {
                    //For Dev enviroment only (on production should be AllowInsecureHttp = false)
                    AllowInsecureHttp = true,
                    TokenEndpointPath = new PathString("/oauth/token"),
                    AccessTokenExpireTimeSpan = TimeSpan.FromDays(365),
                    Provider = new CustomOAuthProvider(),
                    AccessTokenFormat = new CustomJwtFormat(ConfigurationManager.AppSettings["WebsiteLink"].ToString())
                };

                var accessToken = OAuthServerOptions.AccessTokenFormat.Protect(ticket);

                tokenResponse tokenResponseObj = new tokenResponse();
                tokenResponseObj.access_token = accessToken;
                tokenResponseObj.id = ApplicationUserObj.Id;
                tokenResponseObj.firstname = ApplicationUserObj.FirstName;
                tokenResponseObj.lastname = ApplicationUserObj.LastName;
                tokenResponseObj.email = ApplicationUserObj.Email;
                tokenResponseObj.joinDate = ApplicationUserObj.JoinDate;
                tokenResponseObj.instituteID = ApplicationUserObj.InstituteID;
                tokenResponseObj.slug = CMN_INSTITUTEMethod.INSTITUTE_SLUG;
                tokenResponseObj.phonenumber = ApplicationUserObj.PhoneNumber;

                return Ok(tokenResponseObj);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class tokenResponse
        {
            public string access_token { get; set; }
            public string id { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public DateTime joinDate { get; set; }
            public int instituteID { get; set; }
            public string slug { get; set; }
            public string phonenumber { get; set; }
        }

        //Updates Institute User
        [Authorize]
        [Route("update")]
        [HttpPost]
        public IHttpActionResult USERUPDATE(CreateUserUpdateBindingModel userModel)
        {
            ApplicationUser model = AppUserManager.FindById(User.Identity.GetUserId());
          
            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

            if (userModel.OldPassword != null)
            {
                var user = userManager.FindAsync(model.UserName, userModel.OldPassword).Result;

                if (user == null)
                {
                    return BadRequest("Email Address or Old password is incorrect!");
                }
            }

            ApplicationUser userObj = AppUserManager.Users.Where(s => s.Email == userModel.Email).FirstOrDefault();

            if (userObj != null)
            {
                if (userObj.Email != model.Email)
                {
                    return BadRequest("An account with this email address already exists.  Please enter a different email address to continue.  If you are a returning user please sign-in before proceeding.");
                }
            }

            model.Email = userModel.Email;
            model.FirstName = userModel.FirstName;
            model.LastName = userModel.LastName;
            model.PhoneNumber = userModel.PhoneNumber;

            if (userModel.NewPassword != null)
            {
                model.PasswordHash = AppUserManager.PasswordHasher.HashPassword(userModel.NewPassword);
            }

            ApplicationDbContext db = new ApplicationDbContext();

            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                AspNetIdentity.Infrastructure.TransactionContext trandb = new AspNetIdentity.Infrastructure.TransactionContext();
                TransactionDetailContext trandetaildb = new TransactionDetailContext();

                PAY_TRANSACTION PAY_TRANSACTIONObj = trandb.PAY_TRANSACTION.Include(s=>s.PAY_TRANSACTION_DETAIL).OrderByDescending(s => s.CREATED_ON).Where(s => s.USER_ID == model.Id).FirstOrDefault();
                PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILObj = trandetaildb.PAY_TRANSACTION_DETAIL.Where(s=>s.PAY_TRANSACTION_ID == PAY_TRANSACTIONObj.ID).FirstOrDefault();

                if (PAY_TRANSACTIONObj != null)
                {
                    PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME = model.FirstName;
                    PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME = model.LastName;
                    PAY_TRANSACTION_DETAILObj.PAYEE_EMAIL = model.Email;
                    PAY_TRANSACTION_DETAILObj.PAYEE_PHONE_NUMBER = model.PhoneNumber;

                    if (PAY_TRANSACTION_DETAILObj.IS_PAYEE_SAME == true)
                    {
                        PAY_TRANSACTION_DETAILObj.STUDENT_FIRST_NAME = model.FirstName;
                        PAY_TRANSACTION_DETAILObj.STUDENT_LAST_NAME = model.LastName;
                        PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL = model.Email;
                    }

                    PAY_TRANSACTION_DETAILObj.UPDATED_BY = model.Id;
                    PAY_TRANSACTION_DETAILObj.UPDATED_ON = DateTime.Now;

                    trandetaildb.Entry(PAY_TRANSACTION_DETAILObj).State = EntityState.Modified;
                    trandetaildb.SaveChanges();
                }

                return Content(HttpStatusCode.OK, model);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return Content(HttpStatusCode.OK, function("failure"));
            }
        }

        //update university user from university portal
        [Route("updateUniversityUser")]
        [HttpPost]
        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        public IHttpActionResult updateUniversityUser(CreateUniversityUserUpdateBindingModel userModel)
        {
            ApplicationUser userObj = AppUserManager.Users.Where(s => s.Id == userModel.ID).FirstOrDefault();

            ApplicationUser checkEmail = AppUserManager.Users.Where(s => s.Email == userModel.Email && s.InstituteID == userModel.InstituteID).FirstOrDefault();

            if (checkEmail != null)
            {
                if (checkEmail.Email != userObj.Email)
                {
                    return BadRequest("Email Exist!");
                }
            }

            userObj.Email = userModel.Email;
            userObj.FirstName = userModel.FirstName;
            userObj.LastName = userModel.LastName;
            userObj.PhoneNumber = userModel.PhoneNumber;
            userObj.PasswordHash = AppUserManager.PasswordHasher.HashPassword(userModel.NewPassword);

            db.Entry(userObj).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                return Content(HttpStatusCode.OK, userObj);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return Content(HttpStatusCode.OK, function("failure"));
            }
        }

        //update university user from university portal
        [Route("deleteNewUsers")]
        [HttpGet]
        [Authorize(Roles = "Admin, SuperAdmin")]
        public async Task<IHttpActionResult> deleteNewUsers([FromUri] string UserID)
        {
            ApplicationUser userObj = AppUserManager.Users.Where(s => s.Id == UserID).FirstOrDefault();

            if (userObj != null)
            {
                AspNetIdentityContext cancelContext = new AspNetIdentityContext();
                List<AspNetUser_NOTES> AspNetUser_NOTESList = cancelContext.AspNetUser_NOTES.Where(s => s.USER_ID == UserID).ToList();

                for (int i = 0; i < AspNetUser_NOTESList.Count(); i++)
                {
                    int ID = AspNetUser_NOTESList[i].ID;
                    AspNetUser_NOTES AspNetUser_NOTESObj = cancelContext.AspNetUser_NOTES.Where(s => s.ID == ID).FirstOrDefault();
                    cancelContext.Entry(AspNetUser_NOTESObj).State = EntityState.Deleted;
                    cancelContext.SaveChanges();
                }

                IdentityResult result = await this.AppUserManager.DeleteAsync(userObj);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                return Ok();
            }
           
            return NotFound();
        }

        public sucessJSONFormating function(string firstParam)
        {
            return new sucessJSONFormating
            {
                status = firstParam
            };
        }

        public class sucessJSONFormating
        {
            public string status { get; set; }
        }

        public class CreateUserUpdateBindingModel
        {
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Display(Name = "Institute ID")]
            public int InstituteID { get; set; }

            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Display(Name = "phone Number")]
            public string PhoneNumber { get; set; }

            [Display(Name = "Old Password")]
            public string OldPassword { get; set; }

            [Display(Name = "New Password")]
            public string NewPassword { get; set; }

            [Display(Name = "Confirm Password")]
            public string ConfirmPassword { get; set; }
        }

        public class CreateUniversityUserUpdateBindingModel
        {
            [Display(Name = "ID")]
            public string ID { get; set; }

            [Display(Name = "Email")]
            public string Email { get; set; }

             [Display(Name = "Institute ID")]
            public int InstituteID { get; set; }

            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Display(Name = "phone Number")]
            public string PhoneNumber { get; set; }

            [Display(Name = "New Password")]
            public string NewPassword { get; set; }
        }

        [Route("instituteUserDetail")]
        [Authorize]
        [HttpGet]
        public async Task<IHttpActionResult> instituteUserDetail([FromUri] string id)
        {
            try
            {
                var user = await this.AppUserManager.FindByIdAsync(id);

                IQueryable<PAY_TRANSACTION> transactionObject = context.PAY_TRANSACTION.Include(y => y.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).Where(s => s.USER_ID == user.Id);

                USER_DETAIL USER_DETAILObj = new USER_DETAIL();

                USER_DETAILObj.USER = this.TheModelFactory.Create(user);
                USER_DETAILObj.PAY_TRANSACTION = transactionObject;

                return Ok(USER_DETAILObj);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class USER_DETAIL
        {
            public UserReturnModel USER { get; set; }
            public IQueryable<PAY_TRANSACTION> PAY_TRANSACTION { get; set; }
        }

        //Get User by ID
        [Authorize]
        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string Id)
        {
            try{
                var user = await this.AppUserManager.FindByIdAsync(Id);

                if (user != null)
                {
                    return Ok(this.TheModelFactory.Create(user));
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //Get University User
        [Authorize]
        [Route("GetUniversityUser")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUniversityUser([FromUri] string Id)
        {
            try{
                var user = await this.AppUserManager.FindByIdAsync(Id);

                if (user != null)
                {
                    return Ok(user);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //To get detail of specific user using token
        [Authorize]
        [Route("userDetail")]
        public async Task<IHttpActionResult> GetUserDetail()
        {
            try
            {
                var userid = User.Identity.GetUserId();
                Log.Debug(userid);

                var user = await this.AppUserManager.FindByIdAsync(userid);
                Log.Debug(user.Email);

                if (user != null)
                {
                    return Ok(this.TheModelFactory.Create(user));
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //Forgot Password email will be generated for users
        [Route("forgotPassword")]
        [HttpGet]
        public async Task<IHttpActionResult> forgotPassword([FromUri] string email)
        {
            try
            {
                InstituteContext db = new InstituteContext();
                ApplicationUser CheckUser = this.AppUserManager.Users.Where(s => s.Email == email).FirstOrDefault();
                CMN_INSTITUTE CMN_INSTITUTEObj = db.CMN_INSTITUTE.Where(s => s.ID == CheckUser.InstituteID).FirstOrDefault();

                if (CheckUser == null)
                {
                    return NotFound();
                }

                if (CMN_INSTITUTEObj == null)
                {
                    return BadRequest("Institute Not Found");
                }

                string code = Guid.NewGuid().ToString();

                RESET_QUEUE RESET_QUEUEObj = new RESET_QUEUE();
                RESET_QUEUEObj.USER_ID = CheckUser.Id;
                RESET_QUEUEObj.TOKEN = code;
                RESET_QUEUEObj.INSTITUTE_ID = CMN_INSTITUTEObj.ID;
                RESET_QUEUEObj.EXPIRY_DATE = DateTime.Now.AddDays(1);

                AspNetIdentityContext context = new AspNetIdentityContext();
                context.Entry(RESET_QUEUEObj).State = EntityState.Added;
                context.SaveChanges();

                //int _min = 10000000;
                //int _max = 99999999;
                //Random _rdm = new Random();
                //string password = _rdm.Next(_min, _max).ToString();

                //CheckUser.PasswordHash = AppUserManager.PasswordHasher.HashPassword(password);
                //var result = await AppUserManager.UpdateAsync(CheckUser);
                string link = ConfigurationManager.AppSettings["websitelink"].ToString();
                string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/ForgotPassword.html"));
                Body = Body.Replace("#LINK#", link + "" + CMN_INSTITUTEObj.INSTITUTE_SLUG + "/" + code);

                //string body = "We received a request to reset your password for your Paymytuition account.<br /> To reset your password, click this secure link <a href=\"" + link + "" + code + "\">here</a> or copy and paste the URL below into your browser:<br />" + link + "<br /> If you didn’t mean to reset your password, then you can ignore this email – your password will not change.<br /><br />"; 
                //bool state = SendEmail.SendingEmail("MTFX Password recovery", body, ConfigurationManager.AppSettings["email"].ToString(), CheckUser.Email);
                bool state = SendEmail.SendingEmail("Paymytuition Password Reset", Body, ConfigurationManager.AppSettings["email"].ToString(), CheckUser.Email);
                return Ok(formingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //Forgot Password email will be generated for university users
        [Route("forgotUniversityPassword")]
        [HttpGet]
        public async Task<IHttpActionResult> forgotUniversityPassword([FromUri] string email)
        {
            try{
                ApplicationUser CheckUser = this.AppUserManager.Users.Where(s => s.Email == email).FirstOrDefault();

                if (CheckUser == null)
                {
                    return NotFound();
                }

                string code = Guid.NewGuid().ToString();

                RESET_QUEUE RESET_QUEUEObj = new RESET_QUEUE();
                RESET_QUEUEObj.USER_ID = CheckUser.Id;
                RESET_QUEUEObj.TOKEN = code;
                RESET_QUEUEObj.INSTITUTE_ID = -1;
                RESET_QUEUEObj.EXPIRY_DATE = DateTime.Now.AddDays(1);

                AspNetIdentityContext context = new AspNetIdentityContext();
                context.Entry(RESET_QUEUEObj).State = EntityState.Added;
                context.SaveChanges();

                //int _min = 10000000;
                //int _max = 99999999;
                //Random _rdm = new Random();
                //string password = _rdm.Next(_min, _max).ToString();

                //CheckUser.PasswordHash = AppUserManager.PasswordHasher.HashPassword(password);
                //var result = await AppUserManager.UpdateAsync(CheckUser);
                string link = ConfigurationManager.AppSettings["universitywebsitelink"].ToString();
                string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/ForgotPassword.html"));
                Body = Body.Replace("#LINK#", link + "" + code);

                //string body = "We received a request to reset your password for your Paymytuition account.<br /> To reset your password, click this secure link <a href=\"" + link + "" + code + "\">here</a> or copy and paste the URL below into your browser:<br />" + link + "<br /> If you didn’t mean to reset your password, then you can ignore this email – your password will not change.<br /><br />"; 
                //bool state = SendEmail.SendingEmail("MTFX Password recovery", body, ConfigurationManager.AppSettings["email"].ToString(), CheckUser.Email);
                bool state = SendEmail.SendingEmail("Paymytuition Password Reset", Body, ConfigurationManager.AppSettings["email"].ToString(), CheckUser.Email);
                return Ok(formingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //To verify token before showing reset password screen url which is routed from forgot password email
        [Route("verifyToken")]
        [HttpGet]
        public async Task<IHttpActionResult> verifyToken([FromUri] string code, [FromUri] int instituteid)
        {
            try{
                AspNetIdentityContext context = new AspNetIdentityContext();
                RESET_QUEUE CheckUser = null;

                CheckUser = context.RESET_QUEUE.Where(s => s.TOKEN == code && s.INSTITUTE_ID == instituteid).FirstOrDefault();

                if (CheckUser == null)
                {
                    return Content(HttpStatusCode.NotFound, "does not exist");
                }

                return Ok(formingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //Change password with code in url and new password
        [Route("changePassword")]
        [HttpGet]
        public async Task<IHttpActionResult> changePassword([FromUri] string code, [FromUri] string password)
        {
            try{
                AspNetIdentityContext context = new AspNetIdentityContext();
                var CheckUser = context.RESET_QUEUE.Where(s=>s.TOKEN == code).FirstOrDefault();

                if (CheckUser == null)
                {
                    return Content(HttpStatusCode.NotFound, "Problem in changing password");
                }

                if (CheckUser.EXPIRY_DATE < DateTime.Now)
                {
                    return Content(HttpStatusCode.NotFound, "Problem in changing password");
                }

                ApplicationUser user = this.AppUserManager.Users.Where(s=>s.Id == CheckUser.USER_ID).FirstOrDefault();

                user.PasswordHash = AppUserManager.PasswordHasher.HashPassword(password);
                var result = await AppUserManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    context.Entry(CheckUser).State = EntityState.Deleted;
                    context.SaveChanges();

                    return Content(HttpStatusCode.OK, "Password successfully changed");
                }
                else
                {
                    return Content(HttpStatusCode.NotFound, "Problem in changing password");
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public forming formingFunc(string state)
        {
            return new forming
            {
                status = state
            };
        }

        public class forming
        {
            public string status { get; set; }
        }

        
        //public async Task<IHttpActionResult> ForgotPassword([FromUri] string email, [FromUri] int instituteID )
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = AppUserManager.Users.Where(s => s.Email == email && s.InstituteID == instituteID).FirstOrDefault();
        //        if (user == null)
        //        {
        //            return NotFound();
        //        }

        //        var code = await AppUserManager.GeneratePasswordResetTokenAsync(user.Id);
        //        var callbackUrl = Url.Link("DefaultApi", code);
        //        await AppUserManager.SendEmailAsync(user.Id, "Reset Password",
        //    "Please reset your password by clicking here: <a href=\"" + callbackUrl + "\">link</a>");
        //    }
        //    return Ok();
        //    // If we got this far, something failed, redisplay form
        //}

        [Authorize(Roles = "Admin")]
        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            try{
                var user = await this.AppUserManager.FindByNameAsync(username);

                if (user != null)
                {
                    return Ok(this.TheModelFactory.Create(user));
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [Route("create")]
        public async Task<IHttpActionResult> CreateUser(CreateUserBindingModel createUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (createUserModel.RoleName.ToLower() == "sub_admin")
            {
                string askerID = User.Identity.GetUserId();
                if (askerID == null)
                {
                    return BadRequest("You dont have access");
                }
               
                var roles = this.AppRoleManager.Roles.Where(s => s.Name == "University").FirstOrDefault();

                var CheckingIdentityUser = AppUserManager.Users.Where(s => s.Id == askerID && s.Roles.Where(l => l.RoleId == roles.Id).FirstOrDefault() != null).FirstOrDefault();

                if (CheckingIdentityUser == null)
                {
                    return BadRequest("You dont have access");
                }
            }

            if (createUserModel.RoleName.ToLower() == "university")
            {
                string askerID = User.Identity.GetUserId();
                if (askerID == null)
                {
                    return BadRequest("You dont have access");
                }

                var roles = this.AppRoleManager.Roles.Where(s => s.Name == "Admin").FirstOrDefault();

                var CheckingIdentityUser = AppUserManager.Users.Where(s => s.Id == askerID && s.Roles.Where(l => l.RoleId == roles.Id).FirstOrDefault() != null).FirstOrDefault();

                if (CheckingIdentityUser == null)
                {
                    return BadRequest("You dont have access");
                }
            }

            ApplicationUser CheckUser = this.AppUserManager.Users.Where(s=> s.Email == createUserModel.Email).FirstOrDefault();

            if (CheckUser != null)
            {
                return BadRequest("An account with this email address already exists. Please enter a different email address to continue.  If you are a returning user please sign-in before proceeding.");
            }

            var asdas = this.AppUserManager.Users.OrderByDescending(s => s.UserName).ToList();

            ApplicationUser asdsas = asdas.OrderByDescending(s => Convert.ToInt32(s.UserName)).FirstOrDefault();

            int temp = Convert.ToInt32(asdsas.UserName) + 1;

            var user = new ApplicationUser()
            {
                UserName = temp.ToString(),
                Email = createUserModel.Email,
                FirstName = createUserModel.FirstName,
                LastName = createUserModel.LastName,
                Level = createUserModel.Level,
                JoinDate = DateTime.Now,
                InstituteID = createUserModel.InstituteID,
                EmailConfirmed = true,
                PhoneNumber = createUserModel.PhoneNumber
            };

            //validation will be done of password, email wagera
            IdentityResult addUserResult = await this.AppUserManager.CreateAsync(user, createUserModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            try
            {
                string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/SignUpEmail.html"));
                Body = Body.Replace("#NAME#", user.FirstName + " " + user.LastName);
                Body = Body.Replace("#LINK#", ConfigurationManager.AppSettings["mainlink"].ToString());

                EMAIL_QUEUE eq = new EMAIL_QUEUE();
                eq.SUBJECT = "Welcome to Paymytuition by MTFX!";
                eq.BODY = Body;
                eq.DATE_CREATED = DateTime.Now;
                eq.EMAIL_TO = user.Email;
                eq.BCC = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

                Studentcontext.Entry(eq).State = EntityState.Added;
                Studentcontext.EMAIL_QUEUE.Add(eq);
                Studentcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }

            string code = await this.AppUserManager.GenerateEmailConfirmationTokenAsync(user.Id);

            //var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute", new { userId = user.Id, code = code }));

            //await this.AppUserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

            Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            if (createUserModel.RoleName.ToLower() == "admin" || createUserModel.RoleName.ToLower() == "superadmin")
            {
                IdentityResult addUserResult2 = await this.AppUserManager.AddToRolesAsync(user.Id, "Visitor");
            }
            else
            {
                IdentityResult addUserResult2 = await this.AppUserManager.AddToRolesAsync(user.Id, createUserModel.RoleName);
            }

            return Created(locationHeader, TheModelFactory.Create(user));
        }

        [Authorize]
        [HttpGet]
        [Route("getSubAdminList")]
        public async Task<IHttpActionResult> getSubAdminList()
        {
            try{
                var userID = User.Identity.GetUserId();

                ApplicationDbContext context = new ApplicationDbContext();
                ApplicationUser user = context.Users.Where(s => s.Id == userID).FirstOrDefault();

                if(user == null)
                {
                    return BadRequest("User not found");
                }

                int instituteID = user.InstituteID;

                var roles = this.AppRoleManager.Roles.Where(s => s.Name == "Sub_Admin").FirstOrDefault();

                return Ok(this.AppUserManager.Users.Where(s => s.InstituteID == instituteID && s.Roles.FirstOrDefault().RoleId == roles.Id).ToList().Select(u => this.TheModelFactory.Create(u)));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous] 
        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        public async Task<IHttpActionResult> ConfirmEmail(string userId = "", string code = "")
        {
            try{
                if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
                {
                    ModelState.AddModelError("", "User Id and Code are required");
                    return BadRequest(ModelState);
                }

                IdentityResult result = await this.AppUserManager.ConfirmEmailAsync(userId, code);

                if (result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return GetErrorResult(result);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            try{
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                IdentityResult result = await this.AppUserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //[HttpDelete]
        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}")]
        public async Task<IHttpActionResult> DeleteUser(string id)
        {
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
            try{
                var appUser = await this.AppUserManager.FindByIdAsync(id);

                if (appUser != null)
                {
                    IdentityResult result = await this.AppUserManager.DeleteAsync(appUser);

                    if (!result.Succeeded)
                    {
                        return GetErrorResult(result);
                    }

                    return Ok();
                }
           
                return NotFound();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //This method can be accessed only by authenticated users who belongs to “Admin” role, that’s why we have added the attribute [Authorize(Roles=”Admin”)]
        //The method accepts the UserId in its URI and array of the roles this user Id should be enrolled in.
        //The method will validates that this array of roles exists in the system, if not, HTTP Bad response will be sent indicating which roles doesn’t exist.
        //The system will delete all the roles assigned for the user then will assign only the roles sent in the request.

        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}/roles")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignRolesToUser([FromUri] string id, [FromBody] string[] rolesToAssign)
        {
            try{
                var appUser = await this.AppUserManager.FindByIdAsync(id);

                if (appUser == null)
                {
                    return NotFound();
                }

                var currentRoles = await this.AppUserManager.GetRolesAsync(appUser.Id);

                var rolesNotExists = rolesToAssign.Except(this.AppRoleManager.Roles.Select(x => x.Name)).ToArray();

                if (rolesNotExists.Count() > 0)
                {
                    ModelState.AddModelError("", string.Format("Roles '{0}' does not exixts in the system", string.Join(",", rolesNotExists)));
                    return BadRequest(ModelState);
                }

                IdentityResult removeResult = await this.AppUserManager.RemoveFromRolesAsync(appUser.Id, currentRoles.ToArray());

                if (!removeResult.Succeeded)
                {
                    ModelState.AddModelError("", "Failed to remove user roles");
                    return BadRequest(ModelState);
                }

                IdentityResult addResult = await this.AppUserManager.AddToRolesAsync(appUser.Id, rolesToAssign);

                if (!addResult.Succeeded)
                {
                    ModelState.AddModelError("", "Failed to add user roles");
                    return BadRequest(ModelState);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}/assignclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignClaimsToUser([FromUri] string id, [FromBody] List<ClaimBindingModel> claimsToAssign)
        {
            try{
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var appUser = await this.AppUserManager.FindByIdAsync(id);

                if (appUser == null)
                {
                    return NotFound();
                }

                foreach (ClaimBindingModel claimModel in claimsToAssign)
                {
                    if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                    {
                        await this.AppUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                    }

                    await this.AppUserManager.AddClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}/removeclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> RemoveClaimsFromUser([FromUri] string id, [FromBody] List<ClaimBindingModel> claimsToRemove)
        {
            try{
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var appUser = await this.AppUserManager.FindByIdAsync(id);

                if (appUser == null)
                {
                    return NotFound();
                }

                foreach (ClaimBindingModel claimModel in claimsToRemove)
                {
                    if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                    {
                        await this.AppUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // upload image s3
        [Route("uploadImage")]
        public async Task<IHttpActionResult> PostFile()
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                if (Request.Content.IsMimeMultipartContent())
                {
                    String UUID = Guid.NewGuid().ToString();

                    Request.Content.LoadIntoBufferAsync().Wait();
                    await Request.Content.ReadAsMultipartAsync<MultipartMemoryStreamProvider>(new MultipartMemoryStreamProvider()).ContinueWith((task) =>
                    {
                        MultipartMemoryStreamProvider provider = task.Result;
                        foreach (HttpContent content in provider.Contents)
                        {
                            if (content.Headers.ContentType.ToString() == "image/png")
                            {
                                UUID = UUID + ".png";
                            }
                            else if (content.Headers.ContentType.ToString() == "image/jpg")
                            {
                                UUID = UUID + ".jpg";
                            }
                            else if (content.Headers.ContentType.ToString() == "image/jpeg")
                            {
                                UUID = UUID + ".jpeg";
                            }
                            else if (content.Headers.ContentType.ToString() == "image/gif")
                            {
                                UUID = UUID + ".gif";
                            }
                            else
                            {
                                UUID = UUID + ".png";
                            }

                            Stream stream = content.ReadAsStreamAsync().Result;
                            Image image = Image.FromStream(stream);
                            var testName = content.Headers.ContentDisposition.Name;
                            String filePath = HostingEnvironment.MapPath("~/images");
                            String fileName = content.Headers.ContentDisposition.FileName;
                            String fullPath = Path.Combine(filePath, UUID);
                            image.Save(fullPath, ImageFormat.Png);
                        }
                    });


                    string Filepath = HostingEnvironment.MapPath("~/images/"+UUID);

                    //IAmazonS3 client;
                    //client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1);
                    //PutObjectRequest request = new PutObjectRequest()
                    //{
                    //    BucketName = ConfigurationManager.AppSettings["AWSBucket"].ToString() + "/Images",
                    //    Key = UUID,
                    //    FilePath = Filepath
                    //};
                    //PutObjectResponse response2 = client.PutObject(request);

                    //if (response2.HttpStatusCode == HttpStatusCode.OK)
                    //{
                    return Ok(ConfigurationManager.AppSettings["mainlink"].ToString() + "/server/images/" + UUID);
                   // }

                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted"));
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted"));
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted"));
            }
        }

        [HttpPost]
        [Route("deleteImage")]
        public IHttpActionResult DeleteFile(deleteImageBindingModel deleteImageModel)
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                IAmazonS3 client;
                client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1);

                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = ConfigurationManager.AppSettings["AWSBucket"].ToString()+"/Images",
                    Key = deleteImageModel.key.Substring(deleteImageModel.key.IndexOf(ConfigurationManager.AppSettings["AWSBucket"].ToString()+"/"+ ConfigurationManager.AppSettings["AWSBucketFolder"].ToString()) + ConfigurationManager.AppSettings["AWSBucket"].ToString().Length + ConfigurationManager.AppSettings["AWSBucketFolder"].ToString().Length + 2)
                };

                DeleteObjectResponse response2 = client.DeleteObject(deleteObjectRequest);

                if (response2.HttpStatusCode == HttpStatusCode.NoContent)
                {
                    return Content(HttpStatusCode.OK, "Success");
                }
                else
                {
                    return Content(response2.HttpStatusCode, response2.ContentLength.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted"));
            }
        }

        // upload file
        [HttpPost, Route("uploadDocument")]
        public async Task<IHttpActionResult> UploadFile()
        {
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                string root = System.Web.HttpContext.Current.Server.MapPath("~");
                var provider = new CustomMultipartFormDataStreamProvider(root);

                await Request.Content.ReadAsMultipartAsync(provider);
                String keyName = Guid.NewGuid().ToString() + "." + extension.Substring(0, extension.Length - 1);

                string filePath = HostingEnvironment.MapPath("~/documents/" + keyName);

                foreach (MultipartFileData fileData in provider.FileData)
                {
                    if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                    {
                        throw new HttpResponseException(HttpStatusCode.NotAcceptable);
                    }
                    string fileName = fileData.Headers.ContentDisposition.FileName;
                    File.Move(fileData.LocalFileName, Path.Combine(filePath));
                }

                string existingBucketName = ConfigurationManager.AppSettings["mainlink"].ToString() + "/server/documents";

              //  TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1));


               // fileTransferUtility.Upload(filePath, existingBucketName, keyName);
               // Console.WriteLine("Upload 2 completed");

                return Ok(ConfigurationManager.AppSettings["mainlink"].ToString() + "/server/documents/" + keyName);

                //// 3. Upload data from a type of System.IO.Stream.
                //using (FileStream fileToUpload = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                //{
                //    fileTransferUtility.Upload(fileToUpload, existingBucketName, keyName);
                //}
                //Console.WriteLine("Upload 3 completed");

                // 4.Specify advanced settings/options.
                //TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
                //{
                //    BucketName = existingBucketName,
                //    FilePath = filePath,
                //    StorageClass = S3StorageClass.ReducedRedundancy,
                //    PartSize = 6291456, // 6 MB.
                //    Key = keyName,
                //    CannedACL = S3CannedACL.PublicRead
                //};
                //fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
                //fileTransferUtilityRequest.Metadata.Add("param2", "Value2");
                //fileTransferUtility.Upload(fileTransferUtilityRequest);
                //Console.WriteLine("Upload 4 completed");
            }
            catch (AmazonS3Exception s3Exception)
            {
                Log.Debug(s3Exception.Message);
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, s3Exception.Message + " " + s3Exception.InnerException));
            }
        }

        //delete document
        [HttpPost]
        [Route("deleteDocument")]
        public IHttpActionResult DeleteDocument(deleteDocumentBindingModel deleteDocument)
        {
            try
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                IAmazonS3 client;
                client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1);

                int i = deleteDocument.key.Length;
                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = ConfigurationManager.AppSettings["AWSBucket"].ToString() + "/Documents",
                    Key = deleteDocument.key.Substring(deleteDocument.key.IndexOf(ConfigurationManager.AppSettings["AWSBucket"].ToString()+"/"+ ConfigurationManager.AppSettings["AWSBucketDocument"].ToString()) + ConfigurationManager.AppSettings["AWSBucket"].ToString().Length + ConfigurationManager.AppSettings["AWSBucketDocument"].ToString().Length + 2)
                };

                DeleteObjectResponse response2 = client.DeleteObject(deleteObjectRequest);

                if (response2.HttpStatusCode == HttpStatusCode.NoContent)
                {
                    return Content(HttpStatusCode.OK, "success");
                }
                else
                {
                    return Content(response2.HttpStatusCode, response2.ContentLength.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted" + ex));
            }
        }

        public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
        {
            public CustomMultipartFormDataStreamProvider(string path) : base(path) { }

            public override string GetLocalFileName(HttpContentHeaders headers)
            {
                extension = headers.ContentDisposition.FileName.Split('.').Last();
                return headers.ContentDisposition.FileName = "testDocument." + extension.Substring(0, extension.Length - 1);
            }
        }
    }

    public class deleteImageBindingModel
    {
        [Display(Name = "key")]
        public string key { get; set; }
    }

    public class deleteDocumentBindingModel
    {
        [Display(Name = "key")]
        public string key { get; set; }
    }

    public class html
    {
        public string htmlStringX { get; set; }
    }
}