﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Microsoft.AspNet.Identity;
using log4net;

namespace AspNetIdentity.Controllers
{
    public class BatchTransactionController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        // GET api/BatchTransaction
        //public IQueryable<PAY_BATCH_TRANSACTION> GetPAY_BATCH_TRANSACTION()
        //{
        //    return db.PAY_BATCH_TRANSACTION;
        //}

        // GET api/BatchTransaction/5
        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("api/BatchTransaction/Transactions")]
        public IHttpActionResult Transactions(int id)
        {
            try
            {
                List<PAY_BATCH_TRANSACTION> pay_batch_transaction = db.PAY_BATCH_TRANSACTION.Include(s => s.PAY_TRANSACTION).Where(s => s.PAY_BATCH_ID == id).ToList();

                if (pay_batch_transaction == null)
                {
                    return NotFound();
                }

                List<PAY_TRANSACTION> PAY_TRANSACTIONList = new List<PAY_TRANSACTION>();
                AspNetIdentity.Infrastructure.TransactionContext context = new AspNetIdentity.Infrastructure.TransactionContext();

                for (int i = 0; i < pay_batch_transaction.Count(); i++)
                {
                    int eyedee = (int)pay_batch_transaction[i].TRANSACTION_ID;
                    PAY_TRANSACTION PAY_TRANSACTIONObj = context.PAY_TRANSACTION.Include(s=>s.PAY_TRANSACTION_DETAIL.Select(p=>p.MAS_PAYMENT_TYPE)).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.ID == eyedee).FirstOrDefault();
                    PAY_TRANSACTIONList.Add(PAY_TRANSACTIONObj);
                }

                return Ok(PAY_TRANSACTIONList);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // PUT api/BatchTransaction/5
        //public IHttpActionResult PutPAY_BATCH_TRANSACTION(int id, PAY_BATCH_TRANSACTION pay_batch_transaction)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != pay_batch_transaction.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(pay_batch_transaction).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PAY_BATCH_TRANSACTIONExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        public class Batch
        {
            public int batchid;
            public int[] transactionid;
        }

        // POST api/BatchTransaction
        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("api/BatchTransaction/create")]
        public IHttpActionResult gettingPAY_BATCH_TRANSACTION([FromUri] int batchid, [FromUri] string transactionid)
        {
            try
            {
                transactionid = transactionid.Substring(1, transactionid.Length - 2);

                int[] transactionidarray = transactionid.Split(',').Select(str => int.Parse(str)).ToArray();

                if (transactionidarray.Count() == 0)
                {
                    return BadRequest();
                }

                AspNetIdentity.Infrastructure.TransactionContext contextObj = new AspNetIdentity.Infrastructure.TransactionContext();

                for (int i = 0; i < transactionidarray.Count(); i++)
                {
                    PAY_BATCH_TRANSACTION obj = new PAY_BATCH_TRANSACTION();

                    obj.PAY_BATCH_ID = batchid;
                    obj.TRANSACTION_ID = transactionidarray[i];
                    obj.CREATED_BY = User.Identity.GetUserId();
                    obj.UPDATED_BY = User.Identity.GetUserId();
                    obj.CREATED_ON = DateTime.Now;
                    obj.UPDATED_ON = DateTime.Now;

                    db.PAY_BATCH_TRANSACTION.Add(obj);
                    db.SaveChanges();

                    int tranID = Convert.ToInt32(transactionidarray[i]);
                    PAY_TRANSACTION tran = contextObj.PAY_TRANSACTION.Where(s => s.ID == tranID).FirstOrDefault();
                    tran.STATUS = "ADMIN_UNPOST";
                    contextObj.Entry(tran).State = EntityState.Modified;
                    contextObj.SaveChanges();
                }

                return Ok("Success!");
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("api/BatchTransaction/deleteBatch")]
        public IHttpActionResult deleteBatch([FromUri] int batchid)
        {
            try{
                if (batchid == 0)
                {
                    return BadRequest();
                }

                AspNetIdentity.Infrastructure.TransactionContext contextObj = new AspNetIdentity.Infrastructure.TransactionContext();

                PAY_BATCH PAY_BATCHObj = db.PAY_BATCH.Where(s => s.ID == batchid).FirstOrDefault();

                if (PAY_BATCHObj == null)
                {
                    return BadRequest("Wrong BatchID or Batch DoesNot Exist");
                }

                //Setting Statuses
                List<PAY_BATCH_TRANSACTION> PAY_BATCH_TRANSACTIONList = db.PAY_BATCH_TRANSACTION.Where(s=>s.PAY_BATCH_ID == batchid).ToList();
                for (int i = 0; i < PAY_BATCH_TRANSACTIONList.Count(); i++)
                {
                    int batchTranID = (int)PAY_BATCH_TRANSACTIONList[i].TRANSACTION_ID;
                    PAY_TRANSACTION transactionObj = contextObj.PAY_TRANSACTION.Where(s => s.ID == batchTranID).FirstOrDefault();

                    transactionObj.SUB_STATUS = "";
                    transactionObj.STATUS = "POST";
                    transactionObj.UPDATED_ON = DateTime.Now;

                    contextObj.Entry(transactionObj).State = EntityState.Modified;
                    contextObj.SaveChanges();

                   //Deleting Relationships
                    db.Entry(PAY_BATCH_TRANSACTIONList[i]).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                //Deleting Batch
                db.Entry(PAY_BATCHObj).State = EntityState.Deleted;
                db.SaveChanges();

                return Ok("Success!");
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/BatchTransaction/5
        //[ResponseType(typeof(PAY_BATCH_TRANSACTION))]
        //public IHttpActionResult DeletePAY_BATCH_TRANSACTION(int id)
        //{
        //    PAY_BATCH_TRANSACTION pay_batch_transaction = db.PAY_BATCH_TRANSACTION.Find(id);
        //    if (pay_batch_transaction == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PAY_BATCH_TRANSACTION.Remove(pay_batch_transaction);
        //    db.SaveChanges();

        //    return Ok(pay_batch_transaction);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PAY_BATCH_TRANSACTIONExists(int id)
        {
            return db.PAY_BATCH_TRANSACTION.Count(e => e.ID == id) > 0;
        }
    }
}