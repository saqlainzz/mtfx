﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/currencymethod")]
    public class CurrencyMethodController : ApiController
    {
        private CurrencyMethodContext db = new CurrencyMethodContext();

        [Authorize(Roles = "Admin")]
        [Route("currencies")]
        public IHttpActionResult GetCountries()
        {
            return Ok(db.CMN_CURRENCY_METHOD);
        }

        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(CMN_CURRENCY_METHOD))]
        public IHttpActionResult GetCMN_CURRENCY_METHOD(int id)
        {
            CMN_CURRENCY_METHOD CMN_CURRENCY_METHODMethod = db.CMN_CURRENCY_METHOD.Find(id);
            if (CMN_CURRENCY_METHODMethod == null)
            {
                return NotFound();
            }

            return Ok(CMN_CURRENCY_METHODMethod);
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [ResponseType(typeof(CMN_CURRENCY_METHOD))]
        public IHttpActionResult PostCMN_CURRENCY_METHOD(CMN_CURRENCY_METHOD CMN_CURRENCY_METHODMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(CMN_CURRENCY_METHODMethod).State = EntityState.Added;
            db.CMN_CURRENCY_METHOD.Add(CMN_CURRENCY_METHODMethod);
            db.SaveChanges();

            return Ok();
        }

        //[Authorize(Roles = "Admin")]
        //public IHttpActionResult PutCMN_CURRENCY_METHOD(int id, CMN_CURRENCY_METHOD CMN_CURRENCY_METHODMethod)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != CMN_CURRENCY_METHODMethod.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(CMN_CURRENCY_METHODMethod).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CMN_COUNTRYExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// DELETE api/Default1/5
        //[Authorize(Roles = "Admin")]
        //[ResponseType(typeof(CMN_CURRENCY_METHOD))]
        //public IHttpActionResult DeleteCMN_CURRENCY_METHOD(int id)
        //{
        //    CMN_CURRENCY_METHOD CMN_CURRENCY_METHODMethod = db.CMN_CURRENCY_METHOD.Find(id);
        //    if (CMN_CURRENCY_METHODMethod == null)
        //    {
        //        return NotFound();
        //    }

        //    db.CMN_CURRENCY_METHOD.Remove(CMN_CURRENCY_METHODMethod);
        //    db.SaveChanges();

        //    return Ok(CMN_CURRENCY_METHODMethod);
        //}


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.CMN_CURRENCY_METHOD.Count(e => e.ID == id) > 0;
        }
    }
}