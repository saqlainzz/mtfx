﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;

namespace AspNetIdentity.Controllers
{
    public class FAQQuestionController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        // GET api/FAQQuestion
        public List<FAQ_QUESTION> GetFAQ_QUESTION()
        {
            return db.FAQ_QUESTION.ToList();
        }

        [HttpGet]
        [Route("api/getAnswer")]
        public IHttpActionResult getAnswer([FromUri] int id)
        {
            FAQ_ANSWER FAQAnswer = db.FAQ_ANSWER.Include(s => s.FAQ_QUESTION).Where(s=>s.QUESTION_ID == id).FirstOrDefault();
            if (FAQAnswer == null)
            {
                return NotFound();
            }

            return Ok(FAQAnswer);
        }

        //// PUT api/FAQQuestion/5
        //public IHttpActionResult PutFAQ_QUESTION(int id, FAQ_QUESTION faq_question)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != faq_question.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(faq_question).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!FAQ_QUESTIONExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST api/FAQQuestion
        //[ResponseType(typeof(FAQ_QUESTION))]
        //public IHttpActionResult PostFAQ_QUESTION(FAQ_QUESTION faq_question)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.FAQ_QUESTION.Add(faq_question);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = faq_question.ID }, faq_question);
        //}

        //// DELETE api/FAQQuestion/5
        //[ResponseType(typeof(FAQ_QUESTION))]
        //public IHttpActionResult DeleteFAQ_QUESTION(int id)
        //{
        //    FAQ_QUESTION faq_question = db.FAQ_QUESTION.Find(id);
        //    if (faq_question == null)
        //    {
        //        return NotFound();
        //    }

        //    db.FAQ_QUESTION.Remove(faq_question);
        //    db.SaveChanges();

        //    return Ok(faq_question);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FAQ_QUESTIONExists(int id)
        {
            return db.FAQ_QUESTION.Count(e => e.ID == id) > 0;
        }
    }
}