﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Microsoft.AspNet.Identity;
using AspNetIdentity.Infrastructure;

namespace AspNetIdentity.Controllers
{
    public class LabelsController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
        
        [Authorize]
        [Route("labels")]
        public IHttpActionResult GetCountries([FromUri] string name)
        {
            string UserID = User.Identity.GetUserId();
            ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
            ApplicationUser AppUserObj = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();

            var typeList = db.MAS_LABELS.Where(s => s.NAME == name && s.CMN_INSTITUTE_ID == AppUserObj.InstituteID).ToList();
            return Ok(typeList);
        }

        // GET: api/LabelsLS
        public IQueryable<MAS_LABELS> GetMAS_LABELS()
        {
            return db.MAS_LABELS;
        }

        // GET: api/LabelsLS/5
        [ResponseType(typeof(MAS_LABELS))]
        public IHttpActionResult GetMAS_LABELS(int id)
        {
            MAS_LABELS mAS_LABELS = db.MAS_LABELS.Find(id);
            if (mAS_LABELS == null)
            {
                return NotFound();
            }

            return Ok(mAS_LABELS);
        }

        // PUT: api/LabelsLS/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMAS_LABELS(int id, MAS_LABELS mAS_LABELS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mAS_LABELS.ID)
            {
                return BadRequest();
            }

            db.Entry(mAS_LABELS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MAS_LABELSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LabelsLS
        [ResponseType(typeof(MAS_LABELS))]
        public IHttpActionResult PostMAS_LABELS(MAS_LABELS mAS_LABELS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAS_LABELS.Add(mAS_LABELS);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mAS_LABELS.ID }, mAS_LABELS);
        }

        // DELETE: api/LabelsLS/5
        [ResponseType(typeof(MAS_LABELS))]
        public IHttpActionResult DeleteMAS_LABELS(int id)
        {
            MAS_LABELS mAS_LABELS = db.MAS_LABELS.Find(id);
            if (mAS_LABELS == null)
            {
                return NotFound();
            }

            db.MAS_LABELS.Remove(mAS_LABELS);
            db.SaveChanges();

            return Ok(mAS_LABELS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_LABELSExists(int id)
        {
            return db.MAS_LABELS.Count(e => e.ID == id) > 0;
        }
    }
}