﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;

namespace AspNetIdentity.Controllers
{
    public class FAQCategoryController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        [HttpGet]
        [Route("api/getAllCategory")]
        public IHttpActionResult GetFAQ_CATEGORY()
        {
            List<FAQ_CATEGORY> listCategory = db.FAQ_CATEGORY.ToList();
            List<formatFAQ_CATEGORYFAQ_QUESTION> formatFAQ_CATEGORYFAQ_QUESTIONList = new List<formatFAQ_CATEGORYFAQ_QUESTION>();
            
            for(int i=0;i<listCategory.Count; i++)
            {
                formatFAQ_CATEGORYFAQ_QUESTION formatFAQ_CATEGORYFAQ_QUESTIONObj  = new formatFAQ_CATEGORYFAQ_QUESTION();
                int ID = listCategory[i].ID;

                formatFAQ_CATEGORYFAQ_QUESTIONObj.FAQ_CATEGORY = listCategory[i];
                formatFAQ_CATEGORYFAQ_QUESTIONObj.FAQ_QUESTION = db.FAQ_QUESTION.Where(s => s.CATEGORY_ID == ID).ToList();

                formatFAQ_CATEGORYFAQ_QUESTIONList.Add(formatFAQ_CATEGORYFAQ_QUESTIONObj);
            }

            return Ok(formatFAQ_CATEGORYFAQ_QUESTIONList);
        }

        public class formatFAQ_CATEGORYFAQ_QUESTION
        {
            public FAQ_CATEGORY FAQ_CATEGORY { get; set; }
            public List<FAQ_QUESTION> FAQ_QUESTION { get; set; }

        }

        // GET api/FAQCategory/5
        [ResponseType(typeof(FAQ_CATEGORY))]
        public IHttpActionResult GetFAQ_CATEGORY(int id)
        {
            FAQ_CATEGORY faq_category = db.FAQ_CATEGORY.Find(id);
            if (faq_category == null)
            {
                return NotFound();
            }

            return Ok(faq_category);
        }

        //// PUT api/FAQCategory/5
        //public IHttpActionResult PutFAQ_CATEGORY(int id, FAQ_CATEGORY faq_category)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != faq_category.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(faq_category).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!FAQ_CATEGORYExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST api/FAQCategory
        //[ResponseType(typeof(FAQ_CATEGORY))]
        //public IHttpActionResult PostFAQ_CATEGORY(FAQ_CATEGORY faq_category)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.FAQ_CATEGORY.Add(faq_category);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = faq_category.ID }, faq_category);
        //}

        //// DELETE api/FAQCategory/5
        //[ResponseType(typeof(FAQ_CATEGORY))]
        //public IHttpActionResult DeleteFAQ_CATEGORY(int id)
        //{
        //    FAQ_CATEGORY faq_category = db.FAQ_CATEGORY.Find(id);
        //    if (faq_category == null)
        //    {
        //        return NotFound();
        //    }

        //    db.FAQ_CATEGORY.Remove(faq_category);
        //    db.SaveChanges();

        //    return Ok(faq_category);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FAQ_CATEGORYExists(int id)
        {
            return db.FAQ_CATEGORY.Count(e => e.ID == id) > 0;
        }
    }
}