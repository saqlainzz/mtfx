﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/paymenttype")]
    public class PaymentTypeController : ApiController
    {
        private PaymentTypeContext db = new PaymentTypeContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Authorize]
        [Route("paymenttypes")]
        public IHttpActionResult GetCountries()
        {
            string UserID = User.Identity.GetUserId();
            ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
            ApplicationUser AppUserObj = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();

            var typeList = db.MAS_PAYMENT_TYPE.Where(s => s.CMN_INSTITUTE_ID == AppUserObj.InstituteID).ToList();
            return Ok(typeList);
        }

        [Authorize]
        [HttpGet]
        [Route("GetbyID")]
        public IHttpActionResult GetbyID([FromUri] int ID)
        {
            if (ID == 0)
            {
                return BadRequest("ID not correct");
            }

            var typeList = db.MAS_PAYMENT_TYPE.Where(s => s.CMN_INSTITUTE_ID == ID).ToList();

            return Ok(typeList);
        }

        //[ResponseType(typeof(MAS_PAYMENT_TYPE))]
        //public IHttpActionResult GetMAS_PAYMENT_TYPE(int id)
        //{
        //    MAS_PAYMENT_TYPE MAS_PAYMENT_TYPEMethod = db.MAS_PAYMENT_TYPE.Find(id);
        //    if (MAS_PAYMENT_TYPEMethod == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(MAS_PAYMENT_TYPEMethod);
        //}

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [ResponseType(typeof(MAS_PAYMENT_TYPE))]
        public IHttpActionResult PostMAS_PAYMENT_TYPE(MAS_PAYMENT_TYPE MAS_PAYMENT_TYPEMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(MAS_PAYMENT_TYPEMethod).State = EntityState.Added;
            db.MAS_PAYMENT_TYPE.Add(MAS_PAYMENT_TYPEMethod);
            db.SaveChanges();

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        public IHttpActionResult PutMAS_PAYMENT_TYPE(int id, MAS_PAYMENT_TYPE MAS_PAYMENT_TYPEMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != MAS_PAYMENT_TYPEMethod.ID)
            {
                return BadRequest();
            }

            db.Entry(MAS_PAYMENT_TYPEMethod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CMN_COUNTRYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE api/Default1/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(MAS_PAYMENT_TYPE))]
        public IHttpActionResult DeleteMAS_PAYMENT_TYPE(int id)
        {
            MAS_PAYMENT_TYPE MAS_PAYMENT_TYPEMethod = db.MAS_PAYMENT_TYPE.Find(id);
            if (MAS_PAYMENT_TYPEMethod == null)
            {
                return NotFound();
            }

            db.MAS_PAYMENT_TYPE.Remove(MAS_PAYMENT_TYPEMethod);
            db.SaveChanges();

            return Ok(MAS_PAYMENT_TYPEMethod);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.MAS_PAYMENT_TYPE.Count(e => e.ID == id) > 0;
        }
    }
}