﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Microsoft.AspNet.Identity;
using AspNetIdentity.Infrastructure;

namespace AspNetIdentity.Controllers
{
    public class NotesController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        // GET: api/Notes
        public IQueryable<PAY_TRANSACTION_NOTES> GetPAY_TRANSACTION_NOTES()
        {
            return db.PAY_TRANSACTION_NOTES;
        }

        [Authorize]
        [HttpGet]
        [Route("api/Notes/GetNotes")]
        public IHttpActionResult GetNotes([FromUri] int transactionid)
        {
            List<PAY_TRANSACTION_NOTES> pAY_TRANSACTION_NOTES = db.PAY_TRANSACTION_NOTES.Where(s => s.PAY_TRANSACTION_ID == transactionid).ToList();
            if (pAY_TRANSACTION_NOTES == null)
            {
                return NotFound();
            }

            for (int i = 0; i < pAY_TRANSACTION_NOTES.Count(); i++)
            {
                string UserID = pAY_TRANSACTION_NOTES[i].CREATED_BY;

                ApplicationDbContext usercontext = new ApplicationDbContext();
                ApplicationUser user = usercontext.Users.Where(s => s.Id == UserID).FirstOrDefault();

                if (user == null)
                {
                    pAY_TRANSACTION_NOTES[i].CREATED_BY = "UNKNOWN";
                }
                else
                {
                    pAY_TRANSACTION_NOTES[i].CREATED_BY = user.FirstName + " " + user.LastName;
                }
            }

            return Ok(pAY_TRANSACTION_NOTES);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Notes/createNotes")]
        public IHttpActionResult createNotes([FromUri] string note, [FromUri] int transactionid)
        {
            if (note == null)
            {
                return NotFound();
            }

            string UserID = User.Identity.GetUserId();
            ApplicationDbContext usercontext = new ApplicationDbContext();
            ApplicationUser user = usercontext.Users.Where(s => s.Id == UserID).FirstOrDefault();

            PAY_TRANSACTION_NOTES PAY_TRANSACTION_NOTESObj = new PAY_TRANSACTION_NOTES();
            PAY_TRANSACTION_NOTESObj.NOTES = note;
            PAY_TRANSACTION_NOTESObj.PAY_TRANSACTION_ID = transactionid;
            PAY_TRANSACTION_NOTESObj.CREATED_ON = DateTime.Now;
            PAY_TRANSACTION_NOTESObj.CREATED_BY = UserID;

            db.PAY_TRANSACTION_NOTES.Add(PAY_TRANSACTION_NOTESObj);
            db.SaveChanges();

            return Ok(PAY_TRANSACTION_NOTESObj);
        }

        [Authorize]
        [HttpDelete]
        [Route("api/Notes/deleteNotes")]
        public IHttpActionResult deleteNotes([FromUri] int noteid)
        {
            if (noteid == null)
            {
                return NotFound();
            }

            string UserID = User.Identity.GetUserId();

            PAY_TRANSACTION_NOTES PAY_TRANSACTION_NOTESObj = db.PAY_TRANSACTION_NOTES.Where(s => s.ID == noteid).FirstOrDefault();
            db.Entry(PAY_TRANSACTION_NOTESObj).State = EntityState.Deleted;
            db.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PAY_TRANSACTION_NOTESExists(int id)
        {
            return db.PAY_TRANSACTION_NOTES.Count(e => e.ID == id) > 0;
        }
    }
}