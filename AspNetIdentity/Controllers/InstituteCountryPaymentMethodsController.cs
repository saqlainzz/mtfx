﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Microsoft.AspNet.Identity;
using log4net;

namespace AspNetIdentity.Controllers
{
    public class InstituteCountryPaymentMethodsController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/InstituteCountryPaymentMethods
        public List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> GetCMN_INSTITUTE_COUNTRY_PAYMENTMETHODS()
        {
            return db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_PAYMENT_METHOD).Include(s=>s.MAS_COUNTRY).ToList();
        }

        [HttpGet]
        [Route("api/GetPaymentMethods")]
        public IHttpActionResult GetCMN_INSTITUTE_COUNTRY_PAYMENTMETHODS([FromUri] int countryid, [FromUri] int instituteid)
        {
            try
            {
                MAS_COUNTRY MAS_COUNTRYobj = db.MAS_COUNTRY.Include(s => s.CMN_CURRENCY).Where(s => s.ID == countryid).FirstOrDefault();

                if (MAS_COUNTRYobj == null)
                {
                    return Content(HttpStatusCode.NotFound, "Invalid country ID");
                }

                List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> cmn_institute_country_paymentmethods = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_PAYMENT_METHOD).Include(s => s.MAS_COUNTRY).Where(s => s.MAS_COUNTRY_ID == countryid && s.CMN_INSTITUTE_ID == instituteid).OrderBy(s => s.SORT_ORDER).ToList();
                if (cmn_institute_country_paymentmethods.Count() == 0)
                {
                    return Content(HttpStatusCode.NotFound, "No Records Found.");
                    //return Ok(db.CMN_INSTITUE_PAYMENT_METHODS.Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_PAYMENT_METHOD).Where(s=>s.CMN_INSTITUTE_ID == instituteid));
                }

                for (int i = 0; i < cmn_institute_country_paymentmethods.Count(); i++)
                {
                    if (cmn_institute_country_paymentmethods[i].CMN_PAYMENT_METHOD.METHOD_NAME.Contains("Visa"))
                    {
                        cmn_institute_country_paymentmethods[i].CMN_PAYMENT_METHOD.METHOD_NAME = cmn_institute_country_paymentmethods[i].CMN_PAYMENT_METHOD.METHOD_NAME;// +" in " + MAS_COUNTRYobj.CMN_CURRENCY.CURR_SYMBOL;
                    }

                    if (cmn_institute_country_paymentmethods[i].CMN_PAYMENT_METHOD.METHOD_NAME.Contains("MasterCard"))
                    {
                        cmn_institute_country_paymentmethods[i].CMN_PAYMENT_METHOD.METHOD_NAME = cmn_institute_country_paymentmethods[i].CMN_PAYMENT_METHOD.METHOD_NAME;// +" in " + MAS_COUNTRYobj.CMN_CURRENCY.CURR_SYMBOL;
                    }
                }

                return Ok(cmn_institute_country_paymentmethods);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // PUT api/InstituteCountryPaymentMethods/5
        //public IHttpActionResult PutCMN_INSTITUTE_COUNTRY_PAYMENTMETHODS(int id, CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS cmn_institute_country_paymentmethods)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != cmn_institute_country_paymentmethods.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(cmn_institute_country_paymentmethods).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //public class Batches
        //{
        //    public int institueid;
        //    public int countryid;
        //    public int[] paymentmethodid;
        //}

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("api/InstituteCountryPaymentMethods/create")]
        public IHttpActionResult saveCMN_INSTITUTE_COUNTRY_PAYMENTMETHODS([FromUri] int institueid, [FromUri] int countryid, [FromUri] string paymentmethodid)
        {
            try
            {
                paymentmethodid = paymentmethodid.Substring(1, paymentmethodid.Length - 2);

                int[] paymentmethodidarray = paymentmethodid.Split(',').Select(str => int.Parse(str)).ToArray();

                if (paymentmethodidarray.Count() == 0)
                {
                    return BadRequest();
                }

                for (int i = 0; i < paymentmethodidarray.Count(); i++)
                {
                    CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS obj = new CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS();

                    obj.CMN_INSTITUTE_ID = institueid;
                    obj.MAS_COUNTRY_ID = countryid;
                    obj.CMN_PAYMENT_METHOD_ID = paymentmethodidarray[i];
                    obj.CREATED_BY = User.Identity.GetUserId();
                    obj.UPDATED_BY = User.Identity.GetUserId();
                    obj.CREATED_ON = DateTime.Now;
                    obj.UPDATED_ON = DateTime.Now;

                    db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Add(obj);
                    db.SaveChanges();
                }

                return Ok(formatingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class getCountryMethods
        {
            public int instituteID { get; set; }
            public MAS_COUNTRY country { get; set; }
            public List<CMN_PAYMENT_METHOD> paymentMethod { get; set; }
        }

        [Authorize]
        [HttpGet]
        [Route("api/GetInstitutePaymentMethods")]
        public IHttpActionResult GetttingCMN_INSTITUTE_COUNTRY_PAYMENTMETHODS([FromUri] int instituteid)
        {
            try
            {
                //List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> cmn_institute_country_paymentmethods = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_PAYMENT_METHOD).Include(s => s.MAS_COUNTRY).Where(s => s.CMN_INSTITUTE_ID == instituteid).ToList();
                List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> cmn_institute_country_paymentmethods = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_INSTITUTE_ID == instituteid).ToList();
            
                if (cmn_institute_country_paymentmethods == null)
                {
                    return BadRequest();
                }

                List<getCountryMethods> countryMethodsList = new List<getCountryMethods>();

                List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> icpmObj = cmn_institute_country_paymentmethods.GroupBy(x => new {x.MAS_COUNTRY_ID, x.CMN_INSTITUTE_ID} ).Select(x => x.First()).ToList();

                for (int i = 0; i < icpmObj.Count(); i++)
                {
                    getCountryMethods gcmObj = new getCountryMethods();
                    gcmObj.paymentMethod = new List<CMN_PAYMENT_METHOD>();

                    gcmObj.instituteID = instituteid;
                    int countryID = icpmObj[i].MAS_COUNTRY_ID;
                    CountryContext countryContext = new CountryContext();
                    MAS_COUNTRY cmObj = countryContext.MAS_COUNTRY.Where(s => s.ID == countryID).FirstOrDefault();

                    gcmObj.country = cmObj;

                    List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> institueContryList = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_INSTITUTE_ID == instituteid && s.MAS_COUNTRY_ID == countryID).ToList();

                    for (int j = 0; j < institueContryList.Count(); j++)
                    {
                        int payMethodid = institueContryList[j].CMN_PAYMENT_METHOD_ID;
                        CMN_PAYMENT_METHOD pmObj = db.CMN_PAYMENT_METHOD.Where(s => s.ID == payMethodid).FirstOrDefault();
                        gcmObj.paymentMethod.Add(pmObj);
                    }

                    countryMethodsList.Add(gcmObj);
                }

                return Ok(countryMethodsList);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/InstituteCountryPaymentMethods/delete")]
        public IHttpActionResult deletingCMN_INSTITUTE_COUNTRY_PAYMENTMETHODS([FromUri] int institueid, [FromUri] int countryid)
        {
            try{
                List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> icpmObj = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_INSTITUTE_ID == institueid && s.MAS_COUNTRY_ID == countryid).ToList();

                for (int i = 0; i < icpmObj.Count(); i++)
                {
                    db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Remove(icpmObj[i]);
                    db.SaveChanges();
                }

                return Ok(formatingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }


        public formating formatingFunc(string state)
        {
            return new formating
            {
                status = state
            };
        }

        public class formating
        {
            public string status { get; set; }
        }

        //// DELETE api/InstituteCountryPaymentMethods/5
        //[ResponseType(typeof(CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS))]
        //public IHttpActionResult DeleteCMN_INSTITUTE_COUNTRY_PAYMENTMETHODS(int id)
        //{
        //    CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS cmn_institute_country_paymentmethods = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Find(id);
        //    if (cmn_institute_country_paymentmethods == null)
        //    {
        //        return NotFound();
        //    }

        //    db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Remove(cmn_institute_country_paymentmethods);
        //    db.SaveChanges();

        //    return Ok(cmn_institute_country_paymentmethods);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSExists(int id)
        {
            return db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Count(e => e.ID == id) > 0;
        }
    }
}