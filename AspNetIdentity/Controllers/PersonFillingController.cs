﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/personfilling")]
    public class PersonFillingController : ApiController
    {
        private PersonFillingContext db = new PersonFillingContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("personfillings")]
        public IHttpActionResult GetCountries()
        {
            return Ok(db.MAS_PERSON_FILLING);
        }

        [ResponseType(typeof(MAS_PERSON_FILLING))]
        public IHttpActionResult GetMAS_PERSON_FILLING(int id)
        {
            MAS_PERSON_FILLING MAS_PERSON_FILLINGMethod = db.MAS_PERSON_FILLING.Find(id);
            if (MAS_PERSON_FILLINGMethod == null)
            {
                return NotFound();
            }

            return Ok(MAS_PERSON_FILLINGMethod);
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [ResponseType(typeof(MAS_PERSON_FILLING))]
        public IHttpActionResult PostMAS_PERSON_FILLING(MAS_PERSON_FILLING MAS_PERSON_FILLINGMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(MAS_PERSON_FILLINGMethod).State = EntityState.Added;
            db.MAS_PERSON_FILLING.Add(MAS_PERSON_FILLINGMethod);
            db.SaveChanges();

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        public IHttpActionResult PutMAS_PERSON_FILLING(int id, MAS_PERSON_FILLING MAS_PERSON_FILLINGMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != MAS_PERSON_FILLINGMethod.ID)
            {
                return BadRequest();
            }

            db.Entry(MAS_PERSON_FILLINGMethod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CMN_COUNTRYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE api/Default1/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(MAS_PERSON_FILLING))]
        public IHttpActionResult DeleteMAS_PERSON_FILLING(int id)
        {
            MAS_PERSON_FILLING MAS_PERSON_FILLINGMethod = db.MAS_PERSON_FILLING.Find(id);
            if (MAS_PERSON_FILLINGMethod == null)
            {
                return NotFound();
            }

            db.MAS_PERSON_FILLING.Remove(MAS_PERSON_FILLINGMethod);
            db.SaveChanges();

            return Ok(MAS_PERSON_FILLINGMethod);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.MAS_PERSON_FILLING.Count(e => e.ID == id) > 0;
        }
    }
}