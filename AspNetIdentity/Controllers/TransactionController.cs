﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.IO;
using iTextSharp.text.pdf;
using System.Web.Hosting;
using Amazon.S3.Transfer;
using Amazon.S3;
using Amazon.S3.Model;
using Moneris;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using System.Globalization;
using Paysafe;
using System.Dynamic;
using log4net;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/transaction")]
    public class TransactionController : ApiController
    {
        private AspNetIdentity.Infrastructure.TransactionContext db = new AspNetIdentity.Infrastructure.TransactionContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[Route("transactions")]
        //public List<PAY_TRANSACTION> GetTransactions()
        //{
        //    IQueryable<PAY_TRANSACTION> obj = db.PAY_TRANSACTION.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1);
        //    return obj.ToList();
        //}

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("GetTransactions")]
        public IHttpActionResult AllTransactions([FromUri] string status,[FromUri] int instituteid,[FromUri] string substatus)
        {
            try
            {
                //int countPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["CountPerPage"]);
                //pagenumber = pagenumber - 1;
                List<PAY_TRANSACTION> transactionObject = null;
                int count = 0;

                substatus = substatus.Substring(1, substatus.Length - 2);

                List<string> substatusarray = substatus.Split(',').ToList();

                if (substatusarray[0] == "")
                {
                    substatusarray.Remove(substatusarray[0]);
                }

                if (status.ToLower() == "all" && instituteid == 0)
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s=>s.PAY_TRANSACTION_DETAIL.Select(p=>p.MAS_PAYMENT_TYPE)).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).ToList();
                    count = db.PAY_TRANSACTION.Count();
                    return Ok(formatObject(count, transactionObject));
                }

                if (status.ToLower() == "all" && instituteid != 0)
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid).OrderByDescending(s => s.CREATED_ON).ToList();
                    count = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteid).Count();
                    return Ok(formatObject(count, transactionObject));
                }

                if (substatusarray.Count() != 0)
                {
                    if (instituteid == 0)
                    {
                        transactionObject = new List<PAY_TRANSACTION>();

                        for (int i = 0; i < substatusarray.Count(); i++)
                        {
                            if (substatusarray[i].ToLower() == "new")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "received")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "initiated")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.PAYMENT_SENT_DATE != null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "extended")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == true && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "cancelled")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.SUB_STATUS == "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "onhold")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.SUB_STATUS == "ON_HOLD").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }
                        }
                    }
                    else
                    {
                        transactionObject = new List<PAY_TRANSACTION>();

                        for (int i = 0; i < substatusarray.Count(); i++)
                        {
                            if (substatusarray[i].ToLower() == "new")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "received")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "initiated")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.PAYMENT_SENT_DATE != null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "extended")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == true && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "cancelled")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.SUB_STATUS == "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "onhold")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.SUB_STATUS == "ON_HOLD").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if(instituteid == 0)
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status).OrderByDescending(s => s.CREATED_ON).ToList();
                        count = db.PAY_TRANSACTION.Where(s => s.STATUS.ToLower() == status).Count();
                        return Ok(formatObject(count, transactionObject));
                    }
                    else
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.CMN_INSTITUTE_ID == instituteid).OrderByDescending(s => s.CREATED_ON).ToList();
                        count = db.PAY_TRANSACTION.Where(s => s.STATUS.ToLower() == status && s.CMN_INSTITUTE_ID == instituteid).Count();
                        return Ok(formatObject(count, transactionObject));
                    }
                }

                count = transactionObject.Count();
                transactionObject = transactionObject.OrderByDescending(s => s.CREATED_ON).ToList();
                
                return Ok(formatObject(count, transactionObject));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("ExportGetTransactions")]
        public IHttpActionResult ExportGetTransactions([FromUri] string status, [FromUri] int instituteid, [FromUri] string substatus)
        {
            try
            {
                //int countPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["CountPerPage"]);
                List<PAY_TRANSACTION> transactionObject = null;

                substatus = substatus.Substring(1, substatus.Length - 2);

                List<string> substatusarray = substatus.Split(',').ToList();

                if (substatusarray[0] == "")
                {
                    substatusarray.Remove(substatusarray[0]);
                }

                if (status.ToLower() == "all" && instituteid == 0)
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).ToList();
                    return Ok(transactionObject);
                }

                if (status.ToLower() == "all" && instituteid != 0)
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid).ToList();
                    return Ok(transactionObject);
                }

                if (substatusarray.Count() != 0)
                {
                    if (instituteid == 0)
                    {
                        transactionObject = new List<PAY_TRANSACTION>();

                        for (int i = 0; i < substatusarray.Count(); i++)
                        {
                            if (substatusarray[i].ToLower() == "new")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "received")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "initiated")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.PAYMENT_SENT_DATE != null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "extended")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == true && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "cancelled")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.SUB_STATUS == "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "onhold")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.SUB_STATUS == "ON_HOLD").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }
                        }
                    }
                    else
                    {
                        transactionObject = new List<PAY_TRANSACTION>();

                        for (int i = 0; i < substatusarray.Count(); i++)
                        {
                            if (substatusarray[i].ToLower() == "new")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "received")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "initiated")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.PAYMENT_SENT_DATE != null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "extended")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == true && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "cancelled")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.SUB_STATUS == "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "onhold")
                            {
                                var tranList = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.STATUS == status && s.SUB_STATUS == "ON_HOLD").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (instituteid == 0)
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status).ToList();
                        return Ok(transactionObject);
                    }
                    else
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == status && s.CMN_INSTITUTE_ID == instituteid).ToList();
                       return Ok(transactionObject);
                    }
                }
  
                return Ok(transactionObject);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("ComplainceReporting")]
        public IHttpActionResult ComplainceReporting([FromUri] string status, [FromUri] int instituteid)
        {
            try
            {
                List<PAY_TRANSACTION> transactionObject = null;
                int count = 0;

                if (instituteid == 0)
                {
                    if (status == "all")
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).ToList();
                        count = db.PAY_TRANSACTION.Count();
                        return Ok(formatObject(count, transactionObject));
                    }
                    else
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT.STATUS).FirstOrDefault() == status).OrderByDescending(s => s.CREATED_ON).ToList();
                        count = db.PAY_TRANSACTION.Where(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT.STATUS).FirstOrDefault() == status).Count();
                        return Ok(formatObject(count, transactionObject));
                    }
                }
                else
                {
                    if (status == "all")
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid).OrderByDescending(s => s.CREATED_ON).ToList();
                        count = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteid).OrderByDescending(s => s.CREATED_ON).ToList().Count();
                        return Ok(formatObject(count, transactionObject));
                    }
                    else
                    {
                        transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT.STATUS).FirstOrDefault() == status && s.CMN_INSTITUTE_ID == instituteid).OrderByDescending(s => s.CREATED_ON).ToList();
                        count = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_NOTES).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT.STATUS).FirstOrDefault() == status && s.CMN_INSTITUTE_ID == instituteid).OrderByDescending(s => s.CREATED_ON).ToList().Count();
                        return Ok(formatObject(count, transactionObject));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("ExportComplainceReporting")]
        public IHttpActionResult ExportComplainceReporting([FromUri] string status)
        {
            try
            {
                List<PAY_TRANSACTION> transactionObject = null;

                if (status == "all")
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).ToList();
                    return Ok(transactionObject);
                }
                else
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT.STATUS).FirstOrDefault() == status).OrderByDescending(s => s.CREATED_ON).ToList();
                    return Ok(transactionObject);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class logModel
        {
            public string TRANSACTION_ID { get; set; }
            public string INSTITUTE_NAME { get; set; }
            public string AMOUNT { get; set; }
            public string MERCHANT_REF_ID { get; set; }
            public string PAYMENT_ID { get; set; }
            public string RESPONSE_CODE { get; set; }
            public string DESCRIPTION { get; set; }
            public bool SUCCESS { get; set; }
            public DateTime CREATED_ON { get; set; }
            public PAY_TRANSACTION PAY_TRANSACTION { get; set; }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("transactionlogs")]
        public IHttpActionResult transactionlogs()
        {
            try
            {
                int count = 0;

                AspNetIdentityContext context = new AspNetIdentityContext();

                List<TRANSACTION_RESPONSE> TRANSACTION_RESPONSEList = context.TRANSACTION_RESPONSE.OrderByDescending(s=>s.CREATED_ON).ToList();
                count = context.TRANSACTION_RESPONSE.Count();
                List<logModel> logModelList = new List<logModel>();

                for (int i = 0; i < TRANSACTION_RESPONSEList.Count; i++)
                {
                    int eyeeDee = (int)TRANSACTION_RESPONSEList[i].TRANSACTION_ID;
                    PAY_TRANSACTION PAY_TRANSACTIONObj = db.PAY_TRANSACTION.Include(s=>s.CMN_PAYMENT_METHOD1).Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_COUNTRY)).Where(s => s.ID == eyeeDee).FirstOrDefault();

                    if(PAY_TRANSACTIONObj != null)
                    {
                         logModel logModelObj = new logModel();
                         logModelObj.TRANSACTION_ID = PAY_TRANSACTIONObj.PREFIX;
                         logModelObj.MERCHANT_REF_ID = TRANSACTION_RESPONSEList[i].MERCHANT_REF_ID;
                         logModelObj.INSTITUTE_NAME = PAY_TRANSACTIONObj.CMN_INSTITUTE.INSTITUTE_NAME;
                         logModelObj.AMOUNT = PAY_TRANSACTIONObj.CMN_CURRENCY.CURR_SYMBOL + " " + PAY_TRANSACTIONObj.AMOUNT_FX;
                         logModelObj.PAYMENT_ID = TRANSACTION_RESPONSEList[i].PAYMENT_ID;
                         logModelObj.RESPONSE_CODE = TRANSACTION_RESPONSEList[i].RESPONSE_CODE;
                         logModelObj.DESCRIPTION = TRANSACTION_RESPONSEList[i].MESSAGE;
                         logModelObj.SUCCESS = (bool)TRANSACTION_RESPONSEList[i].SUCCESS;
                         logModelObj.CREATED_ON = (DateTime)TRANSACTION_RESPONSEList[i].CREATED_ON;
                         logModelObj.PAY_TRANSACTION = PAY_TRANSACTIONObj;

                         logModelList.Add(logModelObj);
                    }
                }

                count = logModelList.Count();
                logModelList = logModelList.OrderByDescending(s => s.CREATED_ON).ToList();

                return Ok(logReturnObject(count, logModelList));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class logReturnModel
        {
            public int count { get; set; }
            public List<logModel> logModel { get; set; }
        }


        public logReturn logReturnObject(int COUNT, List<logModel> tranObject)
        {
            return new logReturn
            {
                total_count = COUNT,
                data = tranObject
            };
        }

        public class logReturn
        {
            public int total_count { get; set; }
            public List<logModel> data { get; set; }
        }


        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("GetUniversityTransactions")]
        public IHttpActionResult GetInstituteTransactions([FromUri] string status, [FromUri] int instituteID)
        {
            try
            {
                List<PAY_TRANSACTION> transactionObject = null;

                if (status.ToLower() == "all")
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteID).OrderByDescending(s => s.CREATED_ON).ToList();
                }
                else
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS.ToLower() == status.ToLower() && s.CMN_INSTITUTE_ID == instituteID).OrderByDescending(s => s.CREATED_ON).ToList();
                }

                List<PAY_TRANSACTION> transactionResponse = new List<PAY_TRANSACTION>();

                for (int i = 0; i < transactionObject.Count(); i++)
                {
                    PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILobj = transactionObject[i].PAY_TRANSACTION_DETAIL.FirstOrDefault();

                    if (PAY_TRANSACTION_DETAILobj != null)
                    {
                        if (PAY_TRANSACTION_DETAILobj.MAS_STUDENT.STATUS == "SUBMITTED")
                        {
                            transactionResponse.Add(transactionObject[i]);
                        }
                    }
                }

                //IQueryable<PAY_TRANSACTION_DETAIL> transactionObject = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_COUNTRY).Include(s => s.MAS_STUDENT).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Include(s => s.PAY_TRANSACTION).Include(s => s.MAS_STUDENT);
                return Ok(transactionResponse);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("GetFilterInstituteBatchTransactions")]
        public IHttpActionResult GetFilterInstituteBatchTransactions([FromUri] string status, [FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try
            {
                if (ToDate != null)
                {
                    ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                string UserID = User.Identity.GetUserId();
                ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                int instituteID = 0;
                instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return BadRequest("Invalid Token");
                }

                List<PAY_TRANSACTION> transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_BANK_DETAILS).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_BATCH_TRANSACTION).Include(s => s.MAS_COUNTRY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteID && (s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST")).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS).ToList();

                status = status.Substring(1, status.Length - 2);

                string[] substatusarray = status.Split(',').ToArray();

                if (substatusarray[0] == "" && FromDate == null && ToDate == null)
                {
                    transactionObject = db.PAY_TRANSACTION.Where(s => 2 == 1).ToList();
                    return Ok(transactionObject);
                }
                else
                {
                    if (substatusarray.Count() != 0)
                    {
                        if (substatusarray[0] != "")
                        {
                            transactionObject = new List<PAY_TRANSACTION>();
                        }

                        for (int i = 0; i < substatusarray.Count(); i++)
                        {
                            if (substatusarray[i] == "FUNDS_IN_TRANSIT")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.STATUS == "ADMIN_POST" && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED" && s.CMN_INSTITUTE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "RECEIPT_CONFIRMED")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.STATUS == "UNIVERSITY_POST" && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED" && s.CMN_INSTITUTE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "PENDING_RELEASE")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.STATUS == "ADMIN_UNPOST" && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED" && s.CMN_INSTITUTE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "ON_HOLD")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => (s.STATUS == "UNIVERSITY_POST" || s.STATUS == "ADMIN_POST" || s.STATUS == "ADMIN_UNPOST") && s.SUB_STATUS == "ON_HOLD" && s.CMN_INSTITUTE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i] == "CANCELLED")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => (s.STATUS == "UNIVERSITY_POST" || s.STATUS == "ADMIN_POST" || s.STATUS == "ADMIN_UNPOST") && s.SUB_STATUS == "CANCELLED" && s.CMN_INSTITUTE_ID == instituteID).ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }
                        }
                    }

                    if (FromDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON > FromDate).ToList();
                    }

                    if (ToDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON < ToDate).ToList();
                    }

                    if (transactionObject == null)
                    {
                        return NotFound();
                    }

                    return Ok(transactionObject);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("GetFilterUniversityTransactions")]
        public IHttpActionResult GetFilterUniversityTransactions([FromUri] string status, [FromUri] string substatus, [FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try
            {
                if (ToDate != null)
                {
                    ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                string UserID = User.Identity.GetUserId();
                ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                int instituteID = 0;
                instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return BadRequest("Invalid Token");
                }

                List<PAY_TRANSACTION> transactionObject = null;

                if (status == null)
                {
                    return BadRequest("No Status Found");
                }

                transactionObject = db.PAY_TRANSACTION.Include(s=>s.PAY_TRANSACTION_DETAIL.Select(p=>p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == status && s.PAY_TRANSACTION_DETAIL.Select(a => a.MAS_STUDENT.STATUS).FirstOrDefault() == "SUBMITTED").OrderByDescending(s => s.CREATED_ON).ToList();

                substatus = substatus.Substring(1, substatus.Length - 2);

                string[] substatusarray = substatus.Split(',').ToArray();

                if (substatusarray[0] == "" && FromDate == null && ToDate == null)
                {
                    transactionObject = db.PAY_TRANSACTION.Where(s=>2==1).ToList();
                    return Ok(transactionObject);
                }
                else
                {
                    if (substatusarray.Count() != 0)
                    {
                        if (substatusarray[0] != "")
                        {
                            transactionObject = new List<PAY_TRANSACTION>();
                        }

                        for (int i = 0; i < substatusarray.Count(); i++)
                        {
                            if (substatusarray[i].ToLower() == "new")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == false && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "received")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == status && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "initiated")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == status && s.PAYMENT_SENT_DATE != null && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "extended")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == status && s.PAYMENT_SENT_DATE == null && s.NEED_DAYS == true && s.SUB_STATUS != "ON_HOLD" && s.SUB_STATUS != "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "cancelled")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == status && s.SUB_STATUS == "CANCELLED").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }

                            if (substatusarray[i].ToLower() == "onhold")
                            {
                                var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == status && s.SUB_STATUS == "ON_HOLD").ToList();
                                for (int k = 0; k < tranList.Count(); k++)
                                {
                                    transactionObject.Add(tranList[k]);
                                }
                            }
                        }
                    }

                    if (FromDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON > FromDate).ToList();
                    }

                    if (ToDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON < ToDate).ToList();
                    }

                    transactionObject = transactionObject.OrderByDescending(s => s.CREATED_ON).ToList();

                    List<PAY_TRANSACTION> transactionResponse = new List<PAY_TRANSACTION>();

                    for (int i = 0; i < transactionObject.Count(); i++)
                    {
                        PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILobj = transactionObject[i].PAY_TRANSACTION_DETAIL.FirstOrDefault();

                        if (PAY_TRANSACTION_DETAILobj != null)
                        {
                            if (PAY_TRANSACTION_DETAILobj.MAS_STUDENT.STATUS == "SUBMITTED")
                            {
                                transactionResponse.Add(transactionObject[i]);
                            }
                        }
                    }

                    //IQueryable<PAY_TRANSACTION_DETAIL> transactionObject = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_COUNTRY).Include(s => s.MAS_STUDENT).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Include(s => s.PAY_TRANSACTION).Include(s => s.MAS_STUDENT);
                    return Ok(transactionResponse);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("GetFilterRemittanceTransactions")]
        public IHttpActionResult GetFilterRemittanceTransactions([FromUri] string status, [FromUri] string substatus, [FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try { 
                if (ToDate != null)
                {
                    ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                string UserID = User.Identity.GetUserId();
                ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                int instituteID = 0;
                instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return BadRequest("Invalid Token");
                }

                List<PAY_TRANSACTION> transactionObject = null;

                if (status == null)
                {
                    return BadRequest("No Status Found");
                }

                transactionObject = db.PAY_TRANSACTION.Include(s=>s.PAY_TRANSACTION_DETAIL.Select(p=>p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteID && (s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "POST")).OrderByDescending(s => s.CREATED_ON).ToList();

                substatus = substatus.Substring(1, substatus.Length - 2);

                string[] substatusarray = substatus.Split(',').ToArray();

                if (substatusarray.Count() != 0)
                {
                    if (substatusarray[0] != "")
                    {
                        transactionObject = new List<PAY_TRANSACTION>();
                    }

                    for (int i = 0; i < substatusarray.Count(); i++)
                    {
                        if (substatusarray[i] == "PAYMENT_REALEASED")
                        {
                            var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == "ADMIN_POST").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                transactionObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "POSTED_TO_ACCOUNT")
                        {
                            var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == "POST").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                transactionObject.Add(tranList[k]);
                            }
                        }

                        if (substatusarray[i] == "PAYMENT_REALEASED")
                        {
                            var tranList = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == "ADMIN_UNPOST").ToList();
                            for (int k = 0; k < tranList.Count(); k++)
                            {
                                transactionObject.Add(tranList[k]);
                            }
                        }
                    }
                }

                if (FromDate != null)
                {
                    transactionObject = transactionObject.Where(s => s.CREATED_ON > FromDate).ToList();
                }

                if (ToDate != null)
                {
                    transactionObject = transactionObject.Where(s => s.CREATED_ON < ToDate).ToList();
                }

                transactionObject = transactionObject.OrderByDescending(s => s.CREATED_ON).ToList();

                //IQueryable<PAY_TRANSACTION_DETAIL> transactionObject = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_COUNTRY).Include(s => s.MAS_STUDENT).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Include(s => s.PAY_TRANSACTION).Include(s => s.MAS_STUDENT);
                return Ok(transactionObject);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }


        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("GetInstituteTransactions")]
        public IHttpActionResult GetInstituteTransactions([FromUri] string status, [FromUri] int instituteID, [FromUri] int itemperpage, [FromUri] int pagenumber)
        {
            try
            {
                pagenumber = pagenumber - 1;

                IQueryable<PAY_TRANSACTION> transactionObject = null;

                if (instituteID == 0)
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_BANK_DETAILS).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS.ToLower() == status.ToLower()).OrderByDescending(s => s.CREATED_ON).Skip(itemperpage * pagenumber).Take(itemperpage);
                }
                else
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_BANK_DETAILS).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS.ToLower() == status.ToLower() && s.CMN_INSTITUTE_ID == instituteID).OrderByDescending(s => s.CREATED_ON).Skip(itemperpage * pagenumber).Take(itemperpage);
                }

                if (transactionObject == null)
                {
                    return NotFound();
                }

                int count = 0;

                if (status.ToLower() == "all")
                {
                    count = db.PAY_TRANSACTION.Count();
                    //count = count / itemperpage;
                }
                else
                {
                    count = db.PAY_TRANSACTION.Where(s => s.STATUS.ToLower() == status.ToLower() && s.CMN_INSTITUTE_ID == instituteID).Count();
                    //count = count / itemperpage;
                }

                return Ok(formatObject(count, transactionObject.ToList()));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("GetBatchTransactions")]
        public IHttpActionResult AllBatchTransactions([FromUri] int instituteID)
        {
            try
            {
                IQueryable<PAY_TRANSACTION> transactionObject = null;
                int count = 0;

                if (instituteID == 0)
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s=>s.PAY_TRANSACTION_DETAIL.Select(p=>p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_BANK_DETAILS).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_BATCH_TRANSACTION).Include(s => s.MAS_COUNTRY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST").OrderByDescending(s => s.CREATED_ON);
                    count = db.PAY_TRANSACTION.Where(s => s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST").Count();
                }
                else
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_PAYMENT_TYPE)).Include(s => s.PAY_BANK_DETAILS).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_BATCH_TRANSACTION).Include(s => s.MAS_COUNTRY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => (s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST") && s.CMN_INSTITUTE_ID == instituteID).OrderByDescending(s => s.CREATED_ON);
                    count = db.PAY_TRANSACTION.Where(s => (s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST") && s.CMN_INSTITUTE_ID == instituteID).Count();
                }

                if (transactionObject == null)
                {
                    return NotFound();
                }

                //

                return Ok(formatObject(count, transactionObject.ToList()));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("GetInstituteBatchTransactions")]
        public IHttpActionResult GetInstituteBatchTransactions([FromUri] int instituteID)
        {
            try
            {
                if (instituteID == 0)
                {
                    return BadRequest("Invalid Token");
                }

                IQueryable<PAY_TRANSACTION> transactionObject = null;

                if (instituteID == 0)
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_BANK_DETAILS).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_BATCH_TRANSACTION).Include(s => s.MAS_COUNTRY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST").OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS);
                }
                else
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_BANK_DETAILS).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_BATCH_TRANSACTION).Include(s => s.MAS_COUNTRY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => (s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST") && s.CMN_INSTITUTE_ID == instituteID).OrderByDescending(s => s.CREATED_ON).ThenBy(s => s.ID).ThenBy(s => s.STATUS);
                }

                if (transactionObject == null)
                {
                    return NotFound();
                }

                //

                return Ok(transactionObject);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("Cancel")]
        public IHttpActionResult CancelTransaction([FromUri] int transactionID)
        {
            try{
                PAY_TRANSACTION transactionObject = db.PAY_TRANSACTION.Where(s=>s.ID == transactionID).FirstOrDefault();

                if (transactionObject == null)
                {
                    return Content(HttpStatusCode.NotFound, "Invalid Transaction ID");
                }

                if (User.IsInRole("Visitor"))
                {
                    if (transactionObject.USER_ID != User.Identity.GetUserId())
                    {
                        return Content(HttpStatusCode.NotFound, "You Dont have Access");
                    }
                }

                transactionObject.SUB_STATUS = "CANCELLED";
                transactionObject.UPDATED_BY = User.Identity.GetUserId();
                transactionObject.UPDATED_ON = DateTime.Now;

                db.Entry(transactionObject).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(transactionObject);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("OnHold")]
        public IHttpActionResult OnholdTransaction([FromUri] int transactionID)
        {
            try
            {
                PAY_TRANSACTION transactionObject = db.PAY_TRANSACTION.Where(s => s.ID == transactionID).FirstOrDefault();

                if (transactionObject == null)
                {
                    return Content(HttpStatusCode.NotFound, "Invalid Transaction ID");
                }

                transactionObject.SUB_STATUS = "ON_HOLD";
                transactionObject.UPDATED_BY = User.Identity.GetUserId();
                transactionObject.UPDATED_ON = DateTime.Now;

                db.Entry(transactionObject).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(transactionObject);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public formatingClass formatObject(int COUNT, List<PAY_TRANSACTION> tranObject)
        {
            return new formatingClass
            {
                total_count = COUNT,
                data = tranObject
            };
        }

        public class formatingClass
        {
            public int total_count { get; set; }
            public List<PAY_TRANSACTION> data { get; set; }
        }

        public class Body
        {
            public string status { get; set; }
        }

        [Authorize]
        [HttpGet]
        [Route("GetUserTransactions")]
        public IHttpActionResult GetUserTransactions([FromUri] string status)
        {
            try
            {
                string user = User.Identity.GetUserId();

                int countPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["CountPerPage"]);

                IQueryable<PAY_TRANSACTION> transactionObject = null;

                if (status.ToLower() == "pending")
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(k => k.MAS_STUDENT)).Include(s => s.MAS_COUNTRY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(k => k.MAS_COUNTRY)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).Where(s => s.USER_ID == user && (s.STATUS == "NEW" || s.STATUS == "RECEIVED" || s.STATUS == "ADMIN_UNPOST" || s.STATUS == "POST") && s.SUB_STATUS != "CANCELLED");
                }
                else if (status.ToLower() == "delivered")
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.MAS_COUNTRY).Include(s => s.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH)).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(k => k.MAS_COUNTRY)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(k => k.MAS_STUDENT)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).Where(s => s.USER_ID == user && (s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST") && s.SUB_STATUS != "CANCELLED");
                }
                else if (status.ToLower() == "cancelled")
                {
                    transactionObject = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(k => k.MAS_STUDENT)).Include(s => s.MAS_COUNTRY).Include(s => s.PAY_TRANSACTION_CANCEL_REASON).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(k => k.MAS_COUNTRY)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).Where(s => s.USER_ID == user && s.SUB_STATUS == "CANCELLED");
                }

                if (transactionObject == null)
                {
                    return NotFound();
                }

                return Ok(transactionObject);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class MonerisFormat
        {
            public string CURRENCY { get; set; }
            public string NAME { get; set; }
            public int ORDER_ID { get; set; }
            public string AMOUNT { get; set; }
            public string CARD_NUMBER { get; set; }
            public string CVV_NUMBER { get; set; }
            public string EXPIRY_DATE { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("Moneris")]
        public async Task<IHttpActionResult> MonerisPayment(MonerisFormat MonerisFormat)
        {
            //if (MonerisFormat.CURRENCY == "CAD")
            //{
            //    string order_id = "MRS" + DateTime.Now.ToString("yyyyMMddhhmmss");
            //    string store_id = System.Configuration.ConfigurationManager.AppSettings["MonerisCADKey"];
            //    string api_token = System.Configuration.ConfigurationManager.AppSettings["MonerisCADToken"];
            //    string amount = MonerisFormat.AMOUNT;
            //    string pan = MonerisFormat.CARD_NUMBER;
            //    MonerisFormat.EXPIRY_DATE = MonerisFormat.EXPIRY_DATE.Replace(@"/", "");
            //    string expdate = MonerisFormat.EXPIRY_DATE;
            //    string crypt = "7";
            //    string processing_country_code = "CA";
            //    bool status_check = false;
            //    double doubleAmount = Math.Round(double.Parse(MonerisFormat.AMOUNT), 3);
            //    string stringAmount = doubleAmount.ToString("0.00");

            //    Purchase purchase = new Purchase();
            //    purchase.SetOrderId(order_id);
            //    purchase.SetAmount(stringAmount);
            //    purchase.SetPan(pan);
            //    purchase.SetExpDate(expdate);
            //    purchase.SetCryptType(crypt);
            //    purchase.SetDynamicDescriptor("2134565");

            //    HttpsPostRequest mpgReq = new HttpsPostRequest();
            //    mpgReq.SetProcCountryCode(processing_country_code);
            //    // mpgReq.SetTestMode(true); //false or comment out this line for production transactions
            //    mpgReq.SetStoreId(store_id);
            //    mpgReq.SetApiToken(api_token);
            //    mpgReq.SetTransaction(purchase);
            //    mpgReq.SetStatusCheck(status_check);

            //    var sp = ServicePointManager.SecurityProtocol;
            //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //    mpgReq.Send();
            //    Receipt receipt = mpgReq.GetReceipt();
            //    ServicePointManager.SecurityProtocol = sp;
            //    string responceCode = receipt.GetResponseCode();
            //    System.Collections.Hashtable result = receipt.GetResult();
            //    monerisFormat monerisFormatObj = new monerisFormat();
            //    monerisFormatObj.responsecode = responceCode;
            //    monerisFormatObj.message = result["Message"].ToString();

            //    TRANSACTION_RESPONSE tr = new TRANSACTION_RESPONSE();
            //    tr.MERCHANT_REF_ID = result["ReferenceNum"].ToString();
            //    tr.TRANSACTION_ID = MonerisFormat.ORDER_ID;
            //    tr.PAYMENT_ID = order_id;
            //    tr.RESPONSE_CODE = responceCode;
            //    tr.MESSAGE = result["Message"].ToString();
                
            //    if(responceCode == "200")
            //    {
            //        tr.SUCCESS = true;
            //    }
            //    else
            //    {
            //        tr.SUCCESS = false;
            //    }

            //    tr.CREATED_ON = DateTime.Now;

            //    AspNetIdentityContext context = new AspNetIdentityContext();
            //    context.Entry(tr).State = EntityState.Added;
            //    context.TRANSACTION_RESPONSE.Add(tr);
            //    context.SaveChanges();

            //    return Ok(monerisFormatObj);
            //}
            //else if (MonerisFormat.CURRENCY == "USD")
            //{
            //    string order_id = "Test" + DateTime.Now.ToString("yyyyMMddhhmmss");
            //    string store_id = System.Configuration.ConfigurationManager.AppSettings["MonerisUSDKey"];
            //    string api_token = System.Configuration.ConfigurationManager.AppSettings["MonerisUSDToken"];
            //    string amount = MonerisFormat.AMOUNT;
            //    string pan = MonerisFormat.CARD_NUMBER;
            //    MonerisFormat.EXPIRY_DATE = MonerisFormat.EXPIRY_DATE.Replace(@"/", "");
            //    string expdate = MonerisFormat.EXPIRY_DATE;
            //    string crypt = "7";
            //    string processing_country_code = "CA";
            //    bool status_check = false;
            //    double doubleAmount = Math.Round(double.Parse(MonerisFormat.AMOUNT), 3);
            //    string stringAmount = doubleAmount.ToString("0.00");

            //    Purchase purchase = new Purchase();
            //    purchase.SetOrderId(order_id);
            //    purchase.SetAmount(stringAmount);
            //    purchase.SetPan(pan);
            //    purchase.SetExpDate(expdate);
            //    purchase.SetCryptType(crypt);
            //    purchase.SetDynamicDescriptor("2134565");

            //    HttpsPostRequest mpgReq = new HttpsPostRequest();
            //    mpgReq.SetProcCountryCode(processing_country_code);
            //    // mpgReq.SetTestMode(true); //false or comment out this line for production transactions
            //    mpgReq.SetStoreId(store_id);
            //    mpgReq.SetApiToken(api_token);
            //    mpgReq.SetTransaction(purchase);
            //    mpgReq.SetStatusCheck(status_check);

            //    var sp = ServicePointManager.SecurityProtocol;
            //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //    mpgReq.Send();
            //    Receipt receipt = mpgReq.GetReceipt();
            //    ServicePointManager.SecurityProtocol = sp;
            //    string responceCode = receipt.GetResponseCode();
            //    System.Collections.Hashtable result = receipt.GetResult();
            //    monerisFormat monerisFormatObj = new monerisFormat();
            //    monerisFormatObj.responsecode = responceCode;
            //    monerisFormatObj.message = result["Message"].ToString();


            //    TRANSACTION_RESPONSE tr = new TRANSACTION_RESPONSE();
            //    tr.MERCHANT_REF_ID = result["ReferenceNum"].ToString();
            //    tr.TRANSACTION_ID = MonerisFormat.ORDER_ID;
            //    tr.PAYMENT_ID = order_id;
            //    tr.RESPONSE_CODE = responceCode;
            //    tr.MESSAGE = result["Message"].ToString();

            //    if (responceCode == "200")
            //    {
            //        tr.SUCCESS = true;
            //    }
            //    else
            //    {
            //        tr.SUCCESS = false;
            //    }

            //    tr.CREATED_ON = DateTime.Now;

            //    AspNetIdentityContext context = new AspNetIdentityContext();
            //    context.Entry(tr).State = EntityState.Added;
            //    context.TRANSACTION_RESPONSE.Add(tr);
            //    context.SaveChanges();

            //    return Ok(monerisFormatObj);
            //}
            //else
            //{
                AspNetIdentityContext context = new AspNetIdentityContext();

                PAY_SAFE paySafe = context.PAY_SAFE.Where(s => s.CURRENCY == MonerisFormat.CURRENCY).FirstOrDefault();

                PAY_TRANSACTION pay_tran = db.PAY_TRANSACTION.Include(s=>s.CMN_CURRENCY).Include(s=>s.CMN_INSTITUTE).Include(s=>s.CMN_INSTITUTE.CMN_CURRENCY).Include(s=>s.CMN_PAYMENT_METHOD1).Include(s=>s.MAS_COUNTRY).Include(s=>s.PAY_TRANSACTION_DETAIL.Select(p=>p.MAS_COUNTRY)).Where(s => s.ID == MonerisFormat.ORDER_ID).FirstOrDefault();

                ApplicationDbContext usercontext = new ApplicationDbContext();
                ApplicationUser user = usercontext.Users.Where(s => s.Id == pay_tran.USER_ID).FirstOrDefault();

                if (paySafe == null)
                {
                    monerisFormat monerisFormat = new TransactionController.monerisFormat();
                    monerisFormat.responsecode = "400";
                    monerisFormat.message = "CURRENCY NOT FOUND";
                    return Ok(monerisFormat);
                }

                string apiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];
                string apiSecret = System.Configuration.ConfigurationManager.AppSettings["ApiSecret"];
                string accountNumber = paySafe.FMA;//System.Configuration.ConfigurationManager.AppSettings["accountNumber"];
                int currencyBaseUnitsMultiplier = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrencyBaseUnitsMultiplier"]);

                try
                {
                    String UUID = Guid.NewGuid().ToString();
                    double doubleAmount = Math.Round(double.Parse(MonerisFormat.AMOUNT) * double.Parse(paySafe.DBA_NAME), 3);
                    int amount = Convert.ToInt32(doubleAmount);
                    MonerisFormat.EXPIRY_DATE = MonerisFormat.EXPIRY_DATE.Replace(@"/", "");
                    string subMonth = MonerisFormat.EXPIRY_DATE.Substring(0, 2);
                    string subYear = "20" + MonerisFormat.EXPIRY_DATE.Substring(2, 2);

                    int month = Convert.ToInt32(subMonth);
                    int year = Convert.ToInt32(subYear);

                    PaysafeApiClient client = new PaysafeApiClient(apiKey, apiSecret, Paysafe.Environment.LIVE, accountNumber);

                    bool isOnline = client.cardPaymentService().monitor();

                    //Paysafe.CardPayments.Authorization auth = client.cardPaymentService().authorize(Paysafe.CardPayments.Authorization.Builder()
                    //               .merchantRefNum("439ef627-dcfe-46f5-aa01-9a02fee85bab")
                    //               .amount(203333)
                    //               .settleWithAuth(true)
                    //               .card()
                    //                   .cardNum("5300721105998550")
                    //                   .cvv("387")
                    //                   .cardExpiry()
                    //                       .month(11)
                    //                       .year(2017)
                    //                       .Done()
                    //                   .Done()
                    //                   .billingDetails()
                    //                       .zip("M5H 2N2")
                    //                       .Done()
                    //               .Build());

                    //Paysafe.CardPayments.Authorization response = client.cardPaymentService().authorize(Paysafe.CardPayments.Authorization.Builder()
                    //    .merchantRefNum(UUID)
                    //    .amount(amount)
                    //    .settleWithAuth(true)
                    //    .card()
                    //        .cardNum(MonerisFormat.CARD_NUMBER)
                    //        .cvv(MonerisFormat.CVV_NUMBER)
                    //        .cardExpiry()
                    //            .month(month)
                    //            .year(year)
                    //            .Done()
                    //        .Done()
                    //        .merchantDescriptor()
                    //        .billingDetails()
                    //            .country(pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.MAS_COUNTRY.ISO).FirstOrDefault())
                    //            .street(pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_ADDRESS_LINE1).FirstOrDefault())
                    //            .zip("M5H 2N2")
                    //            .Done()
                    //    .Build());

                    var baseAddress = new Uri("https://api.paysafe.com/cardpayments/");

                    using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation("authorization", "Basic " + apiSecret + "");

                        string StateCode = "";

                        if (pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_MAS_STATE).FirstOrDefault() != null)
                        {
                            if (pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.MAS_COUNTRY.ISO).FirstOrDefault() == "US" || pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.MAS_COUNTRY.ISO).FirstOrDefault() == "CA")
                            {
                                string statestr = pay_tran.PAY_TRANSACTION_DETAIL.Select(p => p.PAYEE_MAS_STATE).FirstOrDefault();

                                MAS_STATE_CODES MAS_STATE_CODESObj = context.MAS_STATE_CODES.Where(s => s.STATE_NAME == statestr).FirstOrDefault();

                                if (MAS_STATE_CODESObj != null)
                                {
                                    StateCode = MAS_STATE_CODESObj.STATE_CODE;
                                }
                                else
                                {
                                    StateCode = pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_MAS_STATE).FirstOrDefault();
                                }
                            }
                            else
                            {
                                StateCode = pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_MAS_STATE).FirstOrDefault();
                            }
                        }

                        string address1 = pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_ADDRESS_LINE1).FirstOrDefault();

                        if (address1 != null)
                        {
                            if (address1.Length > 50)
                            {
                                address1 = address1.Substring(0, 50);
                            }
                        }

                        string address2 = pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_ADDRESS_LINE2).FirstOrDefault();
                        if (address2 != null)
                        {
                            if (address2.Length > 50)
                            {
                                address2 = address2.Substring(0, 50);
                            }
                        }

                        if (MonerisFormat.CARD_NUMBER != null)
                        {
                            if (MonerisFormat.CARD_NUMBER.Length > 20)
                            {
                                MonerisFormat.CARD_NUMBER = MonerisFormat.CARD_NUMBER.Substring(0, 20);
                            }
                        }

                        if (MonerisFormat.CARD_NUMBER != null)
                        {
                            if (MonerisFormat.NAME.Length > 40)
                            {
                                MonerisFormat.NAME = MonerisFormat.NAME.Substring(0, 40);
                            }
                        }
                        
                        string city = pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_MAS_CITY).FirstOrDefault();

                        if (city != null)
                        {
                            if (city.Length > 40)
                            {
                                city = city.Substring(0, 40);
                            }
                        }

                        if (StateCode != null)
                        {
                            if (StateCode.Length > 40)
                            {
                                StateCode = StateCode.Substring(0, 40);
                            }
                        }

                        if (MonerisFormat.CVV_NUMBER != null)
                        {
                            if (MonerisFormat.CVV_NUMBER.Length > 4)
                            {
                                MonerisFormat.CVV_NUMBER = MonerisFormat.CVV_NUMBER.Substring(0, 4);
                            }
                        }

                        string zipCode = pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.PAYEE_POSTAL_CODE).FirstOrDefault();
                        if (zipCode != null)
                        {
                            if (zipCode.Length > 10)
                            {
                                zipCode = zipCode.Substring(0, 10);
                            }
                        }
                        
                        string tranJson  = "{  \"merchantRefNum\": \"" + UUID + "\",  \"amount\": " + amount + ",  \"settleWithAuth\": true,  \"card\": {    \"cardNum\": \"" + MonerisFormat.CARD_NUMBER + "\",    \"cardExpiry\": {      \"month\": " + month + ",      \"year\": " + year + "    },    \"cvv\": \"" + MonerisFormat.CVV_NUMBER + "\"  },  \"profile\": {    \"firstName\": \"" + MonerisFormat.NAME + "\",    \"lastName\": \"\",    \"email\": \"" + user.Email + "\"  },  \"keywords\": [    \"Repeat Customer\",    \"Profile Stored\"  ],  \"billingDetails\": {    \"street\": \"" + address1 + "\", \"street2\": \"" + address2 + "\",  \"city\": \"" + city + "\", \"state\": \"" + StateCode + "\", \"country\": \"" + pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.MAS_COUNTRY.ISO).FirstOrDefault() + "\",    \"zip\": \"" + zipCode + "\"  },  \"description\": \"Tuition payment\"}";

                        string tranCustomJson = "{  \"merchantRefNum\": \"" + UUID + "\",  \"amount\": " + amount + ",  \"settleWithAuth\": true,  \"card\": {    \"cardNum\": \"" + "xxx-xxxx-xxxx-" + MonerisFormat.CARD_NUMBER.Substring(12) + "\",    \"cardExpiry\": {      \"month\": " + month + ",      \"year\": " + year + "    },    \"cvv\": \"" + MonerisFormat.CVV_NUMBER + "\"  },  \"profile\": {    \"firstName\": \"" + MonerisFormat.NAME + "\",    \"lastName\": \"\",    \"email\": \"" + user.Email + "\"  },  \"keywords\": [    \"Repeat Customer\",    \"Profile Stored\"  ],  \"billingDetails\": {    \"street\": \"" + address1 + "\", \"street2\": \"" + address2 + "\",  \"city\": \"" + city + "\", \"state\": \"" + StateCode + "\", \"country\": \"" + pay_tran.PAY_TRANSACTION_DETAIL.Select(s => s.MAS_COUNTRY.ISO).FirstOrDefault() + "\",    \"zip\": \"" + zipCode + "\"  },  \"description\": \"Tuition payment\"}";

                        Log.Debug(tranCustomJson);
                        pay_tran.JSON = tranCustomJson;

                        StringContent sc = new StringContent(tranJson, System.Text.Encoding.Default, "application/json");

                        using (var content = sc)
                        {
                            using (var response = await httpClient.PostAsync("v1/accounts/" + accountNumber + "/auths", content))
                            {
                                monerisFormat monerisFormat = new TransactionController.monerisFormat();
                                //response.StatusCode = HttpStatusCode.OK;

                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    monerisFormat.responsecode = "200";
                                    monerisFormat.message = "SUCCESS";

                                    TRANSACTION_RESPONSE tr = new TRANSACTION_RESPONSE();
                                    tr.MERCHANT_REF_ID = UUID;
                                    tr.TRANSACTION_ID = MonerisFormat.ORDER_ID;
                                    tr.PAYMENT_ID = "";
                                    tr.RESPONSE_CODE = "200";
                                    tr.MESSAGE = "SUCCESS";

                                    tr.SUCCESS = true;

                                    tr.CREATED_ON = DateTime.Now;

                                    AspNetIdentityContext contextDB = new AspNetIdentityContext();
                                    contextDB.Entry(tr).State = EntityState.Added;
                                    contextDB.TRANSACTION_RESPONSE.Add(tr);
                                    contextDB.SaveChanges();

                                    pay_tran.STATUS = "RECEIVED";
                                    pay_tran.UPDATED_BY = User.Identity.GetUserId();
                                    pay_tran.UPDATED_ON = DateTime.Now;
                                    db.Entry(pay_tran).State = EntityState.Modified;
                                    db.SaveChanges();

                                    PAY_TRANSACTION_DETAIL payTranDetail = pay_tran.PAY_TRANSACTION_DETAIL.FirstOrDefault();

                                    string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/PaymentConfirm.html"));
                                    Body = Body.Replace("#STUDENTNAME#", payTranDetail.STUDENT_FIRST_NAME + " " + payTranDetail.STUDENT_LAST_NAME);
                                    string amountPaying = String.Format("{0:n}", pay_tran.AMOUNT_PAYING);
                                    string amountfx = String.Format("{0:n}", pay_tran.AMOUNT_FX);
                                    Body = Body.Replace("#SCHOOLAMOUNT#", amountPaying);
                                    Body = Body.Replace("#SCHOOLCURRENCY#", pay_tran.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL);
                                    Body = Body.Replace("#INSTITUTE#", pay_tran.CMN_INSTITUTE.INSTITUTE_NAME);
                                    Body = Body.Replace("#PAYMENTOPTION#", pay_tran.CMN_PAYMENT_METHOD1.METHOD_NAME);
                                    Body = Body.Replace("#AMOUNT#", amountfx);
                                    Body = Body.Replace("#CURRENCY#", pay_tran.CMN_CURRENCY.CURR_SYMBOL);
                                    Body = Body.Replace("#LINK#", ConfigurationManager.AppSettings["mainlink"].ToString());

                                    EMAIL_QUEUE eq = new EMAIL_QUEUE();
                                    eq.SUBJECT = "Payment Confirmed for " + pay_tran.CMN_INSTITUTE.INSTITUTE_NAME;
                                    eq.BODY = Body;
                                    eq.DATE_CREATED = DateTime.Now;
                                    eq.EMAIL_TO = user.Email;
                                    eq.BCC = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                                    contextDB.Entry(eq).State = EntityState.Added;
                                    contextDB.EMAIL_QUEUE.Add(eq);
                                    contextDB.SaveChanges();
                                }
                                else
                                {
                                    var responseData = await response.Content.ReadAsStringAsync();
                                    JsonParseObject model = JsonConvert.DeserializeObject<JsonParseObject>(responseData);
                                    monerisFormat.responsecode = model.error.code;
                                    monerisFormat.message = model.error.message;

                                    TRANSACTION_RESPONSE tr = new TRANSACTION_RESPONSE();
                                    tr.MERCHANT_REF_ID = UUID;
                                    tr.TRANSACTION_ID = MonerisFormat.ORDER_ID;
                                    tr.PAYMENT_ID = model.id;
                                    tr.RESPONSE_CODE = model.error.code;
                                    tr.MESSAGE = model.error.message;

                                    tr.SUCCESS = false;

                                    tr.CREATED_ON = DateTime.Now;

                                    AspNetIdentityContext contextDB = new AspNetIdentityContext();
                                    contextDB.Entry(tr).State = EntityState.Added;
                                    contextDB.TRANSACTION_RESPONSE.Add(tr);
                                    contextDB.SaveChanges();

                                    string Body = "Transaction # " + pay_tran.PREFIX + " is rejected. Reason : " + model.error.message;

                                    EMAIL_QUEUE eq = new EMAIL_QUEUE();
                                    eq.SUBJECT = "Transaction is rejected";
                                    eq.BODY = Body;
                                    eq.DATE_CREATED = DateTime.Now;
                                    eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();
                                    eq.BCC = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                                    AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

                                    Studentcontext.Entry(eq).State = EntityState.Added;
                                    Studentcontext.EMAIL_QUEUE.Add(eq);
                                    Studentcontext.SaveChanges();

                                    //try
                                    //{
                                    //    string BodyTwo = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/PendingEmail.html"));
                                    //    BodyTwo = BodyTwo.Replace("#NAME#", user.FirstName + " " + user.LastName);
                                    //    string amountTwo = String.Format("{0:n}", pay_tran.AMOUNT_PAYING);
                                    //    string amountfx = String.Format("{0:n}", pay_tran.AMOUNT_FX);
                                    //    BodyTwo = BodyTwo.Replace("#AMOUNTFX#", amountTwo);
                                    //    BodyTwo = BodyTwo.Replace("#INSTITUTECURRENCY#", pay_tran.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL);
                                    //    BodyTwo = BodyTwo.Replace("#INSTITUTION#", pay_tran.CMN_INSTITUTE.INSTITUTE_NAME);
                                    //    BodyTwo = BodyTwo.Replace("#AMOUNT#", amountfx);
                                    //    BodyTwo = BodyTwo.Replace("#PAYMENTMETHOD#", pay_tran.CMN_PAYMENT_METHOD1.METHOD_NAME);
                                    //    BodyTwo = BodyTwo.Replace("#CURRENCY#", pay_tran.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL);
                                    //    BodyTwo = BodyTwo.Replace("#RATE#", pay_tran.EXCHANGE_RATE.ToString());
                                    //    BodyTwo = BodyTwo.Replace("#LINK#", ConfigurationManager.AppSettings["mainlink"].ToString());
                                    //    DateTime dt = pay_tran.EXPIRY_DATE ?? DateTime.Now;
                                    //    string now = dt.ToString("MM/dd/yyyy");
                                    //    BodyTwo = BodyTwo.Replace("#EXPIRY#", now);
                                    //    BodyTwo = BodyTwo.Replace("#PAYMENTID#", pay_tran.PREFIX);

                                    //    EMAIL_QUEUE equ = new EMAIL_QUEUE();
                                    //    equ.SUBJECT = "Payment Pending for " + pay_tran.CMN_INSTITUTE.INSTITUTE_NAME;
                                    //    equ.BODY = BodyTwo;
                                    //    equ.DATE_CREATED = DateTime.Now;
                                    //    equ.EMAIL_TO = user.Email;
                                    //    equ.BCC = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                                    //    Studentcontext.Entry(equ).State = EntityState.Added;
                                    //    Studentcontext.EMAIL_QUEUE.Add(equ);
                                    //    Studentcontext.SaveChanges();
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    Log.Error(ex.Message);
                                    //}
                                }
                                //TRANSACTION_RESPONSE tr = new TRANSACTION_RESPONSE();
                                //tr.MERCHANT_REF_ID = responseData.id().ToString();
                                //tr.TRANSACTION_ID = MonerisFormat.ORDER_ID;
                                //tr.PAYMENT_ID = UUID;
                                //tr.RESPONSE_CODE = response.authCode();
                                //tr.MESSAGE = "";
                                ////getdetaisl
                                //tr.SUCCESS = true;

                                //tr.CREATED_ON = DateTime.Now;

                                //AspNetIdentityContext contextDB = new AspNetIdentityContext();
                                //contextDB.Entry(tr).State = EntityState.Added;
                                //contextDB.TRANSACTION_RESPONSE.Add(tr);
                                //contextDB.SaveChanges();

                                return Ok(monerisFormat);
                            }
                        }
                    }
                    
                    //auth.currencyCode(MonerisFormat.CURRENCY);
                    //Paysafe.CardPayments.Authorization response = client.cardPaymentService().authorize(auth);
                }
                catch (Exception ex)
                {
                    monerisFormat monerisFormat = new TransactionController.monerisFormat();
                    int code = ((Paysafe.Common.PaysafeException)(ex.GetBaseException())).cpde();
                    monerisFormat.responsecode = code.ToString();
                    monerisFormat.message = ex.Message;

                    TRANSACTION_RESPONSE tr = new TRANSACTION_RESPONSE();
                    tr.MERCHANT_REF_ID = "";
                    tr.TRANSACTION_ID = MonerisFormat.ORDER_ID;
                    tr.PAYMENT_ID = "";
                    tr.RESPONSE_CODE = code.ToString();
                    tr.MESSAGE = ex.Message + " " + ex.InnerException;

                    tr.SUCCESS = false;

                    tr.CREATED_ON = DateTime.Now;

                    AspNetIdentityContext contextDB = new AspNetIdentityContext();
                    contextDB.Entry(tr).State = EntityState.Added;
                    contextDB.TRANSACTION_RESPONSE.Add(tr);
                    contextDB.SaveChanges();

                    return Ok(monerisFormat);
                }
            //}
        }

        public class JsonParseObject
        {
            public links[] links { get; set; }
            public string id { get; set; }
            public string merchantRefNum { get; set; }
            public error error { get; set; }
            public int[] riskReasonCode { get; set; }
            public bool settleWithAuth { get; set; }
        }

        public class links
        {
            public string rel{get;set;}
            public string href{get;set;}

        }

        public class error
        {
            public string code { get; set; }
            public string message { get; set; }
            public links[] links { get; set; }
        }

        public class monerisFormat
        {
            public string responsecode { get; set; }
            public string message { get; set; }
        }

        [Authorize]
        [HttpGet]
        [Route("getlasttransaction")]
        [ResponseType(typeof(PAY_TRANSACTION_DETAIL))]
        public IHttpActionResult LastTransactions()
        {
            try
            {
                TransactionDetailContext context = new TransactionDetailContext();
                string user = User.Identity.GetUserId();

                PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILObj = context.PAY_TRANSACTION_DETAIL.Include(s => s.PAY_TRANSACTION.MAS_COUNTRY).Include(s => s.MAS_SEMESTER).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_STUDENT).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).OrderByDescending(s => s.CREATED_ON).Where(s => s.CREATED_BY == user).FirstOrDefault();

                if (PAY_TRANSACTION_DETAILObj == null)
                {
                    return Ok("");
                }

                if (PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE != "")
                {
                    PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE = "{\"upload\":{},\"progress\":100,\"result\":\"" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE + "\"}";
                }
                else
                {
                    PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE = "";
                }

                if (PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE != "")
                {
                    PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE = "{\"upload\":{},\"progress\":100,\"result\":\"" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE + "\"}";
                }
                else
                {
                    PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE = "";
                }

                if (PAY_TRANSACTION_DETAILObj.SECONDARY_DOCUMENT_FILE != "")
                {
                    PAY_TRANSACTION_DETAILObj.SECONDARY_DOCUMENT_FILE = "{\"upload\":{},\"progress\":100,\"result\":\"" + PAY_TRANSACTION_DETAILObj.SECONDARY_DOCUMENT_FILE + "\"}";
                }
                else
                {
                    PAY_TRANSACTION_DETAILObj.SECONDARY_DOCUMENT_FILE = "";
                }

                if (PAY_TRANSACTION_DETAILObj.ACCEPTANCE_LETTER != "")
                {
                    PAY_TRANSACTION_DETAILObj.ACCEPTANCE_LETTER = "{\"upload\":{},\"progress\":100,\"result\":\"" + PAY_TRANSACTION_DETAILObj.ACCEPTANCE_LETTER + "\"}";
                }
                else
                {
                    PAY_TRANSACTION_DETAILObj.ACCEPTANCE_LETTER = "";
                }

                if (PAY_TRANSACTION_DETAILObj.BANK_INSTRUCTIONS_DOCUMENT_FILE != "")
                {
                    PAY_TRANSACTION_DETAILObj.BANK_INSTRUCTIONS_DOCUMENT_FILE = "{\"upload\":{},\"progress\":100,\"result\":\"" + PAY_TRANSACTION_DETAILObj.BANK_INSTRUCTIONS_DOCUMENT_FILE + "\"}";
                }
                else
                {
                    PAY_TRANSACTION_DETAILObj.BANK_INSTRUCTIONS_DOCUMENT_FILE = "";
                }

                PAY_TRANSACTION_DETAILObj.STUDENT_DATE_OF_BIRTH = PAY_TRANSACTION_DETAILObj.STUDENT_DATE_OF_BIRTH.Value.AddHours(8);

                return Ok(PAY_TRANSACTION_DETAILObj);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("PaymentSent")]
        public IHttpActionResult PaymentSent([FromUri] int transactionid, [FromUri] DateTime date)
        {
            try
            {
                if (transactionid < 0)
                {
                    return BadRequest("transactionid is not valid");
                }

                PAY_TRANSACTION PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Where(s => s.ID == transactionid).FirstOrDefault();

                if (PAY_TRANSACTIONMethod == null)
                {
                    return BadRequest("No Transaction found.");
                }

                if (PAY_TRANSACTIONMethod.USER_ID != User.Identity.GetUserId())
                {
                    return BadRequest("Access Denied.");
                }

                PAY_TRANSACTIONMethod.PAYMENT_SENT_DATE = date.Date;
                PAY_TRANSACTIONMethod.UPDATED_BY = User.Identity.GetUserId();
                PAY_TRANSACTIONMethod.UPDATED_ON = DateTime.Now;
                db.Entry(PAY_TRANSACTIONMethod).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(formingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("NeedMoreTime")]
        public IHttpActionResult NeedMoreTime([FromUri] int transactionid, [FromUri] int days)
        {
            try
            {
                if (transactionid < 0)
                {
                    return BadRequest("transactionid is not valid");
                }

                PAY_TRANSACTION PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Where(s => s.ID == transactionid).FirstOrDefault();

                if (PAY_TRANSACTIONMethod == null)
                {
                    return BadRequest("No Transaction found.");
                }

                if (PAY_TRANSACTIONMethod.USER_ID != User.Identity.GetUserId())
                {
                    return BadRequest("Access Denied.");
                }

                if (PAY_TRANSACTIONMethod.NEED_DAYS == false)
                {
                    DateTime dt = DateTime.Now;

                    try
                    {
                        dt = (DateTime)PAY_TRANSACTIONMethod.EXPIRY_DATE;
                        dt = dt.AddDays(days);

                        if (dt.DayOfWeek.ToString().ToLower() == "saturday")
                        {
                            dt = dt.AddDays(2);
                        }

                        if (dt.DayOfWeek.ToString().ToLower() == "sunday")
                        {
                            dt = dt.AddDays(1);
                        }
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message.ToString());
                    }

                    PAY_TRANSACTIONMethod.NEED_DAYS = true;
                    PAY_TRANSACTIONMethod.EXPIRY_DATE = dt;
                    PAY_TRANSACTIONMethod.UPDATED_BY = User.Identity.GetUserId();
                    PAY_TRANSACTIONMethod.UPDATED_ON = DateTime.Now;
                    db.Entry(PAY_TRANSACTIONMethod).State = EntityState.Modified;
                    db.SaveChanges();

                    return Ok(formingFunc(dt.ToString("dd/MM/yyyy")));
                }
                else
                {
                    return Ok(formingFunc("Time Already Taken."));
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("GetDeliveredTransaction")]
        public IHttpActionResult DeliveredTransactions()
        {
            try
            {
                TransactionDetailContext context = new TransactionDetailContext();
                string user = User.Identity.GetUserId();

                List<transactionFormat> transactionFormatList = new List<transactionFormat>();

                List<PAY_TRANSACTION> transactionObject = db.PAY_TRANSACTION.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).OrderByDescending(s => s.CREATED_ON).Where(s => s.USER_ID == user && s.STATUS == "ADMIN_POST").ToList();

                for (int i = 0; i < transactionObject.Count(); i++)
                {
                    transactionFormat transactionFormatObj = new transactionFormat();
                    int tranID = transactionObject[i].ID;
                    PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILObj = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_COUNTRY).Include(s => s.MAS_STUDENT).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).OrderByDescending(s => s.CREATED_ON).Where(s => s.ID == tranID).FirstOrDefault();
                    transactionFormatObj.PAY_TRANSACTION = transactionObject[i];
                    transactionFormatObj.PAY_TRANSACTION_DETAIL = PAY_TRANSACTION_DETAILObj;

                    transactionFormatList.Add(transactionFormatObj);
                }

                return Ok(transactionFormatList);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class transactionFormat
        {
            public PAY_TRANSACTION PAY_TRANSACTION { get; set; }
            public PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAIL { get; set; }
        }

        [ResponseType(typeof(PAY_TRANSACTION))]
        public IHttpActionResult GetPAY_TRANSACTION(int id)
        {
            PAY_TRANSACTION PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Include(s=>s.PAY_TRANSACTION_DETAIL.Select(l=>l.MAS_STUDENT)).Include(s=>s.CMN_PAYMENT_METHOD1).Include(t => t.MAS_COUNTRY).Include(t => t.CMN_INSTITUTE.CMN_CURRENCY).Include(t => t.CMN_CURRENCY).Where(s=>s.ID == id).FirstOrDefault();

            if (PAY_TRANSACTIONMethod == null)
            {
                return NotFound();
            }
            
            return Ok(PAY_TRANSACTIONMethod);
        }

        [Authorize]
        [HttpGet]
        [Route("GetDetails")]
        public IHttpActionResult GetDetails([FromUri] string uid, [FromUri] string methodName)
        {
            try
            {
                PAY_TRANSACTION PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.MAS_COUNTRY).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.UUID == uid).FirstOrDefault();

                if (PAY_TRANSACTIONMethod == null)
                {
                    return NotFound();
                }

                if (PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("Domestic"))
                {
                    TransactionDetailContext context = new TransactionDetailContext();

                    transactionFormat tfObj = new transactionFormat();
                    tfObj.PAY_TRANSACTION = PAY_TRANSACTIONMethod;
                    tfObj.PAY_TRANSACTION_DETAIL = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_STUDENT).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_SEMESTER).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Where(s => s.PAY_TRANSACTION_ID == PAY_TRANSACTIONMethod.ID).FirstOrDefault();

                    return Ok(tfObj);
                }

                if (PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains(methodName))
                {
                    TransactionDetailContext context = new TransactionDetailContext();

                    transactionFormat tfObj = new transactionFormat();
                    tfObj.PAY_TRANSACTION = PAY_TRANSACTIONMethod;
                    tfObj.PAY_TRANSACTION_DETAIL = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_STUDENT).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_SEMESTER).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Where(s => s.PAY_TRANSACTION_ID == PAY_TRANSACTIONMethod.ID).FirstOrDefault();

                    return Ok(tfObj);
                }
                else
                {
                    return BadRequest(PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD1.METHOD_NAME);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("deleteTransaction")]
        public IHttpActionResult deleteTransaction([FromUri] int tranid)
        {
            try
            {
                if (tranid == 0)
                {
                    return BadRequest();
                }

                AspNetIdentity.Infrastructure.TransactionContext contextObj = new AspNetIdentity.Infrastructure.TransactionContext();

                PAY_TRANSACTION PAY_TRANSACTIONObj = db.PAY_TRANSACTION.Where(s => s.ID == tranid).FirstOrDefault();

                if (PAY_TRANSACTIONObj == null)
                {
                    return BadRequest("Wrong tranID or Transaction DoesNot Exist");
                }

                TransactionDetailContext contextDetail = new TransactionDetailContext();

                PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILObj = contextDetail.PAY_TRANSACTION_DETAIL.Where(s => s.PAY_TRANSACTION_ID == PAY_TRANSACTIONObj.ID).FirstOrDefault();
                
                AspNetIdentityContext cancelContext = new AspNetIdentityContext();
                PAY_TRANSACTION_CANCEL_REASON PAY_TRANSACTION_CANCEL_REASONObj = cancelContext.PAY_TRANSACTION_CANCEL_REASON.Where(s => s.TRANSACTION_ID == PAY_TRANSACTIONObj.ID).FirstOrDefault();

                if (PAY_TRANSACTION_CANCEL_REASONObj != null)
                {
                    //Deleting PAY_TRANSACTION_CANCEL_REASON
                    cancelContext.Entry(PAY_TRANSACTION_CANCEL_REASONObj).State = EntityState.Deleted;
                    cancelContext.SaveChanges();
                }

                List<PAY_TRANSACTION_NOTES> PAY_TRANSACTION_NOTESList = cancelContext.PAY_TRANSACTION_NOTES.Where(s=>s.PAY_TRANSACTION_ID == PAY_TRANSACTIONObj.ID).ToList();

                for(int i =0 ;i<PAY_TRANSACTION_NOTESList.Count(); i++)
                {
                    int ID  = PAY_TRANSACTION_NOTESList[i].ID;
                    PAY_TRANSACTION_NOTES PAY_TRANSACTION_NOTESObj = cancelContext.PAY_TRANSACTION_NOTES.Where(s => s.ID == ID).FirstOrDefault();
                    cancelContext.Entry(PAY_TRANSACTION_NOTESObj).State = EntityState.Deleted;
                    cancelContext.SaveChanges();
                }

                if (PAY_TRANSACTION_DETAILObj != null)
                {
                    contextDetail.Entry(PAY_TRANSACTION_DETAILObj).State = EntityState.Deleted;
                    contextDetail.SaveChanges();
                }

                PAY_BATCH_TRANSACTION PAY_BATCH_TRANSACTIONObj = cancelContext.PAY_BATCH_TRANSACTION.Where(s => s.TRANSACTION_ID == PAY_TRANSACTIONObj.ID).FirstOrDefault();

                if (PAY_BATCH_TRANSACTIONObj != null)
                {
                    cancelContext.Entry(PAY_BATCH_TRANSACTIONObj).State = EntityState.Deleted;
                    cancelContext.SaveChanges();
                }

                db.Entry(PAY_TRANSACTIONObj).State = EntityState.Deleted;
                db.SaveChanges();

                return Ok("Success!");
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("create")]
        [Authorize]
        [ResponseType(typeof(PAY_TRANSACTION))]
        public IHttpActionResult PostPAY_TRANSACTION(PAY_TRANSACTION PAY_TRANSACTIONMethod)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                AspNetIdentityContext context = new AspNetIdentityContext();

                CMN_PAYMENT_METHOD paymentMethod = context.CMN_PAYMENT_METHOD.Where(s => s.ID == PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD).FirstOrDefault();

                if (paymentMethod.METHOD_NAME.ToLower() == "online bill payment")
                {
                    CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj = context.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_INSTITUTE_ID == PAY_TRANSACTIONMethod.CMN_INSTITUTE_ID && s.MAS_COUNTRY_ID == PAY_TRANSACTIONMethod.MAS_COUNTRY_ID && s.CMN_PAYMENT_METHOD_ID == PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD).FirstOrDefault();

                    if (CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj == null)
                    {
                        return BadRequest("Bank Details Not Found");
                    }

                    PAY_BANK_DETAILS bankDetail = context.PAY_BANK_DETAILS.Where(s => s.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS_ID == CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj.ID && s.MAS_INSTITUTE_ID == PAY_TRANSACTIONMethod.CMN_INSTITUTE_ID).FirstOrDefault();

                    if (bankDetail != null)
                    {
                        PAY_TRANSACTIONMethod.MAS_BANK_ID = bankDetail.ID;
                    }
                }

                String UUID = Guid.NewGuid().ToString();

                PAY_TRANSACTIONMethod.NEED_DAYS = false;
                PAY_TRANSACTIONMethod.UUID = UUID;
                PAY_TRANSACTIONMethod.EXPIRY_DATE = DateTime.Now;

                for (int i = 0; i < 2; i++)
                {
                    PAY_TRANSACTIONMethod.EXPIRY_DATE = PAY_TRANSACTIONMethod.EXPIRY_DATE.Value.AddDays(1);

                    if (PAY_TRANSACTIONMethod.EXPIRY_DATE.Value.DayOfWeek.ToString().ToLower() == "saturday")
                    {
                        PAY_TRANSACTIONMethod.EXPIRY_DATE = PAY_TRANSACTIONMethod.EXPIRY_DATE.Value.AddDays(2);
                    }

                    if (PAY_TRANSACTIONMethod.EXPIRY_DATE.Value.DayOfWeek.ToString().ToLower() == "sunday")
                    {
                        PAY_TRANSACTIONMethod.EXPIRY_DATE = PAY_TRANSACTIONMethod.EXPIRY_DATE.Value.AddDays(1);
                    }
                }

                //double amounttotal = double.Parse((PAY_TRANSACTIONMethod.EXCHANGE_RATE * PAY_TRANSACTIONMethod.AMOUNT_PAYING).ToString());
                //double amount = Math.Round(amounttotal, 0);

                ////AspNetIdentityContext contextObj = new AspNetIdentityContext();
                //CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS Obj = context.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_PAYMENT_METHOD_ID == PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD && s.CMN_INSTITUTE_ID == PAY_TRANSACTIONMethod.CMN_INSTITUTE_ID && s.MAS_COUNTRY_ID == PAY_TRANSACTIONMethod.MAS_COUNTRY_ID).FirstOrDefault();

                //if (Obj != null)
                //{
                //    amount = amount - (double)Obj.DISCOUNT;
                //}

                //if (amount != PAY_TRANSACTIONMethod.AMOUNT_FX)
                //{
                //    return BadRequest("amountfx is not correct");
                //}
                //obj.Amount = Math.Round(obj.Amount, 0);

                PAY_TRANSACTIONMethod.USER_ID = User.Identity.GetUserId();
                PAY_TRANSACTIONMethod.STATUS = "NEW";
                PAY_TRANSACTIONMethod.CREATED_ON = DateTime.Now;
                PAY_TRANSACTIONMethod.CREATED_BY = User.Identity.GetUserId();
                PAY_TRANSACTIONMethod.UPDATED_BY = User.Identity.GetUserId();
                PAY_TRANSACTIONMethod.UPDATED_ON = DateTime.Now;
                db.Entry(PAY_TRANSACTIONMethod).State = EntityState.Added;
                db.PAY_TRANSACTION.Add(PAY_TRANSACTIONMethod);
                db.SaveChanges();

                PAY_TRANSACTION PAY_TRANSACTIONobject = db.PAY_TRANSACTION.Include(s => s.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.ID == PAY_TRANSACTIONMethod.ID).FirstOrDefault();

                string TransactionID = PAY_TRANSACTIONobject.ID.ToString();
                int len = TransactionID.Length;

                for (int i = len; i < 7; i++)
                {
                    TransactionID = "0" + TransactionID;
                }

                PAY_TRANSACTIONobject.PREFIX = PAY_TRANSACTIONobject.CMN_INSTITUTE.INSTITUTE_PREFIX + "-" + TransactionID;
                PAY_TRANSACTIONobject.COUNTER_NUMBER = (10000 + PAY_TRANSACTIONobject.ID) + "";
                db.Entry(PAY_TRANSACTIONobject).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(PAY_TRANSACTIONobject);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("updateExpiryDate")]
        public IHttpActionResult updateExpiryDate([FromUri] DateTime? expirydate, [FromUri] int transactionid)
        {
            PAY_TRANSACTION transactionObject = db.PAY_TRANSACTION.Where(s => s.ID == transactionid).FirstOrDefault();

            if (transactionObject == null)
            {
                return NotFound();
            }

            if (expirydate != null)
            {
                transactionObject.EXPIRY_DATE = expirydate;
                db.Entry(transactionObject).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(transactionObject);
            }

            return BadRequest("Expiry Date not correct");
        }

        public class BodyModel
        {
            public int id { get; set; }
            public string status { get; set; }
        }

        [Route("update")]
        [Authorize]
        [HttpPost]
        public IHttpActionResult UpdatePAY_TRANSACTION(BodyModel body)
        {
            try
            {
                if (body.id == 0)
                {
                    return BadRequest();
                }

                string Responce = "Success!";

                int ID = body.id;
                PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(j => j.MAS_STUDENT)).Where(s => s.ID == ID).FirstOrDefault();

                if (tranObj.PAY_TRANSACTION_DETAIL.Select(s => s.MAS_STUDENT.STATUS).FirstOrDefault() == "SUBMITTED")
                {
                    if (body.status == "POST")
                    {
                        tranObj.POST_DATE = DateTime.Now.Date;
                    }

                    if (body.status == "RECEIVED")
                    {
                        tranObj.RECEIVED_DATE = DateTime.Now.Date;
                    }

                    tranObj.SUB_STATUS = "";
                    tranObj.STATUS = body.status;
                    tranObj.UPDATED_ON = DateTime.Now;
                    tranObj.UPDATED_BY = User.Identity.GetUserId();
                    db.Entry(tranObj).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    return Content(HttpStatusCode.NotFound, FuncMessage("Some Students were in PENDING status. Transactions could not be moved. Kindly mark status SUBMITTED TO FX OFFICE."));
                }

                return Content(HttpStatusCode.OK, FuncMessage(Responce));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("updateMonerisTransaction")]
        [Authorize]
        [HttpPost]
        public IHttpActionResult updateMonerisTransaction(BodyModel body)
        {   
            try
            {
                if (body.id == 0)
                {
                    return BadRequest();
                }

                int ID = body.id;
                PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(j => j.MAS_STUDENT)).Where(s => s.ID == ID).FirstOrDefault();

                tranObj.STATUS = body.status;
                tranObj.PAYMENT_SENT_DATE = DateTime.Now;
                tranObj.RECEIVED_DATE = DateTime.Now;
                tranObj.UPDATED_ON = DateTime.Now;
                tranObj.UPDATED_BY = User.Identity.GetUserId();
                db.Entry(tranObj).State = EntityState.Modified;
                db.SaveChanges();

                return Content(HttpStatusCode.OK, tranObj);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("updateOwner")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult UpdateOwnerPAY_TRANSACTION([FromUri] int transactionID, [FromUri] int ownerID)
        {
            try
            {
                PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.ID == transactionID).FirstOrDefault();

                if (tranObj == null)
                {
                    return BadRequest(ModelState);
                }

                tranObj.MAS_OWNER_ID = ownerID;
                tranObj.UPDATED_BY = User.Identity.GetUserId();
                tranObj.UPDATED_ON = DateTime.Now;
                db.Entry(tranObj).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(tranObj);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("getHash")]
        [HttpGet]
        public IHttpActionResult getHash([FromUri] string text)
        {
            byte[] hashValue;
            byte[] message = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            string a = hex.Trim();

            return Ok(a);
        }

        public class formingMessage
        {
            public string message {get;set;}
        }

        public formingMessage FuncMessage(string state)
        {
            return new formingMessage
            {
                message = state
            };
        }

        public forming formingFunc(string state)
        {
            return new forming
            {
                status = state
            };
        }

        public class forming
        {
            public string status { get; set; }
        }

        [Route("updateStatus")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult UpdateStatusPAY_TRANSACTION([FromUri] string status, [FromUri] string transactionID)
        {
            try
            {
                string done = "";
                string notDone = "";

                transactionID = transactionID.Substring(1, transactionID.Length - 2);

                int[] transactionidarray = transactionID.Split(',').Select(str => int.Parse(str)).ToArray();

                if (transactionidarray.Count() == 0)
                {
                    return BadRequest();
                }

                //string Responce = "Success!";

                for (int i = 0; i < transactionidarray.Count(); i++)
                {
                    int ID = transactionidarray[i];
                    PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(j => j.MAS_STUDENT)).Where(s => s.ID == ID).FirstOrDefault();

                    if (tranObj.SUB_STATUS != "CANCELLED")
                    {
                        if (tranObj.PAY_TRANSACTION_DETAIL.Select(s => s.MAS_STUDENT.STATUS).FirstOrDefault() == "SUBMITTED")
                        {
                            if (done == "")
                            {
                                done = "DONE : ";
                            }

                            done = done + " " + tranObj.PREFIX;

                            if (status == "POST")
                            {
                                tranObj.POST_DATE = DateTime.Now.Date;
                            }

                            if (status == "RECEIVED")
                            {
                                tranObj.RECEIVED_DATE = DateTime.Now.Date;
                            }

                            tranObj.SUB_STATUS = "";
                            tranObj.STATUS = status;
                            tranObj.UPDATED_ON = DateTime.Now;
                            tranObj.UPDATED_BY = User.Identity.GetUserId();
                            db.Entry(tranObj).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            if (notDone == "")
                            {
                                notDone = "NOT DONE : ";
                            }

                            notDone = notDone + " " + tranObj.PREFIX;
                            //return Content(HttpStatusCode.NotFound, FuncMessage("Some Students were in PENDING status. Transactions could not be moved. Kindly mark status SUBMITTED TO FX OFFICE."));
                        }
                    }
                }

                if (notDone != "")
                {
                    return Content(HttpStatusCode.NotFound, FuncMessage(done + " " + notDone));
                }

                return Ok(formingFunc(done + " " + notDone));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("SubStatus")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult SubStatusOfTransaction([FromUri] string status, [FromUri] string transactionID)
        {
            try
            {
                transactionID = transactionID.Substring(1, transactionID.Length - 2);

                int[] transactionidarray = transactionID.Split(',').Select(str => int.Parse(str)).ToArray();

                if (transactionidarray.Count() == 0)
                {
                    return BadRequest();
                }

                string Responce = "Success!";

                for (int i = 0; i < transactionidarray.Count(); i++)
                {
                    int ID = transactionidarray[i];
                    PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(j => j.MAS_STUDENT)).Where(s => s.ID == ID).FirstOrDefault();

                    if (tranObj.SUB_STATUS != "CANCELLED")
                    {
                        tranObj.SUB_STATUS = status;
                        tranObj.UPDATED_ON = DateTime.Now;
                        tranObj.UPDATED_BY = User.Identity.GetUserId();
                        db.Entry(tranObj).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return Ok(formingFunc(Responce));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("updateSubStatus")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult UpdateStatusPAY_TRANSACTION([FromUri] string status, [FromUri] int transactionID)
        {
            try
            {
                if (transactionID == 0)
                {
                    return Content(HttpStatusCode.NotFound, "transactionID is not valid.");
                }

                PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Where(s => s.ID == transactionID).FirstOrDefault();

                if (tranObj == null)
                {
                    return Content(HttpStatusCode.NotFound, "transactionID is not valid.");
                }

                tranObj.SUB_STATUS = status;
                tranObj.UPDATED_ON = DateTime.Now;
                tranObj.UPDATED_BY = User.Identity.GetUserId();
                db.Entry(tranObj).State = EntityState.Modified;
                db.SaveChanges();

                return Content(HttpStatusCode.OK, "Success");
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [Route("geoswift")]
        [HttpGet]
        public IHttpActionResult geoswift([FromUri] int transactionid, [FromUri] string status)
        {
            try
            {
                if (transactionid == 0)
                {
                    return NotFound();
                }

                if (status.ToLower() == "success")
                {
                    PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL).Where(s => s.ID == transactionid).FirstOrDefault();
                    tranObj.STATUS = "RECEIVED";
                    tranObj.PAYMENT_SENT_DATE = DateTime.Now;
                    tranObj.SUB_STATUS = "";
                    tranObj.UPDATED_ON = DateTime.Now;
                    tranObj.UPDATED_BY = User.Identity.GetUserId();
                    db.Entry(tranObj).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Content(HttpStatusCode.OK, "Success");
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Route("getGeoswiftTransactions")]
        [HttpGet]
        public IHttpActionResult geoswift([FromUri] DateTime FromDate, [FromUri] DateTime ToDate)
        {
            try
            {
                IQueryable<PAY_TRANSACTION> tranObj = db.PAY_TRANSACTION.Include(s=>s.CMN_CURRENCY).Include(s=>s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_PAYMENT_METHOD1).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(p => p.MAS_COUNTRY)).Include(s => s.PAY_TRANSACTION_DETAIL).Where(s => (s.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("Tenpay") || s.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("Alipay") || s.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("Domestic Transfer") || s.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("UnionPay")) && s.STATUS == "NEW" && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate));
                return Content(HttpStatusCode.OK, tranObj.ToList());
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("updateTransactionDetail")]
        public IHttpActionResult updateTransactionDetail([FromUri] string fieldname, [FromUri] int transactionid, [FromUri] string value)
        {
            try
            {
                PAY_TRANSACTION transactionObject = db.PAY_TRANSACTION.Where(s => s.ID == transactionid).FirstOrDefault();

                if (transactionObject == null)
                {
                    return NotFound();
                }


                int intValue = 0;

                try
                {
                    intValue = Int32.Parse(value);
                    typeof(PAY_TRANSACTION).GetProperty(fieldname).SetValue(transactionObject, intValue);
                    db.Entry(transactionObject).State = EntityState.Modified;
                    db.SaveChanges();

                    return Ok(transactionObject);
                }
                catch
                {
                    double dd = double.Parse(value);
                    typeof(PAY_TRANSACTION).GetProperty(fieldname).SetValue(transactionObject, dd);
                    db.Entry(transactionObject).State = EntityState.Modified;
                    db.SaveChanges();

                    return Ok(transactionObject);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //[Authorize(Roles = "Admin")]
        //public IHttpActionResult PutPAY_TRANSACTION(int id, PAY_TRANSACTION PAY_TRANSACTIONMethod)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != PAY_TRANSACTIONMethod.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(PAY_TRANSACTIONMethod).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CMN_COUNTRYExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // DELETE api/Default1/5
        //[Authorize(Roles = "Admin")]
        //[ResponseType(typeof(PAY_TRANSACTION))]
        //public IHttpActionResult DeletePAY_TRANSACTION(int id)
        //{
        //    PAY_TRANSACTION PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Find(id);
        //    if (PAY_TRANSACTIONMethod == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PAY_TRANSACTION.Remove(PAY_TRANSACTIONMethod);
        //    db.SaveChanges();

        //    return Ok(PAY_TRANSACTIONMethod);
        //}

        [HttpGet]
        [Route("GetDocumentLink")]
        public IHttpActionResult GetDocumentLink([FromUri] int transactionID)
        {
            try
            {
                PAY_TRANSACTION PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Include(s => s.CMN_PAYMENT_METHOD1).Include(t => t.MAS_COUNTRY.CMN_CURRENCY).Include(t => t.CMN_INSTITUTE.CMN_CURRENCY).Include(t => t.CMN_CURRENCY).Where(s => s.ID == transactionID).FirstOrDefault();

                if (PAY_TRANSACTIONMethod == null)
                {
                    return BadRequest("Transaction Not Found");
                }

                TransactionDetailContext context = new TransactionDetailContext();

                PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILObj = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_COUNTRY).Include(s => s.MAS_STUDENT).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).OrderByDescending(s => s.CREATED_ON).Where(s => s.PAY_TRANSACTION_ID == transactionID).FirstOrDefault();

                if (PAY_TRANSACTION_DETAILObj == null)
                {
                    return BadRequest("Transaction Detail Not Found");
                }

                if (PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("Bank Transfer - Send Wire"))
                {
                    AspNetIdentityContext bankContext = new AspNetIdentityContext();
                    CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj = bankContext.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_INSTITUTE_ID == PAY_TRANSACTIONMethod.CMN_INSTITUTE_ID && s.MAS_COUNTRY_ID == PAY_TRANSACTIONMethod.MAS_COUNTRY_ID && s.CMN_PAYMENT_METHOD_ID == PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD).FirstOrDefault();

                    if (CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj == null)
                    {
                        return BadRequest("Bank Details Not Found");
                    }

                    PAY_BANK_DETAILS bankDetails = bankContext.PAY_BANK_DETAILS.Where(s => s.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS_ID == CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj.ID && s.MAS_INSTITUTE_ID == PAY_TRANSACTIONMethod.CMN_INSTITUTE_ID).FirstOrDefault();

                    if (bankDetails == null)
                    {
                        return BadRequest("Bank Details Not Found");
                    }

                    string fileNameExisting = System.Web.HttpContext.Current.Server.MapPath("~/Resources/Paymentdocument.pdf");
                    string fileNameNew = System.Web.HttpContext.Current.Server.MapPath("~/Resources/PaymentdocumentEditted.pdf");

                    using (var existingFileStream = new FileStream(fileNameExisting, FileMode.Open))
                    using (var newFileStream = new FileStream(fileNameNew, FileMode.Create))
                    {
                        // Open existing PDF
                        var pdfReader = new PdfReader(existingFileStream);

                        // PdfStamper, which will create
                        var stamper = new PdfStamper(pdfReader, newFileStream);
                        stamper.SetFullCompression();
                        var form = stamper.AcroFields;
                        var fieldKeys = form.Fields.Keys;

                        //foreach (string fieldKey in fieldKeys)
                        //{
                        //    form.SetField(fieldKey, "REPLACED!");
                        //}

                        form.SetField("FullName", PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME + " " + PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME);
                        form.SetField("Address", PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + "" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2);
                        form.SetField("Phone", PAY_TRANSACTION_DETAILObj.PAYEE_PHONE_NUMBER);
                        string AMOUNT_PAYING = String.Format("{0:n}", PAY_TRANSACTIONMethod.AMOUNT_PAYING);
                        form.SetField("PaymentAmount", AMOUNT_PAYING + " " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL);
                        form.SetField("PaymentID", PAY_TRANSACTIONMethod.PREFIX);
                        string AMOUNT_FX = String.Format("{0:n}", PAY_TRANSACTIONMethod.AMOUNT_FX);
                        form.SetField("SettlementAmount", AMOUNT_FX + " " + PAY_TRANSACTIONMethod.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL);
                        form.SetField("Term", "");
                        DateTime dt = PAY_TRANSACTION_DETAILObj.CREATED_ON ?? DateTime.Now;
                        string now = dt.ToString("dd/MM/yyyy");
                        form.SetField("PaymentInstructionsOne", "You've booked a payment of " + AMOUNT_FX + " " + PAY_TRANSACTIONMethod.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL + " to " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + " on " + now + ". Your institution will receive " + AMOUNT_PAYING + " " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL + ".");
                        form.SetField("PaymentInstructionsTwo", "" + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + " has partnered with peerTransfer Corporation or one of its affiliates to process international payments. As an agent of " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + ", peerTransfer remits funds to their accounts located in United States. Additionally, this service allows " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + " to maintain the privacy of banking information to reduce susceptibility to fraudulent activity.");
                        form.SetField("RemittanceInformationReference", bankDetails.BANK_REFRENCE);
                        form.SetField("AmountAndCurrencyToSend", AMOUNT_FX + " " + PAY_TRANSACTIONMethod.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL);
                        form.SetField("Beneficiary", bankDetails.BENEFICIARY);
                        form.SetField("BeneficiaryAddress", bankDetails.BENEFICIARY_ADDRESS);
                        form.SetField("BeneficiaryAccountNumber", bankDetails.BENEFICIARY_ACCOUNT_NUMBER);
                        form.SetField("ABARoutingNumber", bankDetails.ABA_ROUTING_NUMBER);
                        form.SetField("BeneficiaryBankSWIFTBICCode", bankDetails.BANK_BENEFICIARY_CODE);
                        form.SetField("BeneficiaryBank", bankDetails.BANK_BENEFICIARY);
                        form.SetField("BeneficiaryBankAddress", bankDetails.BANK_BENEFICIARY_ADDRESS);
                        form.SetField("DetailOfCharges", bankDetails.DETAIL_OF_CHARGES);
                        form.SetField("Instructions", bankDetails.INSTRUCTIONS_TO_SENDER);
                        form.SetField("Notes", bankDetails.NOTES);

                        dt = PAY_TRANSACTIONMethod.EXPIRY_DATE ?? DateTime.Now;
                        now = dt.ToString("dd/MM/yyyy");

                        form.SetField("ExpiryDate", now);

                        // "Flatten" the form so it wont be editable/usable anymore
                        stamper.FormFlattening = true;

                        stamper.Close();
                        pdfReader.Close();
                    }

                    string filePath = HostingEnvironment.MapPath("~/Resources/PaymentdocumentEditted.pdf");
                    string existingBucketName = ConfigurationManager.AppSettings["AWSBucket"].ToString() + "/Documents";
                    IAmazonS3 client;
                    client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1);

                    String keyName = Guid.NewGuid().ToString();

                    //DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                    //{
                    //    BucketName = existingBucketName,
                    //    Key = keyName
                    //};

                    //DeleteObjectResponse response2 = client.DeleteObject(deleteObjectRequest);

                    TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1));
                    fileTransferUtility.Upload(filePath, existingBucketName, keyName);

                    return Ok("https://s3.amazonaws.com/" + ConfigurationManager.AppSettings["AWSBucket"].ToString() + "/Documents/" + keyName);
                }
                else if (PAY_TRANSACTIONMethod.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("Online"))
                {
                    string fileNameExisting = System.Web.HttpContext.Current.Server.MapPath("~/Resources/onlinePayment.pdf");
                    string fileNameNew = System.Web.HttpContext.Current.Server.MapPath("~/Resources/onlinePaymentEditted.pdf");

                    using (var existingFileStream = new FileStream(fileNameExisting, FileMode.Open))
                    using (var newFileStream = new FileStream(fileNameNew, FileMode.Create))
                    {
                        // Open existing PDF
                        var pdfReader = new PdfReader(existingFileStream);

                        // PdfStamper, which will create
                        var stamper = new PdfStamper(pdfReader, newFileStream);
                        stamper.SetFullCompression();
                        var form = stamper.AcroFields;
                        var fieldKeys = form.Fields.Keys;

                        //foreach (string fieldKey in fieldKeys)
                        //{
                        //    form.SetField(fieldKey, "REPLACED!");
                        //}

                        form.SetField("FullName", PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME + " " + PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME);
                        form.SetField("Address", PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + "" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2);
                        form.SetField("Phone", PAY_TRANSACTION_DETAILObj.PAYEE_PHONE_NUMBER);
                        string AMOUNT_PAYING = String.Format("{0:n}", PAY_TRANSACTIONMethod.AMOUNT_PAYING);
                        form.SetField("PaymentAmount", AMOUNT_PAYING + " " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL);
                        form.SetField("PaymentID", PAY_TRANSACTIONMethod.PREFIX);
                        string AMOUNT_FX = String.Format("{0:n}", PAY_TRANSACTIONMethod.AMOUNT_FX);
                        form.SetField("SettlementAmount", AMOUNT_FX + " " + PAY_TRANSACTIONMethod.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL);
                        form.SetField("Term", "");
                        DateTime dt = PAY_TRANSACTION_DETAILObj.CREATED_ON ?? DateTime.Now;
                        string now = dt.ToString("dd/MM/yyyy");
                        form.SetField("PaymentInstructionsOne", "You've booked a payment of " + AMOUNT_FX + " " + PAY_TRANSACTIONMethod.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL + " to " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + " on " + now + ". Your institution will receive " + AMOUNT_PAYING + " " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL + ".");
                        form.SetField("PaymentInstructionsTwo", "" + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + " has partnered with peerTransfer Corporation or one of its affiliates to process international payments. As an agent of " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + ", peerTransfer remits funds to their accounts located in United States. Additionally, this service allows " + PAY_TRANSACTIONMethod.CMN_INSTITUTE.INSTITUTE_NAME + " to maintain the privacy of banking information to reduce susceptibility to fraudulent activity.");
                        form.SetField("AmountandCurrencytosend", AMOUNT_FX + " " + PAY_TRANSACTIONMethod.MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL);

                        dt = PAY_TRANSACTIONMethod.EXPIRY_DATE ?? DateTime.Now;
                        now = dt.ToString("dd/MM/yyyy");

                        form.SetField("ExpiryDate", now);

                        // "Flatten" the form so it wont be editable/usable anymore
                        stamper.FormFlattening = true;

                        stamper.Close();
                        pdfReader.Close();
                    }

                    string filePath = HostingEnvironment.MapPath("~/Resources/onlinePaymentEditted.pdf");
                    string existingBucketName = ConfigurationManager.AppSettings["AWSBucket"].ToString() + "/Documents";
                    IAmazonS3 client;
                    client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1);

                    String keyName = Guid.NewGuid().ToString();

                    //DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                    //{
                    //    BucketName = existingBucketName,
                    //    Key = keyName
                    //};

                    //DeleteObjectResponse response2 = client.DeleteObject(deleteObjectRequest);

                    TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString(), Amazon.RegionEndpoint.USEast1));
                    fileTransferUtility.Upload(filePath, existingBucketName, keyName);

                    return Ok("https://s3.amazonaws.com/" + ConfigurationManager.AppSettings["AWSBucket"].ToString() + "/Documents/" + keyName);
                }

                return Content(HttpStatusCode.NotFound, "Invalid Payment Method");
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class TranStatus
        {
            public int Count { get; set; }
            public double Amount { get; set; }
            public double Percentage { get; set; }
        }

        public class TranSummary
        {
            public string status { get; set; }
            public TranStatus OBJECT { get; set; }
        }

        [Authorize]
        [HttpGet]
        [Route("getPaymentSummary")]
        public IHttpActionResult getSummaryTransaction([FromUri] DateTime FromDate, [FromUri] DateTime ToDate)
        {
            try
            {
                if (ToDate != null)
                {
                    ToDate = ToDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                var userID = User.Identity.GetUserId();

                ApplicationDbContext context = new ApplicationDbContext();
                ApplicationUser user = context.Users.Where(s => s.Id == userID).FirstOrDefault();
                int instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return BadRequest("instituteID is not valid");
                }

                //List<TranSummary> TranSummary = new List<TransactionController.TranSummary>();
                var TranSummary = new Dictionary<string, TranStatus>();
                double Total = 0.0;

                //FOR UNFUNDED
                TranSummary UNFUNDEDTranSummaryObj = new TranSummary();
                TranStatus UNFUNDEDTranStatus = new TranStatus();
                double UNFUNDEDAMOUNT = 0.00;
                int UNFUNDEDCOUNT = 0;
                double UNFUNDEDPERCENTAGE = 0.00;
                List<PAY_TRANSACTION> PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(a => a.MAS_STUDENT)).Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == "NEW" && s.SUB_STATUS != "CANCELLED" && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).ToList();

                if (PAY_TRANSACTIONMethod == null)
                {
                    UNFUNDEDTranStatus.Count = UNFUNDEDCOUNT;
                    UNFUNDEDTranStatus.Amount = UNFUNDEDAMOUNT;
                    UNFUNDEDTranStatus.Percentage = UNFUNDEDPERCENTAGE;
                    UNFUNDEDTranSummaryObj.status = "NEW";
                    UNFUNDEDTranSummaryObj.OBJECT = UNFUNDEDTranStatus;
                }
                else
                {
                    for (int i = 0; i < PAY_TRANSACTIONMethod.Count; i++)
                    {
                        if (PAY_TRANSACTIONMethod[i].PAY_TRANSACTION_DETAIL.Select(s => s.MAS_STUDENT.STATUS).FirstOrDefault() == "SUBMITTED")
                        {
                            UNFUNDEDCOUNT = UNFUNDEDCOUNT + 1;
                            UNFUNDEDAMOUNT = UNFUNDEDAMOUNT + (double)PAY_TRANSACTIONMethod[i].AMOUNT_PAYING;
                        }
                    }

                    Total = Total + UNFUNDEDCOUNT;
                    UNFUNDEDTranStatus.Count = UNFUNDEDCOUNT;
                    UNFUNDEDTranStatus.Amount = UNFUNDEDAMOUNT;
                    UNFUNDEDTranStatus.Percentage = UNFUNDEDPERCENTAGE;
                    UNFUNDEDTranSummaryObj.status = "NEW";
                    UNFUNDEDTranSummaryObj.OBJECT = UNFUNDEDTranStatus;
                }

                TranSummary.Add("NEW", UNFUNDEDTranStatus);

                //FOR INTRANST
                TranSummary INTRANSTTranSummaryObj = new TranSummary();
                TranStatus INTRANSTTranStatus = new TranStatus();
                double INTRANSTAMOUNT = 0.00;
                int INTRANSTCOUNT = 0;
                double INTRANSTPERCENTAGE = 0.00;
                PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(a => a.MAS_STUDENT)).Where(s => s.CMN_INSTITUTE_ID == instituteID && s.STATUS == "RECEIVED" && s.SUB_STATUS != "CANCELLED" && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).ToList();

                if (PAY_TRANSACTIONMethod == null)
                {
                    INTRANSTTranStatus.Count = INTRANSTCOUNT;
                    INTRANSTTranStatus.Amount = INTRANSTAMOUNT;
                    INTRANSTTranStatus.Percentage = INTRANSTPERCENTAGE;
                    INTRANSTTranSummaryObj.status = "INTRANSIT";
                    INTRANSTTranSummaryObj.OBJECT = INTRANSTTranStatus;
                }
                else
                {
                    for (int i = 0; i < PAY_TRANSACTIONMethod.Count; i++)
                    {
                        if (PAY_TRANSACTIONMethod[i].PAY_TRANSACTION_DETAIL.Select(s => s.MAS_STUDENT.STATUS).FirstOrDefault() == "SUBMITTED")
                        {
                            INTRANSTCOUNT = INTRANSTCOUNT + 1;
                            INTRANSTAMOUNT = INTRANSTAMOUNT + (double)PAY_TRANSACTIONMethod[i].AMOUNT_PAYING;
                        }
                    }

                    Total = Total + INTRANSTCOUNT;
                    INTRANSTTranStatus.Count = INTRANSTCOUNT;
                    INTRANSTTranStatus.Amount = INTRANSTAMOUNT;
                    INTRANSTTranStatus.Percentage = INTRANSTPERCENTAGE;
                    INTRANSTTranSummaryObj.status = "INTRANSIT";
                    INTRANSTTranSummaryObj.OBJECT = INTRANSTTranStatus;
                }

                TranSummary.Add("INTRANSIT", INTRANSTTranStatus);

                //FOR FUNDED
                TranSummary FUNDEDTranSummaryObj = new TranSummary();
                TranStatus FUNDEDTranStatus = new TranStatus();
                double FUNDEDAMOUNT = 0.00;
                int FUNDEDCOUNT = 0;
                double FUNDEDPERCENTAGE = 0.00;
                PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Include(s => s.PAY_TRANSACTION_DETAIL.Select(a => a.MAS_STUDENT)).Where(s => s.CMN_INSTITUTE_ID == instituteID && (s.STATUS == "POST" || s.STATUS == "ADMIN_UNPOST") && s.SUB_STATUS != "CANCELLED" && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).ToList();

                if (PAY_TRANSACTIONMethod == null)
                {
                    FUNDEDTranStatus.Count = FUNDEDCOUNT;
                    FUNDEDTranStatus.Amount = FUNDEDAMOUNT;
                    FUNDEDTranStatus.Percentage = FUNDEDPERCENTAGE;
                    FUNDEDTranSummaryObj.status = "FUNDED";
                    FUNDEDTranSummaryObj.OBJECT = FUNDEDTranStatus;
                }
                else
                {
                    for (int i = 0; i < PAY_TRANSACTIONMethod.Count; i++)
                    {
                        FUNDEDCOUNT = FUNDEDCOUNT + 1;
                        FUNDEDAMOUNT = FUNDEDAMOUNT + (double)PAY_TRANSACTIONMethod[i].AMOUNT_PAYING;
                    }

                    Total = Total + FUNDEDCOUNT;
                    FUNDEDTranStatus.Count = FUNDEDCOUNT;
                    FUNDEDTranStatus.Amount = FUNDEDAMOUNT;
                    FUNDEDTranStatus.Percentage = FUNDEDPERCENTAGE;
                    FUNDEDTranSummaryObj.status = "FUNDED";
                    FUNDEDTranSummaryObj.OBJECT = FUNDEDTranStatus;
                }

                TranSummary.Add("FUNDED", FUNDEDTranStatus);

                //FOR RECEIVED
                TranSummary RECEIVEDTranSummaryObj = new TranSummary();
                TranStatus RECEIVEDTranStatus = new TranStatus();
                double RECEIVEDAMOUNT = 0.00;
                int RECEIVEDCOUNT = 0;
                double RECEIVEDPERCENTAGE = 0.00;
                PAY_TRANSACTIONMethod = db.PAY_TRANSACTION.Include(s => s.PAY_BATCH_TRANSACTION.Select(q => q.PAY_BATCH)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(a => a.MAS_STUDENT)).Where(s => s.CMN_INSTITUTE_ID == instituteID && (s.STATUS == "ADMIN_UNPOST" || s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST") && s.SUB_STATUS != "CANCELLED" && (s.PAY_BATCH_TRANSACTION.Select(q => q.PAY_BATCH.SETTLEMENT_DATE).FirstOrDefault() > FromDate && s.PAY_BATCH_TRANSACTION.Select(q => q.PAY_BATCH.SETTLEMENT_DATE).FirstOrDefault() < ToDate)).ToList();

                if (PAY_TRANSACTIONMethod == null)
                {
                    RECEIVEDTranStatus.Count = RECEIVEDCOUNT;
                    RECEIVEDTranStatus.Amount = RECEIVEDAMOUNT;
                    RECEIVEDTranStatus.Percentage = RECEIVEDPERCENTAGE;
                    RECEIVEDTranSummaryObj.status = "RECEIVED";
                    RECEIVEDTranSummaryObj.OBJECT = RECEIVEDTranStatus;
                }
                else
                {
                    for (int i = 0; i < PAY_TRANSACTIONMethod.Count; i++)
                    {
                        RECEIVEDCOUNT = RECEIVEDCOUNT + 1;
                        RECEIVEDAMOUNT = RECEIVEDAMOUNT + (double)PAY_TRANSACTIONMethod[i].AMOUNT_PAYING;
                    }

                    Total = Total + RECEIVEDCOUNT;
                    RECEIVEDTranStatus.Count = RECEIVEDCOUNT;
                    RECEIVEDTranStatus.Amount = RECEIVEDAMOUNT;
                    RECEIVEDTranStatus.Percentage = RECEIVEDPERCENTAGE;
                    RECEIVEDTranSummaryObj.status = "RECEIVED";
                    RECEIVEDTranSummaryObj.OBJECT = RECEIVEDTranStatus;
                }

                TranSummary.Add("RECEIVED", RECEIVEDTranStatus);

                //double percentage = (TranSummary["NEW"].Count / Total) * 100;
                //TranSummary["NEW"].Percentage = percentage;

                //percentage = (TranSummary["INTRANSIT"].Count / Total) * 100;
                //TranSummary["INTRANSIT"].Percentage = percentage;

                //percentage = (TranSummary["FUNDED"].Count / Total) * 100;
                //TranSummary["FUNDED"].Percentage = percentage;

                //percentage = (TranSummary["RECEIVED"].Count / Total) * 100;
                //TranSummary["RECEIVED"].Percentage = percentage;

                return Ok(TranSummary);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("getRecentTransactions")]
        public IHttpActionResult getCountrySummary([FromUri] string status, [FromUri] DateTime FromDate, [FromUri] DateTime ToDate)
        {
            try
            {
                if (ToDate != null)
                {
                    ToDate = ToDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                var userID = User.Identity.GetUserId();

                ApplicationDbContext context = new ApplicationDbContext();
                ApplicationUser user = context.Users.Where(s => s.Id == userID).FirstOrDefault();
                int instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return NotFound();
                }


                if (instituteID == 0)
                {
                    return Content(HttpStatusCode.NotFound, "Institute ID is invalid");
                }

                List<PAY_TRANSACTION> PAY_TRANSACTIONlist = null;

                if (status.ToLower() == "unfunded")
                {
                    PAY_TRANSACTIONlist = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == "NEW" && s.CMN_INSTITUTE_ID == instituteID && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).OrderByDescending(s => s.CREATED_ON).ToList();
                }

                if (status.ToLower() == "posted")
                {
                    PAY_TRANSACTIONlist = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.STATUS == "RECEIVED" && s.CMN_INSTITUTE_ID == instituteID && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).OrderByDescending(s => s.CREATED_ON).ToList();
                }

                if (status.ToLower() == "remittances")
                {
                    PAY_TRANSACTIONlist = db.PAY_TRANSACTION.Include(s => s.PAY_BATCH_TRANSACTION.Select(q => q.PAY_BATCH)).Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => (s.STATUS == "ADMIN_POST" || s.STATUS == "ADMIN_UNPOST" || s.STATUS == "UNIVERSITY_POST") && s.SUB_STATUS != "CANCELLED" && s.CMN_INSTITUTE_ID == instituteID && (s.PAY_BATCH_TRANSACTION.Select(q => q.PAY_BATCH.SETTLEMENT_DATE).FirstOrDefault() > FromDate && s.PAY_BATCH_TRANSACTION.Select(q => q.PAY_BATCH.SETTLEMENT_DATE).FirstOrDefault() < ToDate)).OrderByDescending(s => s.POST_DATE).Take(5).ToList();
                }

                if (PAY_TRANSACTIONlist == null)
                {
                    return NotFound();
                }

                List<PAY_TRANSACTION> transactionResponse = new List<PAY_TRANSACTION>();

                for (int i = 0; i < PAY_TRANSACTIONlist.Count(); i++)
                {
                    PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILobj = PAY_TRANSACTIONlist[i].PAY_TRANSACTION_DETAIL.FirstOrDefault();

                    if (PAY_TRANSACTION_DETAILobj != null)
                    {
                        if (PAY_TRANSACTION_DETAILobj.MAS_STUDENT.STATUS == "SUBMITTED")
                        {
                            transactionResponse.Add(PAY_TRANSACTIONlist[i]);
                        }
                    }
                }
                transactionResponse = transactionResponse.OrderByDescending(s => s.CREATED_ON).Take(5).ToList();
                //IQueryable<PAY_TRANSACTION_DETAIL> transactionObject = context.PAY_TRANSACTION_DETAIL.Include(s => s.MAS_COUNTRY).Include(s => s.MAS_STUDENT).Include(s => s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Include(s => s.PAY_TRANSACTION).Include(s => s.MAS_STUDENT);
                return Ok(transactionResponse);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("searchTransaction")]
        public IHttpActionResult searchTransaction([FromUri] string prefixid, [FromUri] string studentid)
        {
            try
            {
                string UserID = User.Identity.GetUserId();
                ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                int instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return BadRequest("No Institute ID found");
                }

                List<PAY_TRANSACTION> PAY_TRANSACTIONlist = null;

                PAY_TRANSACTIONlist = db.PAY_TRANSACTION.Include(s => s.MAS_OWNER).Include(s => s.PAY_BANK_DETAILS).Include(s => s.MAS_COUNTRY).Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_STUDENT)).Include(s => s.PAY_TRANSACTION_DETAIL.Select(d => d.MAS_PERSON_FILLING)).Include(s => s.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD1).Where(s => s.CMN_INSTITUTE_ID == instituteID && (s.PAY_TRANSACTION_DETAIL.Select(g => g.MAS_STUDENT.STUDENT_ID).FirstOrDefault().Contains(studentid) || s.PREFIX.Contains(prefixid))).OrderByDescending(s => s.CREATED_ON).ToList();

                List<PAY_TRANSACTION> transactionResponse = new List<PAY_TRANSACTION>();

                for (int i = 0; i < PAY_TRANSACTIONlist.Count(); i++)
                {
                    PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILobj = PAY_TRANSACTIONlist[i].PAY_TRANSACTION_DETAIL.FirstOrDefault();

                    if (PAY_TRANSACTION_DETAILobj != null)
                    {
                        if (PAY_TRANSACTION_DETAILobj.MAS_STUDENT.STATUS == "SUBMITTED")
                        {
                            transactionResponse.Add(PAY_TRANSACTIONlist[i]);
                        }
                    }
                }

                return Ok(transactionResponse);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("getCountrySummary")]
        public IHttpActionResult getCountrySummary([FromUri] int instituteID)
        {
            try
            {
                var data = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID).GroupBy(s => s.MAS_COUNTRY.COUNTRY_NAME).Select(g => new { g.Key, Count = (g.Count() * 100 / db.PAY_TRANSACTION.Count()) }).ToList();

                return Ok(data);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("getCurrencyGraph")]
        public IHttpActionResult getCurrencyGraph([FromUri] DateTime FromDate, [FromUri] DateTime ToDate)
        {
            try
            {
                if (ToDate != null)
                {
                    ToDate = ToDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                var userID = User.Identity.GetUserId();

                ApplicationDbContext context = new ApplicationDbContext();
                ApplicationUser user = context.Users.Where(s => s.Id == userID).FirstOrDefault();
                int instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return NotFound();
                }

                var data = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).GroupBy(s => s.CMN_CURRENCY.CURR_SYMBOL).Select(g => new { g.Key, Count = g.Count() }).OrderByDescending(s => s.Count).Take(10).ToList();

                return Ok(data);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("getPaymentMethodGraph")]
        public IHttpActionResult getPaymentMethodGraph([FromUri] DateTime FromDate, [FromUri] DateTime ToDate)
        {
            try
            {
                if (ToDate != null)
                {
                    ToDate = ToDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                // Date DateTimeFromDate 

                var userID = User.Identity.GetUserId();

                ApplicationDbContext context = new ApplicationDbContext();
                ApplicationUser user = context.Users.Where(s => s.Id == userID).FirstOrDefault();
                int instituteID = user.InstituteID;

                if (instituteID == 0)
                {
                    return NotFound();
                }
                List<graphFormat> graphFormatList = new List<graphFormat>();

                var transactions = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && !(s.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("bank transfer")) && !(s.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("visa")) && !(s.CMN_PAYMENT_METHOD1.METHOD_NAME.Contains("master")) && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).GroupBy(s => s.CMN_PAYMENT_METHOD1.METHOD_NAME).Select(g => new { g.Key, Count = (g.Count() * 100 / db.PAY_TRANSACTION.Where(s => s.CREATED_ON > FromDate && s.CREATED_ON < ToDate).Count()) }).ToList();

                for (int i = 0; i < transactions.Count; i++)
                {
                    if (transactions[i].Count != 0)
                    {
                        graphFormat obje = new graphFormat();
                        obje.key = transactions[i].Key;
                        obje.count = transactions[i].Count;
                        graphFormatList.Add(obje);
                    }
                }

                var banktransfer = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.CMN_PAYMENT_METHOD1.METHOD_NAME.ToLower().Contains("bank transfer") && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).GroupBy(s => s.CMN_PAYMENT_METHOD1.METHOD_NAME).Select(g => new { g.Key, Count = g.Count() }).ToList();
                int banktransferSum = banktransfer.Sum(s => s.Count);
                int total = db.PAY_TRANSACTION.Where(s => s.CREATED_ON > FromDate && s.CREATED_ON < ToDate).Count();
                if (total != 0)
                {
                    banktransferSum = (banktransferSum * 100) / total;
                }
                else
                {
                    banktransferSum = 0;
                }

                graphFormat obj = new graphFormat();

                if (banktransfer.Count != 0)
                {
                    obj.key = "Bank Transfer";
                    obj.count = banktransferSum;
                    graphFormatList.Add(obj);
                }


                var visa = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.CMN_PAYMENT_METHOD1.METHOD_NAME.ToLower().Contains("visa") && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).GroupBy(s => s.CMN_PAYMENT_METHOD1.METHOD_NAME).Select(g => new { g.Key, Count = g.Count() }).ToList();
                int visaSum = visa.Sum(s => s.Count);
                int total3 = db.PAY_TRANSACTION.Where(s => s.CREATED_ON > FromDate && s.CREATED_ON < ToDate).Count();
                if (total3 != 0)
                {
                    visaSum = (visaSum * 100) / total;
                }
                else
                {
                    visaSum = 0;
                }
                if (visa.Count != 0)
                {
                    obj = new graphFormat();
                    obj.key = "Visa Debit/Credit";
                    obj.count = visaSum;
                    graphFormatList.Add(obj);
                }


                var master = db.PAY_TRANSACTION.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.CMN_PAYMENT_METHOD1.METHOD_NAME.ToLower().Contains("master") && (s.CREATED_ON > FromDate && s.CREATED_ON < ToDate)).GroupBy(s => s.CMN_PAYMENT_METHOD1.METHOD_NAME).Select(g => new { g.Key, Count = g.Count() }).ToList();
                int masterSum = master.Sum(s => s.Count);
                int total2 = db.PAY_TRANSACTION.Where(s => s.CREATED_ON > FromDate && s.CREATED_ON < ToDate).Count();
                if (total2 != 0)
                {
                    masterSum = (masterSum * 100) / total;
                }
                else
                {
                    masterSum = 0;
                }
                if (master.Count != 0)
                {
                    obj = new graphFormat();
                    obj.key = "Master Debit/Credit";
                    obj.count = masterSum;
                    graphFormatList.Add(obj);
                }

                graphFormatList = graphFormatList.OrderByDescending(s => s.count).Take(10).ToList();
                return Ok(graphFormatList);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class graphFormat
        {
            public string key { get; set; }
            public int count { get; set; }
        }

        //REPORTING METHODS
        [Authorize]
        [HttpGet]
        [Route("studentPaymentLedger")]
        public IHttpActionResult studentPaymentLedger([FromUri] string StudentID, [FromUri] string FirstName, [FromUri] string LastName, [FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try
            {
                if (FromDate == null & ToDate == null && StudentID == null && FirstName == null && LastName ==null)
                {
                    List<studentReport> studentReportList = new List<studentReport>();
                    return Ok(studentReportList);
                }
                else
                {
                    if (ToDate != null)
                    {
                        ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    string UserID = User.Identity.GetUserId();
                    ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                    ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                    int instituteID = user.InstituteID;

                    TransactionDetailContext context = new TransactionDetailContext();

                    List<PAY_TRANSACTION_DETAIL> transactionObject = context.PAY_TRANSACTION_DETAIL.Include(s => s.PAY_TRANSACTION.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH)).Include(s => s.MAS_STUDENT).Where(s => s.PAY_TRANSACTION.CMN_INSTITUTE_ID == instituteID && (s.PAY_TRANSACTION.STATUS == "ADMIN_POST" || s.PAY_TRANSACTION.STATUS == "UNIVERSITY_POST")).OrderByDescending(s => s.CREATED_ON).ToList();

                    if (transactionObject == null)
                    {
                        return NotFound();
                    }

                    if (StudentID != null)
                    {
                        transactionObject = transactionObject.Where(s => s.MAS_STUDENT.STUDENT_ID.Contains(StudentID)).ToList();
                    }

                    if (FirstName != null)
                    {
                        transactionObject = transactionObject.Where(s => s.STUDENT_FIRST_NAME.ToLower().Contains(FirstName.ToLower())).ToList();
                    }

                    if (LastName != null)
                    {
                        transactionObject = transactionObject.Where(s => s.STUDENT_LAST_NAME.ToLower().Contains(LastName.ToLower())).ToList();
                    }

                    if (FromDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON > FromDate).ToList();
                    }

                    if (ToDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON < ToDate).ToList();
                    }

                    List<studentReport> studentReportList = new List<studentReport>();

                    for (int i = 0; i < transactionObject.Count(); i++)
                    {
                        studentReport studentReportObj = new studentReport();
                        DateTime valueDt = (DateTime)transactionObject[i].CREATED_ON;
                        string AMOUNT_PAYING = String.Format("{0:n}", transactionObject[i].PAY_TRANSACTION.AMOUNT_PAYING);

                        studentReportObj.value_date = valueDt.ToString("MM/dd/yyyy");
                        studentReportObj.payment_id = transactionObject[i].PAY_TRANSACTION.PREFIX;
                        studentReportObj.invoice_number = transactionObject[i].INVOICE_NUMBER;
                        studentReportObj.remittance_batch_no = transactionObject[i].PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(s => s.PAY_BATCH.ID).FirstOrDefault().ToString();
                        studentReportObj.currency = transactionObject[i].PAY_TRANSACTION.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;
                        studentReportObj.amount = transactionObject[i].PAY_TRANSACTION.AMOUNT_PAYING.ToString();
                        studentReportObj.first_name = transactionObject[i].STUDENT_FIRST_NAME.ToString();
                        studentReportObj.last_name = transactionObject[i].STUDENT_LAST_NAME.ToString();

                        studentReportList.Add(studentReportObj);
                    }

                    return Ok(studentReportList);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("transactionSettlementReport")]
        public IHttpActionResult transactionSettlementReport([FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try
            {
                if (FromDate == null && ToDate == null)
                {
                    List<settlementReport> studentReportList = new List<settlementReport>();
                    return Ok(studentReportList);
                }
                else
                {
                    if (ToDate != null)
                    {
                        ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    string UserID = User.Identity.GetUserId();
                    ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                    ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                    int instituteID = user.InstituteID;

                    TransactionDetailContext context = new TransactionDetailContext();

                    List<PAY_TRANSACTION_DETAIL> transactionObject = context.PAY_TRANSACTION_DETAIL.Include(s => s.PAY_TRANSACTION.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH)).Include(s => s.MAS_STUDENT).Where(s => s.PAY_TRANSACTION.CMN_INSTITUTE_ID == instituteID && (s.PAY_TRANSACTION.STATUS == "ADMIN_POST" || s.PAY_TRANSACTION.STATUS == "UNIVERSITY_POST")).OrderByDescending(s => s.CREATED_ON).ToList();

                    if (transactionObject == null)
                    {
                        return NotFound();
                    }

                    if (FromDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON > FromDate).ToList();
                    }

                    if (ToDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON < ToDate).ToList();
                    }

                    if (FromDate == null && ToDate == null)
                    {
                        transactionObject = transactionObject.Where(s => s.CREATED_ON.Value.Month == DateTime.Now.Month).ToList();
                    }

                    List<settlementReport> studentReportList = new List<settlementReport>();

                    for (int i = 0; i < transactionObject.Count(); i++)
                    {
                        settlementReport studentReportObj = new settlementReport();
                        DateTime valueDt = (DateTime)transactionObject[i].CREATED_ON;
                        //string AMOUNT_PAYING = String.Format("{0:n}",;

                        studentReportObj.value_date = valueDt.ToString("MM/dd/yyyy");
                        studentReportObj.payment_id = transactionObject[i].PAY_TRANSACTION.PREFIX;
                        studentReportObj.student_number = transactionObject[i].MAS_STUDENT.STUDENT_ID;
                        studentReportObj.invoice_number = transactionObject[i].INVOICE_NUMBER;
                        studentReportObj.first_name = transactionObject[i].STUDENT_FIRST_NAME;
                        studentReportObj.last_name = transactionObject[i].STUDENT_LAST_NAME;
                        studentReportObj.remittance_batch_no = transactionObject[i].PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(s => s.PAY_BATCH.ID).FirstOrDefault().ToString();
                        studentReportObj.currency = transactionObject[i].PAY_TRANSACTION.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;
                        studentReportObj.amount = transactionObject[i].PAY_TRANSACTION.AMOUNT_PAYING.ToString();

                        studentReportList.Add(studentReportObj);
                    }

                    return Ok(studentReportList);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("remittanceLedger")]
        public IHttpActionResult remittanceLedger([FromUri] string batchID, [FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try
            {
                if (batchID == null && FromDate == null && ToDate == null)
                {
                    List<remittanceLedgerReport> studentReportList = new List<remittanceLedgerReport>();
                    return Ok(studentReportList);
                }
                else
                {
                    if (ToDate != null)
                    {
                        ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    string UserID = User.Identity.GetUserId();
                    ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                    ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                    int instituteID = user.InstituteID;

                    TransactionDetailContext context = new TransactionDetailContext();

                    List<PAY_TRANSACTION_DETAIL> transactionObject = context.PAY_TRANSACTION_DETAIL.Include(s => s.PAY_TRANSACTION.CMN_INSTITUTE.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION.CMN_CURRENCY).Include(s => s.PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH)).Include(s => s.MAS_STUDENT).Where(s => s.PAY_TRANSACTION.CMN_INSTITUTE_ID == instituteID && (s.PAY_TRANSACTION.STATUS == "ADMIN_POST" || s.PAY_TRANSACTION.STATUS == "UNIVERSITY_POST")).OrderByDescending(s => s.CREATED_ON).ToList();

                    if (transactionObject == null)
                    {
                        return NotFound();
                    }

                    if (batchID != null)
                    {
                        int batcheyeDee = Convert.ToInt32(batchID);
                        transactionObject = transactionObject.Where(s => s.PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH.ID).FirstOrDefault() == batcheyeDee).ToList();
                    }

                    if (FromDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH.SETTLEMENT_DATE).FirstOrDefault() >= FromDate).ToList();
                    }

                    if (ToDate != null)
                    {
                        transactionObject = transactionObject.Where(s => s.PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH.SETTLEMENT_DATE).FirstOrDefault() <= ToDate).ToList();
                    }

                    if (batchID == null && FromDate == null && ToDate == null)
                    {
                        transactionObject = transactionObject.Where(s => s.PAY_TRANSACTION.PAY_BATCH_TRANSACTION.Select(j => j.PAY_BATCH.SETTLEMENT_DATE).FirstOrDefault().Value.Month == DateTime.Now.Month).ToList();
                    }

                    List<remittanceLedgerReport> studentReportList = new List<remittanceLedgerReport>();

                    for (int i = 0; i < transactionObject.Count(); i++)
                    {
                        remittanceLedgerReport studentReportObj = new remittanceLedgerReport();
                        DateTime valueDt = (DateTime)transactionObject[i].CREATED_ON;
                        string AMOUNT_PAYING = String.Format("{0:n}", transactionObject[i].PAY_TRANSACTION.AMOUNT_PAYING);

                        studentReportObj.payment_id = transactionObject[i].PAY_TRANSACTION.PREFIX;
                        studentReportObj.student_id = transactionObject[i].MAS_STUDENT.STUDENT_ID;
                        studentReportObj.invoice_number = transactionObject[i].INVOICE_NUMBER;
                        studentReportObj.first_name = transactionObject[i].STUDENT_FIRST_NAME;
                        studentReportObj.last_name = transactionObject[i].STUDENT_LAST_NAME;
                        studentReportObj.currency = transactionObject[i].PAY_TRANSACTION.CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;
                        studentReportObj.amount = transactionObject[i].PAY_TRANSACTION.AMOUNT_PAYING.ToString();

                        studentReportList.Add(studentReportObj);
                    }

                    return Ok(studentReportList);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("remittanceStatement")]
        public IHttpActionResult remittanceStatement([FromUri] DateTime? FromDate, [FromUri] DateTime? ToDate)
        {
            try
            {
                if (FromDate == null && ToDate == null)
                {
                    List<remittanceStatementReport> batchModel = new List<remittanceStatementReport>();
                    return Ok(batchModel);
                }
                else
                {
                    if (ToDate != null)
                    {
                        ToDate = ToDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    string UserID = User.Identity.GetUserId();
                    ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
                    ApplicationUser user = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();
                    int instituteID = user.InstituteID;

                    AspNetIdentityContext context = new AspNetIdentityContext();
                    List<PAY_BATCH> batchObject = null;

                    batchObject = context.PAY_BATCH.Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.CMN_INSTITUE_ID == instituteID && (s.STATUS == "ADMIN_POST" || s.STATUS == "UNIVERSITY_POST")).OrderByDescending(s => s.CREATED_ON).ToList();

                    if (batchObject == null)
                    {
                        return NotFound();
                    }

                    if (FromDate != null)
                    {
                        batchObject = batchObject.Where(s => s.CREATED_ON > FromDate).ToList();
                    }

                    if (ToDate != null)
                    {
                        batchObject = batchObject.Where(s => s.CREATED_ON < ToDate).ToList();
                    }

                    if (FromDate == null && ToDate == null)
                    {
                        batchObject = batchObject.Where(s => s.CREATED_ON.Value.Month == DateTime.Now.Month).ToList();
                    }

                    List<remittanceStatementReport> batchModel = new List<remittanceStatementReport>();
                    AspNetIdentity.Infrastructure.TransactionContext transactionObj = new AspNetIdentity.Infrastructure.TransactionContext();
                    AspNetIdentityContext contextObj = new AspNetIdentityContext();

                    for (int i = 0; i < batchObject.Count(); i++)
                    {
                        int batchID = batchObject[i].ID;
                        List<PAY_BATCH_TRANSACTION> batchTranObj = contextObj.PAY_BATCH_TRANSACTION.Where(s => s.PAY_BATCH_ID == batchID).ToList();

                        remittanceStatementReport batchObj = new remittanceStatementReport();
                        double totalAmount = 0;
                        int totalCount = 0;
                        DateTime valueDT = (DateTime)batchObject[i].CREATED_ON;

                        batchObj.batch_id = batchObject[i].ID.ToString();
                        batchObj.value_date = valueDT.ToString("MM/dd/yyyy");
                        batchObj.currency = batchObject[i].CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;

                        for (int j = 0; j < batchTranObj.Count(); j++)
                        {
                            int batchTranID = (int)batchTranObj[j].TRANSACTION_ID;
                            PAY_TRANSACTION payTran = transactionObj.PAY_TRANSACTION.Where(s => s.ID == batchTranID).FirstOrDefault();
                            totalAmount = totalAmount + Double.Parse(payTran.AMOUNT_PAYING.ToString());
                            totalCount = totalCount + 1;

                            if (j == 0)
                            {
                                CurrencyContext currContext = new CurrencyContext();

                                CMN_CURRENCY currObj = currContext.CMN_CURRENCY.Where(s => s.ID == payTran.CMN_CURRENCY_ID).FirstOrDefault();
                            }
                        }

                        batchObj.amount = totalAmount;
                        batchObj.number_of_payments = totalCount;

                        batchModel.Add(batchObj);
                    }

                    return Ok(batchModel);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public class studentReport
        {
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string value_date { get; set; }
            public string payment_id { get; set; }
            public string invoice_number { get; set; }
            public string remittance_batch_no { get; set; }
            public string currency { get; set; }
            public string amount { get; set; }
        }

        public class settlementReport
        {
            public string value_date { get; set; }
            public string payment_id { get; set; }
            public string student_number { get; set; }
            public string invoice_number { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string remittance_batch_no { get; set; }
            public string currency { get; set; }
            public string amount { get; set; }
        }

        public class remittanceLedgerReport
        {
            public string payment_id { get; set; }
            public string student_id { get; set; }
            public string invoice_number { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string currency { get; set; }
            public string amount { get; set; }
        }

        public class remittanceStatementReport
        {
            public string value_date { get; set; }
            public string batch_id { get; set; }
            public double number_of_payments { get; set; }
            public string currency { get; set; }
            public double amount { get; set; }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.PAY_TRANSACTION.Count(e => e.ID == id) > 0;
        }
    }
}