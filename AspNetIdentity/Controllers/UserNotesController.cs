﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using AspNetIdentity.Infrastructure;
using Microsoft.AspNet.Identity;

namespace AspNetIdentity.Controllers
{
    public class UserNotesController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        // GET: api/UserNotes
        [Authorize]
        [HttpGet]
        [Route("api/UserNotes/GetNotes")]
        public IHttpActionResult GetNotes([FromUri] string userid)
        {
            List<AspNetUser_NOTES> AspNetUser_NOTES = db.AspNetUser_NOTES.Where(s => s.USER_ID == userid).ToList();
            if (AspNetUser_NOTES == null)
            {
                return NotFound();
            }

            for (int i = 0; i < AspNetUser_NOTES.Count(); i++)
            {
                string UserID = AspNetUser_NOTES[i].CREATED_BY;

                ApplicationDbContext usercontext = new ApplicationDbContext();
                ApplicationUser user = usercontext.Users.Where(s => s.Id == UserID).FirstOrDefault();

                if (user == null)
                {
                    AspNetUser_NOTES[i].CREATED_BY = "UNKNOWN";
                }
                else
                {
                    AspNetUser_NOTES[i].CREATED_BY = user.FirstName + " " + user.LastName;
                }
            }

            return Ok(AspNetUser_NOTES);
        }

        [Authorize]
        [HttpPost]
        [Route("api/UserNotes/createNotes")]
        public IHttpActionResult createNotes([FromUri] string note, [FromUri] string userid)
        {
            if (note == null)
            {
                return NotFound();
            }

            string UserID = User.Identity.GetUserId();
            ApplicationDbContext usercontext = new ApplicationDbContext();
            ApplicationUser user = usercontext.Users.Where(s => s.Id == UserID).FirstOrDefault();

            AspNetUser_NOTES AspNetUser_NOTESObj = new AspNetUser_NOTES();
            AspNetUser_NOTESObj.NOTES = note;
            AspNetUser_NOTESObj.USER_ID = userid;
            AspNetUser_NOTESObj.CREATED_ON = DateTime.Now;
            AspNetUser_NOTESObj.CREATED_BY = UserID;

            db.AspNetUser_NOTES.Add(AspNetUser_NOTESObj);
            db.SaveChanges();

            return Ok(AspNetUser_NOTESObj);
        }

        [Authorize]
        [HttpDelete]
        [Route("api/UserNotes/deleteNotes")]
        public IHttpActionResult deleteNotes([FromUri] int noteid)
        {
            if (noteid == null)
            {
                return NotFound();
            }

            string UserID = User.Identity.GetUserId();

            AspNetUser_NOTES PAY_TRANSACTION_NOTESObj = db.AspNetUser_NOTES.Where(s => s.ID == noteid).FirstOrDefault();
            db.Entry(PAY_TRANSACTION_NOTESObj).State = EntityState.Deleted;
            db.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AspNetUser_NOTESExists(int id)
        {
            return db.AspNetUser_NOTES.Count(e => e.ID == id) > 0;
        }
    }
}