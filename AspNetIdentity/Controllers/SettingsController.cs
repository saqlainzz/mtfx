﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;

namespace AspNetIdentity.Controllers
{
    public class SettingsController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        [HttpGet]
        [Route("api/Settings/GetSetting")]
        public IHttpActionResult GetSetting([FromUri] string label)
        {
            var recrd = db.MAS_SETTINGS.Where(s=>s.LABEL == label).FirstOrDefault();
            return Ok(recrd);
        }

        // GET: api/Settings
        public IQueryable<MAS_SETTINGS> GetMAS_SETTINGS()
        {
            return db.MAS_SETTINGS;
        }

        // GET: api/Settings/5
        [ResponseType(typeof(MAS_SETTINGS))]
        public IHttpActionResult GetMAS_SETTINGS(int id)
        {
            MAS_SETTINGS mAS_SETTINGS = db.MAS_SETTINGS.Find(id);
            if (mAS_SETTINGS == null)
            {
                return NotFound();
            }

            return Ok(mAS_SETTINGS);
        }

        // PUT: api/Settings/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMAS_SETTINGS(int id, MAS_SETTINGS mAS_SETTINGS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mAS_SETTINGS.ID)
            {
                return BadRequest();
            }

            db.Entry(mAS_SETTINGS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MAS_SETTINGSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Settings
        [ResponseType(typeof(MAS_SETTINGS))]
        public IHttpActionResult PostMAS_SETTINGS(MAS_SETTINGS mAS_SETTINGS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAS_SETTINGS.Add(mAS_SETTINGS);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mAS_SETTINGS.ID }, mAS_SETTINGS);
        }

        // DELETE: api/Settings/5
        [ResponseType(typeof(MAS_SETTINGS))]
        public IHttpActionResult DeleteMAS_SETTINGS(int id)
        {
            MAS_SETTINGS mAS_SETTINGS = db.MAS_SETTINGS.Find(id);
            if (mAS_SETTINGS == null)
            {
                return NotFound();
            }

            db.MAS_SETTINGS.Remove(mAS_SETTINGS);
            db.SaveChanges();

            return Ok(mAS_SETTINGS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_SETTINGSExists(int id)
        {
            return db.MAS_SETTINGS.Count(e => e.ID == id) > 0;
        }
    }
}