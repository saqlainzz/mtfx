﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Microsoft.AspNet.Identity;
using log4net;

namespace AspNetIdentity.Controllers
{
    public class InstitutePaymentMethodsController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/InstitutePaymentMethods
        public List<CMN_INSTITUE_PAYMENT_METHODS> GetCMN_INSTITUE_PAYMENT_METHODS()
        {
            return db.CMN_INSTITUE_PAYMENT_METHODS.Include(s=>s.CMN_INSTITUTE).Include(s=>s.CMN_PAYMENT_METHOD).ToList();
        }

        // GET api/InstitutePaymentMethods/5
        [ResponseType(typeof(CMN_INSTITUE_PAYMENT_METHODS))]
        public IHttpActionResult GetCMN_INSTITUE_PAYMENT_METHODS(int id)
        {
            try
            {
                List<CMN_INSTITUE_PAYMENT_METHODS> cmn_institue_payment_methods = db.CMN_INSTITUE_PAYMENT_METHODS.Include(s => s.CMN_INSTITUTE).Include(s => s.CMN_PAYMENT_METHOD).Where(s => s.CMN_INSTITUTE_ID == id).ToList();
                if (cmn_institue_payment_methods == null)
                {
                    return NotFound();
                }

                return Ok(cmn_institue_payment_methods);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //// PUT api/InstitutePaymentMethods/5
        //public IHttpActionResult PutCMN_INSTITUE_PAYMENT_METHODS(int id, CMN_INSTITUE_PAYMENT_METHODS cmn_institue_payment_methods)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != cmn_institue_payment_methods.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(cmn_institue_payment_methods).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CMN_INSTITUE_PAYMENT_METHODSExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        public class BatchN
        {
            public int institueid;
            public int[] paymentmethodid;
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [HttpGet]
        [Route("api/InstitutePaymentMethods/create")]
        public IHttpActionResult SaveCMN_INSTITUE_PAYMENT_METHODS([FromUri] int institueid, [FromUri] string paymentmethodid)
        {
            try{
                paymentmethodid = paymentmethodid.Substring(1, paymentmethodid.Length - 2);

                int[] paymentmethodidarray = paymentmethodid.Split(',').Select(str => int.Parse(str)).ToArray();

                if (paymentmethodidarray.Count() == 0)
                {
                    return BadRequest();
                }

                List<CMN_INSTITUE_PAYMENT_METHODS> ipmList = db.CMN_INSTITUE_PAYMENT_METHODS.Where(s=>s.CMN_INSTITUTE_ID == institueid).ToList();

                for (int i = 0; i < ipmList.Count(); i++)
                {
                    db.CMN_INSTITUE_PAYMENT_METHODS.Remove(ipmList[i]);
                    db.SaveChanges();
                }

                for (int i = 0; i < paymentmethodidarray.Count(); i++)
                {
                    CMN_INSTITUE_PAYMENT_METHODS obj = new CMN_INSTITUE_PAYMENT_METHODS();

                    obj.CMN_INSTITUTE_ID = institueid;
                    obj.CMN_PAYMENT_METHOD_ID = paymentmethodidarray[i];
                    obj.CREATED_BY = User.Identity.GetUserId();
                    obj.UPDATED_BY = User.Identity.GetUserId();
                    obj.CREATED_ON = DateTime.Now;
                    obj.UPDATED_ON = DateTime.Now;

                    db.CMN_INSTITUE_PAYMENT_METHODS.Add(obj);
                    db.SaveChanges();
                }

                return Ok(formaingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public formaing formaingFunc(string state)
        {
            return new formaing
            {
                status = state
            };
        }

        public class formaing
        {
            public string status { get; set; }
        }

        // DELETE api/InstitutePaymentMethods/5
        //[ResponseType(typeof(CMN_INSTITUE_PAYMENT_METHODS))]
        //public IHttpActionResult DeleteCMN_INSTITUE_PAYMENT_METHODS(int id)
        //{
        //    CMN_INSTITUE_PAYMENT_METHODS cmn_institue_payment_methods = db.CMN_INSTITUE_PAYMENT_METHODS.Find(id);
        //    if (cmn_institue_payment_methods == null)
        //    {
        //        return NotFound();
        //    }

        //    db.CMN_INSTITUE_PAYMENT_METHODS.Remove(cmn_institue_payment_methods);
        //    db.SaveChanges();

        //    return Ok(cmn_institue_payment_methods);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_INSTITUE_PAYMENT_METHODSExists(int id)
        {
            return db.CMN_INSTITUE_PAYMENT_METHODS.Count(e => e.ID == id) > 0;
        }
    }
}