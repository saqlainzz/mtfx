﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;

namespace AspNetIdentity.Controllers
{
    public class OwnerController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        // GET api/Owner
        public List<MAS_OWNER> GetMAS_OWNER()
        {
            return db.MAS_OWNER.ToList();
        }

        // GET api/Owner/5
        [ResponseType(typeof(MAS_OWNER))]
        public IHttpActionResult GetMAS_OWNER(int id)
        {
            MAS_OWNER mas_owner = db.MAS_OWNER.Find(id);
            if (mas_owner == null)
            {
                return NotFound();
            }

            return Ok(mas_owner);
        }

        //// PUT api/Owner/5
        //public IHttpActionResult PutMAS_OWNER(int id, MAS_OWNER mas_owner)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != mas_owner.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(mas_owner).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!MAS_OWNERExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST api/Owner
        //[ResponseType(typeof(MAS_OWNER))]
        //public IHttpActionResult PostMAS_OWNER(MAS_OWNER mas_owner)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.MAS_OWNER.Add(mas_owner);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = mas_owner.ID }, mas_owner);
        //}

        //// DELETE api/Owner/5
        //[ResponseType(typeof(MAS_OWNER))]
        //public IHttpActionResult DeleteMAS_OWNER(int id)
        //{
        //    MAS_OWNER mas_owner = db.MAS_OWNER.Find(id);
        //    if (mas_owner == null)
        //    {
        //        return NotFound();
        //    }

        //    db.MAS_OWNER.Remove(mas_owner);
        //    db.SaveChanges();

        //    return Ok(mas_owner);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_OWNERExists(int id)
        {
            return db.MAS_OWNER.Count(e => e.ID == id) > 0;
        }
    }
}