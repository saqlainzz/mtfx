﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;

namespace AspNetIdentity.Controllers
{
    public class CityController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        // GET: api/City
        [HttpGet]
        [Route("api/City")]
        public IHttpActionResult GetCity()
        {
            return Ok(db.MAS_CITY);
        }

        [HttpGet]
        [Route("api/City")]
        public IHttpActionResult GetMASCity([FromUri] int id)
        {
            IQueryable<MAS_CITY> cityList = db.MAS_CITY.Where(s => s.STATE_ID == id);
            if (cityList.Count() == 0)
            {
                return NotFound();
            }

            return Ok(cityList);
        }

       

        // POST: api/City
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(MAS_CITY))]
        public IHttpActionResult PostMAS_CITY(MAS_CITY mAS_CITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAS_CITY.Add(mAS_CITY);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mAS_CITY.ID }, mAS_CITY);
        }

        // DELETE: api/City/5
        [ResponseType(typeof(MAS_CITY))]
        public IHttpActionResult DeleteMAS_CITY(int id)
        {
            MAS_CITY mAS_CITY = db.MAS_CITY.Find(id);
            if (mAS_CITY == null)
            {
                return NotFound();
            }

            db.MAS_CITY.Remove(mAS_CITY);
            db.SaveChanges();

            return Ok(mAS_CITY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}