﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;

namespace AspNetIdentity.Controllers
{
    public class StateController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        // GET: api/State
        [HttpGet]
        [Route("api/State")]
        public IHttpActionResult GetState()
        {
            return Ok(db.MAS_STATE);
        }

        [HttpGet]
        [Route("api/getCustomStates")]
        public IHttpActionResult getCustomStates([FromUri] int CountryID )
        {
            List<MAS_STATE_CODES> MAS_STATE_CODESList = db.MAS_STATE_CODES.Where(s => s.MAS_COUNTRY_ID == CountryID).ToList();

            if (MAS_STATE_CODESList.Count() == 0)
            {
                return NotFound();
            }

            return Ok(MAS_STATE_CODESList);
        }

        // GET: api/State/5
        [HttpGet]
        [Route("api/State")]
        public IHttpActionResult GetMASState([FromUri] int id)
        {
            IQueryable<MAS_STATE> stateList = db.MAS_STATE.Where(s => s.COUNTRY_ID == id);
            if (stateList.Count() == 0)
            {
                return NotFound();
            }

            return Ok(stateList);
        }
       
        // POST: api/State
        [ResponseType(typeof(MAS_STATE))]
        public IHttpActionResult PostMAS_STATE(MAS_STATE mAS_STATE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAS_STATE.Add(mAS_STATE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mAS_STATE.ID }, mAS_STATE);
        }

        // DELETE: api/State/5
        [ResponseType(typeof(MAS_STATE))]
        public IHttpActionResult DeleteMAS_STATE(int id)
        {
            MAS_STATE mAS_STATE = db.MAS_STATE.Find(id);
            if (mAS_STATE == null)
            {
                return NotFound();
            }

            db.MAS_STATE.Remove(mAS_STATE);
            db.SaveChanges();

            return Ok(mAS_STATE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}