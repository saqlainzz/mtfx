﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using System.IO;
using AspNetIdentity.Infrastructure;
using System.Configuration;
using log4net;

namespace AspNetIdentity.Controllers
{
    public class TransactionCancelController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/TransactionCancel
        public List<PAY_TRANSACTION_CANCEL_REASON> GetPAY_TRANSACTION_CANCEL_REASON()
        {
            return db.PAY_TRANSACTION_CANCEL_REASON.ToList();
        }

        // GET api/TransactionCancel/5
        [ResponseType(typeof(PAY_TRANSACTION_CANCEL_REASON))]
        public IHttpActionResult GetPAY_TRANSACTION_CANCEL_REASON(int id)
        {
            PAY_TRANSACTION_CANCEL_REASON pay_transaction_cancel_reason = db.PAY_TRANSACTION_CANCEL_REASON.Find(id);
            if (pay_transaction_cancel_reason == null)
            {
                return NotFound();
            }

            return Ok(pay_transaction_cancel_reason);
        }

        //// PUT api/TransactionCancel/5
        //public IHttpActionResult PutPAY_TRANSACTION_CANCEL_REASON(int id, PAY_TRANSACTION_CANCEL_REASON pay_transaction_cancel_reason)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != pay_transaction_cancel_reason.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(pay_transaction_cancel_reason).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PAY_TRANSACTION_CANCEL_REASONExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        public class CANCEL_REASON_CLASS
        {
            public int transactionid { get; set; }
            public string reason { get; set; }
        }

        [Authorize]
        [HttpPost]
        public IHttpActionResult PostPAY_TRANSACTION_CANCEL_REASON(CANCEL_REASON_CLASS CANCEL_REASON_CLASSObj)
        {
            try
            {
                AspNetIdentity.Infrastructure.TransactionContext transactionContext = new AspNetIdentity.Infrastructure.TransactionContext();

                PAY_TRANSACTION tran = transactionContext.PAY_TRANSACTION.Include(s => s.CMN_INSTITUTE).Where(s => s.ID == CANCEL_REASON_CLASSObj.transactionid).FirstOrDefault();

                if (tran == null)
                {
                    return BadRequest("Transaction ID is null or does not exist");
                }

                //string[] reasonsArray = null;

                //try
                //{
                //    reasonsArray = JsonConvert.DeserializeObject<string[]>(CANCEL_REASON_CLASSObj.reason);
                //}
                //catch (Exception ex)
                //{
                //    return BadRequest("Error in converting reason array");
                //}

                if (CANCEL_REASON_CLASSObj.reason == null)
                {
                    return BadRequest("Reason is null");
                }

                //for (int i = 0; i < reasonsArray.Count(); i++)
                //{
                PAY_TRANSACTION_CANCEL_REASON newObjct = new PAY_TRANSACTION_CANCEL_REASON();
                newObjct.TRANSACTION_ID = CANCEL_REASON_CLASSObj.transactionid;
                newObjct.REASON = CANCEL_REASON_CLASSObj.reason;
                newObjct.CREATED_ON = DateTime.Now;
                newObjct.CREATED_BY = User.Identity.GetUserId();
                newObjct.UPDATED_ON = DateTime.Now;
                newObjct.UPDATED_BY = User.Identity.GetUserId();
                db.PAY_TRANSACTION_CANCEL_REASON.Add(newObjct);
                db.SaveChanges();
                //}

                try
                {
                    string ID = User.Identity.GetUserId();
                    ApplicationDbContext context = new ApplicationDbContext();
                    ApplicationUser user = context.Users.Where(s => s.Id == ID).FirstOrDefault();

                    string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/CancelEmail.html"));
                    Body = Body.Replace("#NAME#", user.FirstName + " " + user.LastName);
                    Body = Body.Replace("#PAYMENTID#", tran.PREFIX);
                    Body = Body.Replace("#INSTITUTENAME#", tran.CMN_INSTITUTE.INSTITUTE_NAME);
                    Body = Body.Replace("#LINK#", ConfigurationManager.AppSettings["mainlink"].ToString());
                    string[] strarr = new string[] { user.Email };
                    string sub = "Your transaction to " + tran.CMN_INSTITUTE.INSTITUTE_NAME + " has been cancelled";

                    EMAIL_QUEUE eq = new EMAIL_QUEUE();
                    eq.SUBJECT = sub;
                    eq.BODY = Body;
                    eq.DATE_CREATED = DateTime.Now;
                    eq.EMAIL_TO = user.Email;

                    AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

                    Studentcontext.Entry(eq).State = EntityState.Added;
                    Studentcontext.EMAIL_QUEUE.Add(eq);
                    Studentcontext.SaveChanges();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }

                return Ok(formingFunc("Success!"));
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        public forming formingFunc(string state)
        {
            return new forming
            {
                status = state
            };
        }

        public class forming
        {
            public string status { get; set; }
        }

        // DELETE api/TransactionCancel/5
        //[ResponseType(typeof(PAY_TRANSACTION_CANCEL_REASON))]
        //public IHttpActionResult DeletePAY_TRANSACTION_CANCEL_REASON(int id)
        //{
        //    PAY_TRANSACTION_CANCEL_REASON pay_transaction_cancel_reason = db.PAY_TRANSACTION_CANCEL_REASON.Find(id);
        //    if (pay_transaction_cancel_reason == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PAY_TRANSACTION_CANCEL_REASON.Remove(pay_transaction_cancel_reason);
        //    db.SaveChanges();

        //    return Ok(pay_transaction_cancel_reason);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PAY_TRANSACTION_CANCEL_REASONExists(int id)
        {
            return db.PAY_TRANSACTION_CANCEL_REASON.Count(e => e.ID == id) > 0;
        }
    }
}