﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using AspNetIdentity.Infrastructure;
using Microsoft.AspNet.Identity;

namespace AspNetIdentity.Controllers
{
    public class ProgramOfStudyController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        [Authorize]
        [HttpGet]
        [Route("api/progofstudy")]
        public IHttpActionResult progofstudy()
        {
            string UserID = User.Identity.GetUserId();
            ApplicationDbContext ApplicationDbContext = new ApplicationDbContext();
            ApplicationUser AppUserObj = ApplicationDbContext.Users.Where(s => s.Id == UserID).FirstOrDefault();

            if (AppUserObj == null)
            {
                return BadRequest();
            }

            var typeList = db.MAS_PROGRAM_OF_STUDY.Where(s => s.CMN_INSTITUTE_ID == AppUserObj.InstituteID).ToList();

            return Ok(typeList);
        }

        [Authorize]
        [HttpGet]
        [Route("api/progofstudy/GetbyID")]
        public IHttpActionResult GetbyID([FromUri] int ID)
        {
            if (ID == 0)
            {
                return BadRequest("ID not correct");
            }

            var typeList = db.MAS_PROGRAM_OF_STUDY.Where(s => s.CMN_INSTITUTE_ID == ID).ToList();

            return Ok(typeList);
        }

        // GET: api/ProgramOfStudy
        public IQueryable<MAS_PROGRAM_OF_STUDY> GetMAS_PROGRAM_OF_STUDY()
        {
            return db.MAS_PROGRAM_OF_STUDY;
        }

        // GET: api/ProgramOfStudy/5
        [ResponseType(typeof(MAS_PROGRAM_OF_STUDY))]
        public IHttpActionResult GetMAS_PROGRAM_OF_STUDY(int id)
        {
            MAS_PROGRAM_OF_STUDY mAS_PROGRAM_OF_STUDY = db.MAS_PROGRAM_OF_STUDY.Find(id);
            if (mAS_PROGRAM_OF_STUDY == null)
            {
                return NotFound();
            }

            return Ok(mAS_PROGRAM_OF_STUDY);
        }

        // PUT: api/ProgramOfStudy/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMAS_PROGRAM_OF_STUDY(int id, MAS_PROGRAM_OF_STUDY mAS_PROGRAM_OF_STUDY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mAS_PROGRAM_OF_STUDY.ID)
            {
                return BadRequest();
            }

            db.Entry(mAS_PROGRAM_OF_STUDY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MAS_PROGRAM_OF_STUDYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProgramOfStudy
        [ResponseType(typeof(MAS_PROGRAM_OF_STUDY))]
        public IHttpActionResult PostMAS_PROGRAM_OF_STUDY(MAS_PROGRAM_OF_STUDY mAS_PROGRAM_OF_STUDY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAS_PROGRAM_OF_STUDY.Add(mAS_PROGRAM_OF_STUDY);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mAS_PROGRAM_OF_STUDY.ID }, mAS_PROGRAM_OF_STUDY);
        }

        // DELETE: api/ProgramOfStudy/5
        [ResponseType(typeof(MAS_PROGRAM_OF_STUDY))]
        public IHttpActionResult DeleteMAS_PROGRAM_OF_STUDY(int id)
        {
            MAS_PROGRAM_OF_STUDY mAS_PROGRAM_OF_STUDY = db.MAS_PROGRAM_OF_STUDY.Find(id);
            if (mAS_PROGRAM_OF_STUDY == null)
            {
                return NotFound();
            }

            db.MAS_PROGRAM_OF_STUDY.Remove(mAS_PROGRAM_OF_STUDY);
            db.SaveChanges();

            return Ok(mAS_PROGRAM_OF_STUDY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_PROGRAM_OF_STUDYExists(int id)
        {
            return db.MAS_PROGRAM_OF_STUDY.Count(e => e.ID == id) > 0;
        }
    }
}