﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using log4net;

namespace AspNetIdentity.Controllers
{
    public class BankDetailController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
   
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //// GET api/BankDetail
        //public IQueryable<PAY_BANK_DETAILS> GetPAY_BANK_DETAILS()
        //{
        //    return db.PAY_BANK_DETAILS;
        //}

        //// GET api/BankDetail/5
        //[ResponseType(typeof(PAY_BANK_DETAILS))]
        //public IHttpActionResult GetPAY_BANK_DETAILS(int id)
        //{
        //    PAY_BANK_DETAILS pay_bank_details = db.PAY_BANK_DETAILS.Find(id);
        //    if (pay_bank_details == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(pay_bank_details);
        //}

        [Authorize]
        [HttpGet]
        [Route("api/getBankDetail")]
        public IHttpActionResult AllTransactions([FromUri] int instituteID, [FromUri] int paymentMethodID, [FromUri] int countryID, [FromUri] int languageid = 1)
        {
            try
            {
                MAS_LANGUAGE MAS_LANGUAGEObj = db.MAS_LANGUAGE.Where(s=>s.ID == languageid).FirstOrDefault();

                if (MAS_LANGUAGEObj == null || MAS_LANGUAGEObj.LANGUAGE.ToLower() == "english")
                {
                    CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.MAS_COUNTRY_ID == countryID && s.CMN_PAYMENT_METHOD_ID == paymentMethodID).FirstOrDefault();

                    if (CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj == null)
                    {
                        return BadRequest("Bank Details Not Found");
                    }

                    PAY_BANK_DETAILS pay_bank_details = db.PAY_BANK_DETAILS.Where(s => s.MAS_INSTITUTE_ID == instituteID && s.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS_ID == CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj.ID).FirstOrDefault();

                    if (pay_bank_details == null)
                    {
                        return BadRequest("Bank Details Not Found");
                    }

                    return Ok(pay_bank_details);
                }
                else
                {
                    CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj = db.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Where(s => s.CMN_INSTITUTE_ID == instituteID && s.MAS_COUNTRY_ID == countryID && s.CMN_PAYMENT_METHOD_ID == paymentMethodID).FirstOrDefault();

                    if (CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj == null)
                    {
                        return BadRequest("Bank Details Not Found");
                    }

                    PAY_BANK_DETAILS_TRANSLATIONS pay_bank_details = db.PAY_BANK_DETAILS_TRANSLATIONS.Where(s => s.MAS_INSTITUTE_ID == instituteID && s.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS_ID == CMN_INSTITUTE_COUNTRY_PAYMENTMETHODSObj.ID && s.MAS_LANGUAGE_ID == languageid).FirstOrDefault();

                    if (pay_bank_details == null)
                    {
                        return BadRequest("Bank Details Not Found");
                    }

                    return Ok(pay_bank_details);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        //// PUT api/BankDetail/5
        //public IHttpActionResult PutPAY_BANK_DETAILS(int id, PAY_BANK_DETAILS pay_bank_details)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != pay_bank_details.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(pay_bank_details).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PAY_BANK_DETAILSExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST api/BankDetail
        //[ResponseType(typeof(PAY_BANK_DETAILS))]
        //public IHttpActionResult PostPAY_BANK_DETAILS(PAY_BANK_DETAILS pay_bank_details)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.PAY_BANK_DETAILS.Add(pay_bank_details);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = pay_bank_details.ID }, pay_bank_details);
        //}

        // DELETE api/BankDetail/5
        //[ResponseType(typeof(PAY_BANK_DETAILS))]
        //public IHttpActionResult DeletePAY_BANK_DETAILS(int id)
        //{
        //    PAY_BANK_DETAILS pay_bank_details = db.PAY_BANK_DETAILS.Find(id);
        //    if (pay_bank_details == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PAY_BANK_DETAILS.Remove(pay_bank_details);
        //    db.SaveChanges();

        //    return Ok(pay_bank_details);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PAY_BANK_DETAILSExists(int id)
        {
            return db.PAY_BANK_DETAILS.Count(e => e.ID == id) > 0;
        }
    }
}