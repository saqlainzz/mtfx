﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using System.IO;
using System.Configuration;

namespace AspNetIdentity.Controllers
{
    public class UserFeedbackController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        [HttpPost]
        [Route("api/Feedback/ClientEmail")]
        public IHttpActionResult ClientEmail(clientEmailFormat clientEmailFormatObj)
        {

            string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/ClientEmailCentre.html"));
            Body = Body.Replace("#FIRSTNAME#", clientEmailFormatObj.firstname);
            Body = Body.Replace("#LASTNAME#", clientEmailFormatObj.lastname);
            Body = Body.Replace("#EMAIL#", clientEmailFormatObj.email);
            Body = Body.Replace("#MOBILE#", clientEmailFormatObj.mobile);
            Body = Body.Replace("#SERVICE#", clientEmailFormatObj.service);
            Body = Body.Replace("#CONSENT#", clientEmailFormatObj.consent);

            EMAIL_QUEUE eq = new EMAIL_QUEUE();
            eq.SUBJECT = "MTFX Client Email Centre";
            eq.BODY = Body;
            eq.DATE_CREATED = DateTime.Now;
            eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();

            AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

            Studentcontext.Entry(eq).State = EntityState.Added;
            Studentcontext.EMAIL_QUEUE.Add(eq);
            Studentcontext.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("api/Feedback/RequestCallBack")]
        public IHttpActionResult RequestCallBack(clientEmailFormat clientEmailFormatObj)
        {

            string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/RequestCallBack.html"));
            Body = Body.Replace("#FIRSTNAME#", clientEmailFormatObj.firstname);
            Body = Body.Replace("#LASTNAME#", clientEmailFormatObj.lastname);
            Body = Body.Replace("#EMAIL#", clientEmailFormatObj.email);
            Body = Body.Replace("#MOBILE#", clientEmailFormatObj.mobile);
            Body = Body.Replace("#SERVICE#", clientEmailFormatObj.service);

            EMAIL_QUEUE eq = new EMAIL_QUEUE();
            eq.SUBJECT = "Request Call Back";
            eq.BODY = Body;
            eq.DATE_CREATED = DateTime.Now;
            eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();

            AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

            Studentcontext.Entry(eq).State = EntityState.Added;
            Studentcontext.EMAIL_QUEUE.Add(eq);
            Studentcontext.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("api/Feedback/PaymytuitionClient")]
        public IHttpActionResult PaymytuitionClient(clientEmailFormat clientEmailFormatObj)
        {
            if (clientEmailFormatObj.consent == "YES")
            {
                string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/PersonalTransferYes.html"));
                Body = Body.Replace("#EMAIL#", clientEmailFormatObj.email);

                EMAIL_QUEUE eq = new EMAIL_QUEUE();
                eq.SUBJECT = "Paymytuition Client";
                eq.BODY = Body;
                eq.DATE_CREATED = DateTime.Now;
                eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

                Studentcontext.Entry(eq).State = EntityState.Added;
                Studentcontext.EMAIL_QUEUE.Add(eq);
                Studentcontext.SaveChanges();
                return Ok();
            }
            else
            {
                string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/PersonalTransferNo.html"));
                Body = Body.Replace("#FIRSTNAME#", clientEmailFormatObj.firstname);
                Body = Body.Replace("#LASTNAME#", clientEmailFormatObj.lastname);
                Body = Body.Replace("#EMAIL#", clientEmailFormatObj.email);
                Body = Body.Replace("#MOBILE#", clientEmailFormatObj.mobile);

                EMAIL_QUEUE eq = new EMAIL_QUEUE();
                eq.SUBJECT = "Paymytuition Client";
                eq.BODY = Body;
                eq.DATE_CREATED = DateTime.Now;
                eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

                Studentcontext.Entry(eq).State = EntityState.Added;
                Studentcontext.EMAIL_QUEUE.Add(eq);
                Studentcontext.SaveChanges();
                return Ok();
            }
          
        }

        [HttpPost]
        [Route("api/Feedback/ContactUS")]
        public IHttpActionResult ContactUS(contactUSFormat contactUSFormatObj)
        {

            string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/ContactUs.html"));

            if (contactUSFormatObj.firstname == null)
            {
                Body = Body.Replace("#FIRSTNAME#", "");
            }
            else
            {
                Body = Body.Replace("#FIRSTNAME#", contactUSFormatObj.firstname);
            }

            if (contactUSFormatObj.lastname == null)
            {
                Body = Body.Replace("#LASTNAME#", "");
            }
            else
            {
                Body = Body.Replace("#LASTNAME#", contactUSFormatObj.lastname);
            }

            if (contactUSFormatObj.email == null)
            {
                Body = Body.Replace("#EMAIL#", "");
            }
            else
            {
                Body = Body.Replace("#EMAIL#", contactUSFormatObj.email);
            }

            if (contactUSFormatObj.mobile == null)
            {
                Body = Body.Replace("#MOBILE#", "");
            }
            else
            {
                Body = Body.Replace("#MOBILE#", contactUSFormatObj.mobile);
            }

            if (contactUSFormatObj.subject == null)
            {
                Body = Body.Replace("#SUBJECT#", "");
            }
            else
            {
                Body = Body.Replace("#SUBJECT#", contactUSFormatObj.subject);
            }

            if (contactUSFormatObj.message == null)
            {
                Body = Body.Replace("#MESSAGE#", "");
            }
            else
            {
                Body = Body.Replace("#MESSAGE#", contactUSFormatObj.message);
            }

            EMAIL_QUEUE eq = new EMAIL_QUEUE();
            eq.SUBJECT = "Paymytuition Client";
            eq.BODY = Body;
            eq.DATE_CREATED = DateTime.Now;
            eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();

            AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

            Studentcontext.Entry(eq).State = EntityState.Added;
            Studentcontext.EMAIL_QUEUE.Add(eq);
            Studentcontext.SaveChanges();

            if (contactUSFormatObj.copy == true)
            {
                eq = new EMAIL_QUEUE();
                eq.SUBJECT = "Paymytuition Client";
                eq.BODY = Body;
                eq.DATE_CREATED = DateTime.Now;
                eq.EMAIL_TO = contactUSFormatObj.email;

                Studentcontext.Entry(eq).State = EntityState.Added;
                Studentcontext.EMAIL_QUEUE.Add(eq);
                Studentcontext.SaveChanges();
            }

            return Ok();
        }

        [HttpPost]
        [Route("api/Feedback/educationalInstitutions")]
        public IHttpActionResult educationalInstitutions(educationalInstitutionsFormat educationalInstitutionsFormat)
        {

            string Body = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Resources/EducationalSpecialist.html"));

            if (educationalInstitutionsFormat.firstname == null)
            {
                Body = Body.Replace("#FIRSTNAME#", "");
            }
            else
            {
                Body = Body.Replace("#FIRSTNAME#", educationalInstitutionsFormat.firstname);
            }

            if (educationalInstitutionsFormat.lastname == null)
            {
                Body = Body.Replace("#LASTNAME#", "");
            }
            else
            {
                Body = Body.Replace("#LASTNAME#", educationalInstitutionsFormat.lastname);
            }

            if (educationalInstitutionsFormat.email == null)
            {
                Body = Body.Replace("#EMAIL#", "");
            }
            else
            {
                Body = Body.Replace("#EMAIL#", educationalInstitutionsFormat.email);
            }

            if (educationalInstitutionsFormat.educationalInstitution == null)
            {
                Body = Body.Replace("#INSTITUTE#", "");
            }
            else
            {
                Body = Body.Replace("#INSTITUTE#", educationalInstitutionsFormat.educationalInstitution);
            }

            if (educationalInstitutionsFormat.department == null)
            {
                Body = Body.Replace("#DEPARTMENT#", "");
            }
            else
            {
                Body = Body.Replace("#DEPARTMENT#", educationalInstitutionsFormat.department);
            }

            if (educationalInstitutionsFormat.NoOfStudents == null)
            {
                Body = Body.Replace("#STUDENTS#", "");
            }
            else
            {
                Body = Body.Replace("#STUDENTS#", educationalInstitutionsFormat.NoOfStudents);
            }

            if (educationalInstitutionsFormat.Country == null)
            {
                Body = Body.Replace("#COUNTRY#", "");
            }
            else
            {
                Body = Body.Replace("#COUNTRY#", educationalInstitutionsFormat.Country);
            }

            EMAIL_QUEUE eq = new EMAIL_QUEUE();
            eq.SUBJECT = "Contact an Educational Specialist";
            eq.BODY = Body;
            eq.DATE_CREATED = DateTime.Now;
            eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();

            AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

            Studentcontext.Entry(eq).State = EntityState.Added;
            Studentcontext.EMAIL_QUEUE.Add(eq);
            Studentcontext.SaveChanges();

            return Ok();
        }

        public class clientEmailFormat
        {
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string mobile { get; set; }
            public string service { get; set; }
            public string consent { get; set; }
        }

        public class eduInstEmailFormat
        {
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string mobile { get; set; }
            public string service { get; set; }
            public string consent { get; set; }
        }

        public class educationalInstitutionsFormat
        {
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string educationalInstitution { get; set; }
            public string department { get; set; }
            public string NoOfStudents { get; set; }
            public string Country { get; set; }
        }

        public class contactUSFormat
        {
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string mobile { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
            public bool copy { get; set; }
        }

        // GET api/UserFeedback
        public List<MAS_FEEDBACK> GetMAS_FEEDBACK()
        {
            return db.MAS_FEEDBACK.Where(s=>s.IS_APPROVED == true).ToList();
        }

        // GET api/UserFeedback/5
        [ResponseType(typeof(MAS_FEEDBACK))]
        public IHttpActionResult GetMAS_FEEDBACK(int id)
        {
            MAS_FEEDBACK mas_feedback = db.MAS_FEEDBACK.Find(id);
            if (mas_feedback == null)
            {
                return NotFound();
            }

            return Ok(mas_feedback);
        }

        // PUT api/UserFeedback/5
        public IHttpActionResult PutMAS_FEEDBACK(int id, MAS_FEEDBACK mas_feedback)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mas_feedback.ID)
            {
                return BadRequest();
            }

            db.Entry(mas_feedback).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MAS_FEEDBACKExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/UserFeedback
        [ResponseType(typeof(MAS_FEEDBACK))]
        public IHttpActionResult PostMAS_FEEDBACK(MAS_FEEDBACK mas_feedback)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAS_FEEDBACK.Add(mas_feedback);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mas_feedback.ID }, mas_feedback);
        }

        // DELETE api/UserFeedback/5
        [ResponseType(typeof(MAS_FEEDBACK))]
        public IHttpActionResult DeleteMAS_FEEDBACK(int id)
        {
            MAS_FEEDBACK mas_feedback = db.MAS_FEEDBACK.Find(id);
            if (mas_feedback == null)
            {
                return NotFound();
            }

            db.MAS_FEEDBACK.Remove(mas_feedback);
            db.SaveChanges();

            return Ok(mas_feedback);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_FEEDBACKExists(int id)
        {
            return db.MAS_FEEDBACK.Count(e => e.ID == id) > 0;
        }
    }
}