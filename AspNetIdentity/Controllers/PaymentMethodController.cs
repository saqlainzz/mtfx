﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using AspNetIdentity.ServiceReference1;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/paymentmethod")]
    public class PaymentMethodController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private PaymentMethodContext db = new PaymentMethodContext();
        private AspNetIdentityContext context = new AspNetIdentityContext();

        [Route("paymentmethods")]
        public IHttpActionResult GetCountries()
        {
            return Ok(db.CMN_PAYMENT_METHOD);
        }

        public class format
        {
            public int method_id { get; set; }
            public string value { get; set; }
        }

        [Route("getRate")]
        [HttpGet]
        public async Task<IHttpActionResult> getRate([FromUri] int instituteid, [FromUri] int countryid, [FromUri] double amount, [FromUri] int languageid = 1)
        {
            Log.Info("API Call IN");
            Dictionary<string, double> dictionary = new Dictionary<string, double>();
            Dictionary<string, bool> Ratedictionary = new Dictionary<string, bool>();
            Dictionary<string, double> Discountdictionary = new Dictionary<string, double>();

            try
            {
                AspNetIdentityContext context = new AspNetIdentityContext();
                MAS_LANGUAGE MAS_LANGUAGEObj = context.MAS_LANGUAGE.Where(s=>s.ID == languageid).FirstOrDefault();

                if (instituteid == 0)
                {
                    return BadRequest("InstituteID not valid");
                }

                CMN_INSTITUTE CMN_INSTITUTEObj = context.CMN_INSTITUTE.Include(s=>s.CMN_CURRENCY).Where(s => s.ID == instituteid).FirstOrDefault();

                if (countryid == 0)
                {
                    return BadRequest("InstituteID not valid");
                }

                List<CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS> countryMethodList = context.CMN_INSTITUTE_COUNTRY_PAYMENTMETHODS.Include(s => s.MAS_COUNTRY.CMN_CURRENCY).Include(s => s.CMN_PAYMENT_METHOD.CMN_CURRENCY).Include(s => s.CMN_INSTITUTE.CMN_CURRENCY).Where(s => s.CMN_INSTITUTE_ID == instituteid && s.MAS_COUNTRY_ID == countryid).ToList();

                if (countryMethodList.Count == 0)
                {
                    return BadRequest("No Payment Methods found");
                }

                List<getRateFormat> listGetRate = new List<getRateFormat>();
                string baseCurrency = "";
                string baseSymbol = "";
                //string localCurrency = "";
                //double localRate = 0;
                //double LocalAmount = 0;
                double BUY_AMOUNT = 0;

                for (int i = 0; i < countryMethodList.Count; i++)
                {
                    getRateFormat obj = new getRateFormat();
                    obj.PAYMENT_METHOD_ID = countryMethodList[i].CMN_PAYMENT_METHOD_ID;

                    if (countryMethodList[i].CMN_PAYMENT_METHOD.CMN_CURRENCY == null)
                    {
                        baseCurrency = countryMethodList[i].MAS_COUNTRY.CMN_CURRENCY.CURR_SYMBOL;
                        baseSymbol = countryMethodList[i].MAS_COUNTRY.CMN_CURRENCY.CURR_UNIT;
                        obj.CMN_CURRENCY = countryMethodList[i].MAS_COUNTRY.CMN_CURRENCY;
                    }
                    else
                    {
                        baseCurrency = countryMethodList[i].CMN_PAYMENT_METHOD.CMN_CURRENCY.CURR_SYMBOL;
                        baseSymbol = countryMethodList[i].CMN_PAYMENT_METHOD.CMN_CURRENCY.CURR_UNIT;
                        obj.CMN_CURRENCY = countryMethodList[i].CMN_PAYMENT_METHOD.CMN_CURRENCY;
                    }

                    string convertCurrency = countryMethodList[i].CMN_INSTITUTE.CMN_CURRENCY.CURR_SYMBOL;

                    if (amount < 1)
                    {
                        obj.Rate = 0;
                        obj.Discount = 0;
                        obj.Is_Rate = true;
                        obj.Amount = 0;
                        BUY_AMOUNT = amount;
                        obj.Fees = "0";
                    }
                    else
                    {
                        if (baseCurrency == convertCurrency)
                        {
                            obj.Rate = 0;
                            obj.Is_Rate = true;
                            obj.Amount = amount;
                            BUY_AMOUNT = amount;
                            obj.Fees = countryMethodList[i].FEES;
                        }
                        else
                        {
                            if (dictionary.ContainsKey(baseCurrency))
                            {
                                obj.Rate = dictionary[baseCurrency];
                                if (amount > 3000)
                                {
                                    obj.Discount = Discountdictionary[baseCurrency];
                                }
                                else
                                {
                                    obj.Discount = 0;
                                }
                                obj.Is_Rate = Ratedictionary[baseCurrency];
                                obj.Amount = (amount * dictionary[baseCurrency]);
                                BUY_AMOUNT = (amount * dictionary[baseCurrency]);
                            }
                            else
                            {
                                Log.Info("Calling Web Request");
                                string URL = "http://apilayer.net/api/live";
                                string urlParameters = "?access_key=" + ConfigurationManager.AppSettings["RateApiKey"].ToString() + "&currencies=" + baseCurrency + "&source=" + convertCurrency + "&format=1";

                                HttpClient client = new HttpClient();
                                client.BaseAddress = new Uri(URL);

                                // Add an Accept header for JSON format.
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                // List data response.
                                HttpResponseMessage response = client.GetAsync(urlParameters).Result;

                                double newRate = 0.0;
                                
                                var jsonString = response.Content.ReadAsStringAsync();
                                jsonString.Wait();
                                var model = JsonConvert.DeserializeObject<dynamic>(jsonString.Result);

                                if (Convert.ToBoolean(model.success))
                                {
                                    newRate = model.quotes[convertCurrency + baseCurrency];
                                    obj.Is_Rate = true;
                                    Ratedictionary.Add(baseCurrency, true);
                                    if (countryMethodList[i].DISCOUNT != null)
                                    {
                                        Discountdictionary.Add(baseCurrency, Double.Parse(countryMethodList[i].DISCOUNT.ToString()));
                                    }
                                    else
                                    {
                                        Discountdictionary.Add(baseCurrency, 0);
                                    }
                                }
                                else
                                {
                                    string BodyPart = "Rate API is down";
 
                                    EMAIL_QUEUE eq = new EMAIL_QUEUE();
                                    eq.SUBJECT = "Rate API is down";
                                    eq.BODY = BodyPart;
                                    eq.DATE_CREATED = DateTime.Now;
                                    eq.EMAIL_TO = ConfigurationManager.AppSettings["AdminEmail"].ToString();
                                    eq.BCC = ConfigurationManager.AppSettings["AdminEmail"].ToString();

                                    AspNetIdentityContext Studentcontext = new AspNetIdentityContext();

                                    Studentcontext.Entry(eq).State = EntityState.Added;
                                    Studentcontext.EMAIL_QUEUE.Add(eq);
                                    Studentcontext.SaveChanges();

                                    BCK_RATES ratesBckup = context.BCK_RATES.Where(s => s.FROM_CURRECNCY_SYMBOL == convertCurrency && s.TO_CURRECNCY_SYMBOL == baseCurrency).FirstOrDefault();
                                    newRate = double.Parse(ratesBckup.RATE);
                                    obj.Is_Rate = false;
                                    Ratedictionary.Add(baseCurrency, false);
                                    if (countryMethodList[i].DISCOUNT != null)
                                    {
                                        Discountdictionary.Add(baseCurrency, Double.Parse(countryMethodList[i].DISCOUNT.ToString()));

                                        if (amount > 3000)
                                        {
                                            obj.Discount = Double.Parse(countryMethodList[i].DISCOUNT.ToString());
                                        }
                                        else
                                        {
                                            obj.Discount = 0;
                                        } 
                                    }
                                    else
                                    {
                                        Discountdictionary.Add(baseCurrency, 0);
                                        obj.Discount = 0;
                                    }
                                }
                                
                                
                                Log.Info("Got Web response");
                                Log.Info(response);
                                //string responc = "";// response.GetRateResult;
                                Log.Info("responc" + newRate);
                                try
                                {
                                    dictionary.Add(baseCurrency, newRate);                                  
                                    obj.Rate = newRate;
                                    obj.Amount = (amount * newRate);
                                    BUY_AMOUNT = (amount * newRate);
                                }
                                catch (Exception ex)
                                {
                                    return BadRequest(ex.Message+" "+ex.InnerException);
                                }
                            }

                        }

                        if (countryMethodList[i].DISCOUNT != null)
                        {
                            if (amount > 3000)
                            {
                                obj.Discount = Double.Parse(countryMethodList[i].DISCOUNT.ToString());
                            }
                            else
                            {
                                obj.Discount = 0;
                            } 
                        }
                        else
                        {
                            obj.Discount = 0;
                        }

                        if (countryMethodList[i].BUFFER_PERCENTAGE != null)
                        {
                            double percentage = (double)countryMethodList[i].BUFFER_PERCENTAGE;
                            double finalAmount = obj.Amount;
                            double increment = ((finalAmount * percentage) / 100);

                            double returnAmount = finalAmount + increment;
                            obj.Buffer_Rate = percentage;
                            obj.Amount = returnAmount;
                        }
                    }

                    string Body = "";

                    if (MAS_LANGUAGEObj.LANGUAGE.ToLower() == "english")
                    {
                        Body = countryMethodList[i].CMN_PAYMENT_METHOD.DESCRIPTION;
                    }
                    else
                    {
                        int eyedee = countryMethodList[i].CMN_PAYMENT_METHOD.ID;
                        CMN_PAYMENT_METHOD_TRANSLATIONS CMN_PAYMENT_METHOD_TRANSLATIONSObj = context.CMN_PAYMENT_METHOD_TRANSLATIONS.Where(s => s.MAS_LANGUAGE_ID == languageid && s.PAYMENT_METHOD_ID == eyedee).FirstOrDefault();

                        if (CMN_PAYMENT_METHOD_TRANSLATIONSObj == null)
                        {
                            Body = countryMethodList[i].CMN_PAYMENT_METHOD.DESCRIPTION;
                        }
                        else
                        {
                            Body = CMN_PAYMENT_METHOD_TRANSLATIONSObj.DESCRIPTION;
                        }
                    }

                    if (amount < 1)
                    {
                        //string Body = countryMethodList[i].CMN_PAYMENT_METHOD.DESCRIPTION;

                        string helper = baseSymbol + "" + amount;
                        string helper2 = baseSymbol + "" + BUY_AMOUNT;

                        if (obj.Discount != 0)
                        {
                            if (countryMethodList[i].DISCOUNT == 0)
                            {
                                Body = Body.Replace("Less {{DiscountAmount}} {{currencyPaying}} Rebate", "");
                            }
                            else
                            {
                                Body = Body.Replace("{{DiscountAmount}}", countryMethodList[i].DISCOUNT.ToString());
                            }
                        }
                        else
                        {
                            Body = Body.Replace("Less {{DiscountAmount}} {{currencyPaying}} Rebate", "");
                        }

                        Body = Body.Replace("{{totalAmount}}", helper);
                        Body = Body.Replace("{{AmountPaying}}", helper2);
                        Body = Body.Replace("{{currencyPaying}}", baseCurrency);
                        Body = Body.Replace("{{currency}}", convertCurrency);
                        Body = Body.Replace("{{country}}", countryMethodList[i].MAS_COUNTRY.COUNTRY_NAME);
                        Body = Body.Replace("{{fees}}", "");

                        obj.Description = Body;
                    }
                    else
                    {
                        //string Body = countryMethodList[i].CMN_PAYMENT_METHOD.DESCRIPTION;

                        string str = "";

                        if (countryMethodList[i].FEES != null)
                        {
                            str = " + " + baseSymbol + "" + obj.Fees + " " + baseCurrency + " Fee ";
                        }
                        else
                        {
                            str = "";
                        }

                        obj.Amount = Math.Round(obj.Amount, 0);

                       // obj.Amount = double.Parse(obj.Amount.ToString("0.##"));
                        //double subtractedAmount = amount - obj.Discount;
                        //string discountcommaAmount = String.Format("{0:n}", subtractedAmount);
                        //string discounthelper = baseSymbol + "" + discountcommaAmount;
                        
                        string commaAmount = String.Format("{0:n}", amount);
                        string helper = baseSymbol + "" + commaAmount;
                        string commaAmount2 = String.Format("{0:n}", Math.Round(obj.Amount, 0));
                        string helper2 = baseSymbol + "" + commaAmount2;

                        if (obj.Discount != 0)
                        {
                            if (countryMethodList[i].DISCOUNT == 0)
                            {
                                Body = Body.Replace("Less {{DiscountAmount}} {{currencyPaying}} Rebate", "");
                            }
                            else
                            {
                                Body = Body.Replace("{{DiscountAmount}}", countryMethodList[i].DISCOUNT.ToString());
                            }
                        }
                        else
                        {
                            Body = Body.Replace("Less {{DiscountAmount}} {{currencyPaying}} Rebate", "");
                        } 
                        
                        Body = Body.Replace("{{totalAmount}}", helper);
                        Body = Body.Replace("{{AmountPaying}}", helper2);
                        Body = Body.Replace("{{currencyPaying}}", baseCurrency);
                        Body = Body.Replace("{{currency}}", convertCurrency);
                        Body = Body.Replace("{{country}}", countryMethodList[i].MAS_COUNTRY.COUNTRY_NAME);
                        Body = Body.Replace("{{fees}}", str);

                        obj.Description = Body;
                    }
                   
                    listGetRate.Add(obj);
                }

                Log.Info("API call out");
                Log.Info(listGetRate);
                
                return Ok(listGetRate);
            }
            catch (Exception ex)
            {
                Log.Debug(ex);
                return BadRequest(ex.ToString());
            }
        }
        public class getRateFormat
        {
            public double Amount { get; set; }
            public double Rate { get; set; }
            public double Buffer_Rate { get; set; }
            public bool Is_Rate { get; set; }
            public CMN_CURRENCY CMN_CURRENCY { get; set; }
            public string Fees { get; set; }
            public double Discount { get; set; }
            public int PAYMENT_METHOD_ID { get; set; }
            public string Description { get; set; }
        }

        [Route("DemoTranslate")]
        [HttpGet]
        public IHttpActionResult DemoTranslate()
        {
            string str = "{how_it_works : 'How It Works',help : 'help',login: 'login',aim_high: 'Aim High!',aim_high_sub_heading : 'We put the SMART into international tuition payments.',aim_high_point_1 : 'Pay your fees from any country at any bank in any currency.',aim_high_point_2 : 'Dont overpay your bank on currency exchange.Paymytuition will save you money!',aim_high_point_3 : 'Track your payments from start to finish using Paymytuition students dashboard.',aim_high_point_4 : 'Multilingual customer support to help you when you need us.',want_to_pay : 'I want to pay'}";
            return Ok(str);
        }

        [Route("GetDetail")]
        [HttpPost]
        public IHttpActionResult GetDetail(format format)
        {
            try
            {
                CMN_PAYMENT_METHOD CMN_PAYMENT_METHODMethod = db.CMN_PAYMENT_METHOD.Find(format.method_id);

                if (format.method_id == 0)
                {
                    return Content(HttpStatusCode.NotFound, "Invalid Payment Method ID");
                }

                string Body = CMN_PAYMENT_METHODMethod.DESCRIPTION;

                Body = Body.Replace("{{totalAmount}}", format.value);
                Body = Body.Replace("{{currency}}", format.value);
                Body = Body.Replace("{{country}}", format.value);

                CMN_PAYMENT_METHODMethod.DESCRIPTION = Body;

                return Ok(CMN_PAYMENT_METHODMethod);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [ResponseType(typeof(CMN_PAYMENT_METHOD))]
        public IHttpActionResult GetCMN_PAYMENT_METHOD(int id)
        {
            try{
                CMN_PAYMENT_METHOD CMN_PAYMENT_METHODMethod = db.CMN_PAYMENT_METHOD.Find(id);
                if (CMN_PAYMENT_METHODMethod == null)
                {
                    return NotFound();
                }

                return Ok(CMN_PAYMENT_METHODMethod);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [ResponseType(typeof(CMN_PAYMENT_METHOD))]
        public IHttpActionResult PostCMN_PAYMENT_METHOD(CMN_PAYMENT_METHOD CMN_PAYMENT_METHODMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(CMN_PAYMENT_METHODMethod).State = EntityState.Added;
            db.CMN_PAYMENT_METHOD.Add(CMN_PAYMENT_METHODMethod);
            db.SaveChanges();

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        public IHttpActionResult PutCMN_PAYMENT_METHOD(int id, CMN_PAYMENT_METHOD CMN_PAYMENT_METHODMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != CMN_PAYMENT_METHODMethod.ID)
            {
                return BadRequest();
            }

            db.Entry(CMN_PAYMENT_METHODMethod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CMN_COUNTRYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE api/Default1/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(CMN_PAYMENT_METHOD))]
        public IHttpActionResult DeleteCMN_PAYMENT_METHOD(int id)
        {
            CMN_PAYMENT_METHOD CMN_PAYMENT_METHODMethod = db.CMN_PAYMENT_METHOD.Find(id);
            if (CMN_PAYMENT_METHODMethod == null)
            {
                return NotFound();
            }

            db.CMN_PAYMENT_METHOD.Remove(CMN_PAYMENT_METHODMethod);
            db.SaveChanges();

            return Ok(CMN_PAYMENT_METHODMethod);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.CMN_PAYMENT_METHOD.Count(e => e.ID == id) > 0;
        }
    }
}