﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;
using Microsoft.AspNet.Identity;
using AspNetIdentity.Infrastructure;
using log4net;
using AspNetIdentity.ServiceReference1;
using System.Xml;

namespace AspNetIdentity.Controllers
{
    public class StudentController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/Student
        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        public List<MAS_STUDENT> GetMAS_STUDENT()
        {
            return db.MAS_STUDENT.ToList();
        }

        // GET api/Student/5
        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [ResponseType(typeof(MAS_STUDENT))]
        public IHttpActionResult GetMAS_STUDENT(int id)
        {
            MAS_STUDENT mas_student = db.MAS_STUDENT.Find(id);
            if (mas_student == null)
            {
                return NotFound();
           } 

            return Ok(mas_student);
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [Route("api/Student/checkStudent")]
        [HttpGet]
        public IHttpActionResult checkStudent([FromUri] string studentID, [FromUri] int instituteID)
        {
            MAS_STUDENT tranObj = db.MAS_STUDENT.Where(s => s.STUDENT_ID == studentID && s.MAS_INSTITUTE_ID == instituteID).FirstOrDefault();

            if (tranObj == null)
            {
                return BadRequest("Student does not exist");
            }
            else
            {
                return Ok(tranObj);
            }
        }

        [Authorize(Roles = "Admin, Sub_Admin, SuperAdmin, University")]
        [Route("api/Student/updateStatus")]
        [HttpGet]
        public IHttpActionResult updateStatus([FromUri] int studentID, [FromUri] string status, [FromUri] int transactionID)
        {
            MAS_STUDENT tranObj = db.MAS_STUDENT.Where(s => s.ID == studentID).FirstOrDefault();

            if (tranObj == null)
            {
                return BadRequest(ModelState);
            }

            AspNetIdentity.Infrastructure.TransactionContext trandb = new AspNetIdentity.Infrastructure.TransactionContext();
            PAY_TRANSACTION PAY_TRANSACTIONObj = trandb.PAY_TRANSACTION.Include(s=>s.CMN_INSTITUTE).Include(s=>s.CMN_INSTITUTE.MAS_COUNTRY).Where(s=>s.ID == transactionID).FirstOrDefault();

            if(PAY_TRANSACTIONObj == null)
            {
                return BadRequest("No Transaction");
            }

            TransactionDetailContext tranDetaildb = new TransactionDetailContext();
            PAY_TRANSACTION_DETAIL PAY_TRANSACTION_DETAILObj = tranDetaildb.PAY_TRANSACTION_DETAIL.Include(s=>s.MAS_PERSON_FILLING).Include(s => s.MAS_PAYMENT_TYPE).Include(s => s.MAS_COUNTRY).Where(s => s.PAY_TRANSACTION_ID == transactionID).FirstOrDefault();

            if(PAY_TRANSACTION_DETAILObj == null)
            {
                return BadRequest("No Transaction Detail");
            }

            ServiceReference1.DSWSSoapClient svcClient = new ServiceReference1.DSWSSoapClient();

            UserCredentials us = new UserCredentials();
            us.userName = "devry";
            us.passWord = "ds";
            us.custID = "0004938";

            string customerService = "";

            if (PAY_TRANSACTIONObj.CMN_INSTITUTE.MAS_COUNTRY.ISO == "CA")
            {
                customerService = "Group Paymytuition";
            }
            else
            {
                customerService = "MTUS TRADING";
            }

            string getxml = "<INPUT><FILTER><TYPE_NAME TYPE=\"STRING\" OPERATOR=\"EQUALS TO\">EMPLOYEE</TYPE_NAME></FILTER></INPUT>";

            string GetResponseEntity = svcClient.GetAllEntities(us, getxml);

            XmlDocument GetxmlDoc = new XmlDocument();
            GetxmlDoc.LoadXml(GetResponseEntity);
            XmlNodeList GetxnList = GetxmlDoc.SelectNodes("OUTPUT/ENTITY");
            List<string> strArr = new List<string>();
            string salesChannelID = "";

            foreach (XmlNode xn in GetxnList)
            {
                if (xn["NAME"].InnerText == PAY_TRANSACTIONObj.CMN_INSTITUTE.INSTITUTE_NAME)
                {
                    salesChannelID = xn["ENTITY_ID"].InnerText;
                }
            }

            if (salesChannelID == "")
            {
                string Createxml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                      + "<INPUT><ENTITY>"
                      + "<NAME>" + PAY_TRANSACTIONObj.CMN_INSTITUTE.INSTITUTE_NAME + "</NAME>"
                      + "<ALIAS>" + PAY_TRANSACTIONObj.CMN_INSTITUTE.INSTITUTE_NAME + "</ALIAS>"
                      + "<STATUS><ID>1</ID></STATUS>"
                      + "<ENTITY_TYPE><ID>18</ID></ENTITY_TYPE></ENTITY></INPUT>";

                string CreateResponseEntity = svcClient.CreateEntity(us, Createxml);

                XmlDocument createxmlDoc = new XmlDocument();
                createxmlDoc.LoadXml(CreateResponseEntity);
                XmlNodeList createxnList = createxmlDoc.SelectNodes("OUTPUT/RESULT");

                foreach (XmlNode xn in createxnList)
                {
                    if (xn["STATUS"].InnerText == "TRUE")
                    {
                        salesChannelID = xn["RECORDID"].InnerText;
                    }
                    else
                    {
                        return BadRequest("ERROR : " + xn["ERROR"].InnerText);
                    }
                }
            }

            string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                        + "<INPUT><ENTITY>"
                        + "<NAME>" + PAY_TRANSACTION_DETAILObj.STUDENT_FIRST_NAME + " " + PAY_TRANSACTION_DETAILObj.STUDENT_LAST_NAME + "</NAME>"
                        + "<ALIAS>" + PAY_TRANSACTION_DETAILObj.STUDENT_FIRST_NAME + " " + PAY_TRANSACTION_DETAILObj.STUDENT_LAST_NAME + "</ALIAS>"
                        + "<STATUS><ID>1</ID></STATUS>"
                        + "<ENTITY_TYPE><ID>21</ID></ENTITY_TYPE></ENTITY>"
                        + "<HOME_ADDRESS><STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 == null ? null : PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                        + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE>"
                        + "<CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY>"
                        + "<PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE>"
                        + "<OTHER></OTHER>"
                        + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                        + "<IS_DEFAULT>true</IS_DEFAULT></HOME_ADDRESS>"
                        + "<INTERNET><EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</EMAIL>"
                        + "<ALTERNATE_EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</ALTERNATE_EMAIL>"
                        + "<WEB_SITE></WEB_SITE></INTERNET>"
                        + "<PHONE><BUSINESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_PHONE_NUMBER + "</BUSINESS>"
                        + "<DIRECT></DIRECT><MOBILE></MOBILE></PHONE>"
                        + "<CUSTOMERSERVICE><RELATIONSHIP_MANAGER><Value>PAYMYTUITION</Value></RELATIONSHIP_MANAGER>"
                        + "<CUSTOMER_SERVICE_GROUP><Value>" + customerService + "</Value></CUSTOMER_SERVICE_GROUP>"
                        + "<INTRODUCED_BY><Value>PAYMYTUITION</Value></INTRODUCED_BY>"
                        + "<CUST_REMARKS>" + PAY_TRANSACTION_DETAILObj.MAS_PAYMENT_TYPE.TITLE + " " + PAY_TRANSACTION_DETAILObj.PROGRAM_OF_STUDY + "</CUST_REMARKS>"
                        + "<RELATIONSHIP_START_DATE>" + DateTime.Now.ToString() + "</RELATIONSHIP_START_DATE>"
                        + "<ACQUIRED_REASON><Value>REFERRAL</Value></ACQUIRED_REASON><SALES_CHANNEL1>" + salesChannelID + "</SALES_CHANNEL1><SALES_CHANNEL2>19132</SALES_CHANNEL2><SALES_CHANNEL5>" + salesChannelID + "</SALES_CHANNEL5></CUSTOMERSERVICE>"
                        + "<INDIVIDUAL_PROFILE><DATE_OF_BIRTH>" + PAY_TRANSACTION_DETAILObj.STUDENT_DATE_OF_BIRTH + "</DATE_OF_BIRTH>"
                        + "<OCCUPATION>STUDENT</OCCUPATION>"
                        + "<IS_FACE_TO_FACE>1</IS_FACE_TO_FACE>"
                        + "<INDV_ACQUISITION_METHOD_ID><Value>REFERRAL</Value></INDV_ACQUISITION_METHOD_ID>"
                        + "<INDV_NATURE_OF_RELATIONSHIP><Value>TUITION FEES</Value></INDV_NATURE_OF_RELATIONSHIP>"
                        + "<INDV_PURPOSE_OF_RELATIONSHIP><Value>INTERNATIONAL MONEY TRANSFERS</Value></INDV_PURPOSE_OF_RELATIONSHIP>"
                        + "<ACQUISITION_DETAILS></ACQUISITION_DETAILS>"
                        + "<IS_THIRD_PARTY>0</IS_THIRD_PARTY></INDIVIDUAL_PROFILE><INDV_ADDRESS><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY>"
                        + "<PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE><OTHER>No No</OTHER>"
                        + "<INDV_COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></INDV_COUNTRY>"
                        + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                        + "<ADDRESS_TYPE></ADDRESS_TYPE></INDV_ADDRESS></INPUT>";

            string responseEntity = svcClient.CreateEntity(us, xml);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(responseEntity);
            XmlNodeList xnList = xmlDoc.SelectNodes("OUTPUT/RESULT");

            bool submissionStatus = false;
            string entityID = "";

            foreach (XmlNode xn in xnList)
            {
                if (xn["STATUS"].InnerText == "TRUE")
                {
                    submissionStatus = true;
                    entityID = xn["RECORDID"].InnerText;
                }
                else
                {
                    return BadRequest("ERROR : " + xn["ERROR"].InnerText);
                }
            }

            if (PAY_TRANSACTION_DETAILObj.IS_PAYEE_SAME == false)
            {
                string contactXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                    + "<INPUT><WEB><CONFIRMATION_EMAIL>" + PAY_TRANSACTION_DETAILObj.PAYEE_EMAIL + "</CONFIRMATION_EMAIL>"
                    + "<EMAIL>" + PAY_TRANSACTION_DETAILObj.PAYEE_EMAIL + "</EMAIL>"
                    + "<ALTERNATE_EMAIL>" + PAY_TRANSACTION_DETAILObj.PAYEE_EMAIL + "</ALTERNATE_EMAIL>"
                    + "<WEB_SITE></WEB_SITE></WEB>"
                    + "<CONTACT><ENTITY><ID>" + entityID + "</ID></ENTITY>"
                    + "<SALUTATION></SALUTATION><NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME + "</NAME>"
                    + "<DESCRIPTION>" + PAY_TRANSACTION_DETAILObj.MAS_PERSON_FILLING.TITLE + "</DESCRIPTION>"
                    + "<LAST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME + "</LAST_NAME><INITIAL></INITIAL>"
                    + "<IS_PRIMARY_CONTACT>false</IS_PRIMARY_CONTACT>"
                    + "<AUTHORIZED_SIGNATORY>true</AUTHORIZED_SIGNATORY>"
                    + "<AUTHORIZED_TO_TRANSACT>true</AUTHORIZED_TO_TRANSACT>"
                    + "<IS_ACTIVE>true</IS_ACTIVE>"
                    + "<GROUP_ID></GROUP_ID></CONTACT>"
                    + "<PHONE><ASSISTANT></ASSISTANT>"
                    + "<BUSINESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_PHONE_NUMBER + "</BUSINESS>"
                    + "<BUSINESS_FAX></BUSINESS_FAX>"
                    + "<DIRECT></DIRECT><MOBILE></MOBILE><OTHER></OTHER><OTHER_FAX></OTHER_FAX><PAGER></PAGER></PHONE>"
                    + "<HOME_ADDRESS>"
                    + "<STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                    + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY>"
                    + "<PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE><OTHER></OTHER>"
                    + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                    + "<IS_DEFAULT>false</IS_DEFAULT></HOME_ADDRESS></INPUT>";

                string responseContact = svcClient.CreateContact(us, contactXML);

                string contactXMLTwo = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                   + "<INPUT><WEB><CONFIRMATION_EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</CONFIRMATION_EMAIL>"
                   + "<EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</EMAIL>"
                   + "<ALTERNATE_EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</ALTERNATE_EMAIL>"
                   + "<WEB_SITE></WEB_SITE></WEB>"
                   + "<CONTACT><ENTITY><ID>" + entityID + "</ID></ENTITY>"
                   + "<SALUTATION></SALUTATION><NAME>" + PAY_TRANSACTION_DETAILObj.STUDENT_FIRST_NAME + "</NAME>"
                   + "<DESCRIPTION>STUDENT</DESCRIPTION>"
                   + "<LAST_NAME>" + PAY_TRANSACTION_DETAILObj.STUDENT_LAST_NAME + "</LAST_NAME><INITIAL></INITIAL>"
                   + "<IS_PRIMARY_CONTACT>true</IS_PRIMARY_CONTACT>"
                   + "<AUTHORIZED_SIGNATORY>true</AUTHORIZED_SIGNATORY>"
                   + "<AUTHORIZED_TO_TRANSACT>true</AUTHORIZED_TO_TRANSACT>"
                   + "<IS_ACTIVE>true</IS_ACTIVE>"
                   + "<GROUP_ID></GROUP_ID></CONTACT>"
                   + "<PHONE><ASSISTANT></ASSISTANT>"
                   + "<BUSINESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_PHONE_NUMBER + "</BUSINESS>"
                   + "<BUSINESS_FAX></BUSINESS_FAX>"
                   + "<DIRECT></DIRECT><MOBILE></MOBILE><OTHER></OTHER><OTHER_FAX></OTHER_FAX><PAGER></PAGER></PHONE>"
                   + "<OFFICE_ADDRESS>"
                   + "<STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                   + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY>"
                   + "<PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE><OTHER></OTHER>"
                   + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                   + "<IS_DEFAULT>false</IS_DEFAULT></OFFICE_ADDRESS></INPUT>";

                string responseContact1 = svcClient.CreateContact(us, contactXMLTwo);
               
                int a = 1;
            }
            else
            {
                string contactXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                   + "<INPUT><WEB><CONFIRMATION_EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</CONFIRMATION_EMAIL>"
                   + "<EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</EMAIL>"
                   + "<ALTERNATE_EMAIL>" + PAY_TRANSACTION_DETAILObj.STUDENT_EMAIL + "</ALTERNATE_EMAIL>"
                   + "<WEB_SITE></WEB_SITE></WEB>"
                   + "<CONTACT><ENTITY><ID>" + entityID + "</ID></ENTITY>"
                   + "<SALUTATION></SALUTATION><NAME>" + PAY_TRANSACTION_DETAILObj.STUDENT_FIRST_NAME + "</NAME>"
                   + "<DESCRIPTION>STUDENT</DESCRIPTION>"
                   + "<LAST_NAME>" + PAY_TRANSACTION_DETAILObj.STUDENT_LAST_NAME + "</LAST_NAME><INITIAL></INITIAL>"
                   + "<IS_PRIMARY_CONTACT>true</IS_PRIMARY_CONTACT>"
                   + "<AUTHORIZED_SIGNATORY>true</AUTHORIZED_SIGNATORY>"
                   + "<AUTHORIZED_TO_TRANSACT>true</AUTHORIZED_TO_TRANSACT>"
                   + "<IS_ACTIVE>true</IS_ACTIVE>"
                   + "<GROUP_ID></GROUP_ID></CONTACT>"
                   + "<PHONE><ASSISTANT></ASSISTANT>"
                   + "<BUSINESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_PHONE_NUMBER + "</BUSINESS>"
                   + "<BUSINESS_FAX></BUSINESS_FAX>"
                   + "<DIRECT></DIRECT><MOBILE></MOBILE><OTHER></OTHER><OTHER_FAX></OTHER_FAX><PAGER></PAGER></PHONE>"
                   + "<OFFICE_ADDRESS>"
                   + "<STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                   + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY>"
                   + "<PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE><OTHER></OTHER>"
                   + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                   + "<IS_DEFAULT>true</IS_DEFAULT></OFFICE_ADDRESS></INPUT>";

                string responseContact = svcClient.CreateContact(us, contactXML);
                int a = 1;
            }

            string IndividualIdentificationXML = "";

            if (PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_TYPE.ToLower() == "passport")
            {
                IndividualIdentificationXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                    + "<INPUT><INDIVIDUAL_IDENTIFICATION><ENTITY_ID>" + entityID + "</ENTITY_ID>"
                    + "<IDENTIFICATION_TYPE_ID><ID>102</ID></IDENTIFICATION_TYPE_ID>"
                    + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.PAYER_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                    + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                    + "<ISSUE_DATE></ISSUE_DATE>"
                    + "<VERIFICATION_DATE></VERIFICATION_DATE>"
                    + "<ATTESTATION_METHOD_ID></ATTESTATION_METHOD_ID>"
                    + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE + "</DESTINATION_PATH><ACTUAL_FILE_NAME></ACTUAL_FILE_NAME>"
                    + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_EXPIRY + "</EXPIRY_DATE></INDIVIDUAL_IDENTIFICATION></INPUT>";

                string responseContact = svcClient.CreateIndividualIdentification(us, IndividualIdentificationXML);
                int a = 1;
            }
            else
            {
                IndividualIdentificationXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                    + "<INPUT><INDIVIDUAL_IDENTIFICATION><ENTITY_ID>" + entityID + "</ENTITY_ID>"
                    + "<IDENTIFICATION_TYPE_ID><ID>98</ID></IDENTIFICATION_TYPE_ID>"
                    + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.PAYER_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                    + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                    + "<ISSUE_DATE></ISSUE_DATE>"
                    + "<VERIFICATION_DATE></VERIFICATION_DATE>"
                    + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE + "</DESTINATION_PATH><ACTUAL_FILE_NAME></ACTUAL_FILE_NAME>"
                    + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_EXPIRY + "</EXPIRY_DATE></INDIVIDUAL_IDENTIFICATION></INPUT>";

                string responseContact = svcClient.CreateIndividualIdentification(us, IndividualIdentificationXML);
                int a = 1;
            }


            string IndividualIdentificationStudentXML = "";

            if (PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_TYPE.ToLower() == "passport")
            {
                IndividualIdentificationStudentXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                    + "<INPUT><INDIVIDUAL_IDENTIFICATION><ENTITY_ID>" + entityID + "</ENTITY_ID>"
                    + "<IDENTIFICATION_TYPE_ID><ID>102</ID></IDENTIFICATION_TYPE_ID>"
                    + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.STUDENT_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                    + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                    + "<ISSUE_DATE></ISSUE_DATE>"
                    + "<VERIFICATION_DATE></VERIFICATION_DATE>"
                    + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE + "</DESTINATION_PATH><ACTUAL_FILE_NAME></ACTUAL_FILE_NAME>"
                    + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_EXPIRY + "</EXPIRY_DATE></INDIVIDUAL_IDENTIFICATION></INPUT>";

                string responseContact = svcClient.CreateIndividualIdentification(us, IndividualIdentificationStudentXML);
                int a = 1;
            }
            else
            {
                IndividualIdentificationStudentXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                    + "<INPUT><INDIVIDUAL_IDENTIFICATION><ENTITY_ID>" + entityID + "</ENTITY_ID>"
                    + "<IDENTIFICATION_TYPE_ID><ID>98</ID></IDENTIFICATION_TYPE_ID>"
                    + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.STUDENT_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                    + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                    + "<ISSUE_DATE></ISSUE_DATE>"
                    + "<VERIFICATION_DATE></VERIFICATION_DATE>"
                    + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE + "</DESTINATION_PATH><ACTUAL_FILE_NAME></ACTUAL_FILE_NAME>"
                    + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_EXPIRY + "</EXPIRY_DATE></INDIVIDUAL_IDENTIFICATION></INPUT>";

                string responseContact = svcClient.CreateIndividualIdentification(us, IndividualIdentificationStudentXML);
                int a = 1;
            }


            if (PAY_TRANSACTION_DETAILObj.IS_PAYEE_SAME == false)
            {
                if (PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_TYPE.ToLower() == "passport")
                {
                    string CreateSignatureCardXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                      + "<INPUT><SIGNATURE_CARD>"
                      + "<ENTITY_ID>" + entityID + "</ENTITY_ID><TITLE>Mr.</TITLE>"
                      + "<FIRST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME + "</FIRST_NAME><LAST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME + "</LAST_NAME>"
                      + "<INITIAL></INITIAL><AUTHORIZED_SIGNATORY>1</AUTHORIZED_SIGNATORY>"
                      + "<AUTHORIZED_TO_TRANSACT>1</AUTHORIZED_TO_TRANSACT><OCCUPATION>" + PAY_TRANSACTION_DETAILObj.MAS_PERSON_FILLING.TITLE + "</OCCUPATION>"
                      + "<DATE_OF_BIRTH></DATE_OF_BIRTH><COPY_FROM_SOURCE></COPY_FROM_SOURCE>"
                      + "<COPY_FROM_SOURCE_ID></COPY_FROM_SOURCE_ID></SIGNATURE_CARD>"
                      + "<ADDRESS><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY><PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE>"
                      + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                      + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                      + "</ADDRESS>"
                      + "<DETAIL><ID></ID>"
                      + "<IDENTIFICATION_TYPE_ID><ID>102</ID></IDENTIFICATION_TYPE_ID>"
                      + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.STUDENT_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                      + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                      + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE + "</DESTINATION_PATH>"
                      + "<ACTUAL_FILE_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_NAME + "</ACTUAL_FILE_NAME>"
                      + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_EXPIRY + "</EXPIRY_DATE></DETAIL></INPUT>";

                    string responseContact = svcClient.CreateSignatureCard(us, CreateSignatureCardXML);
                    int a = 1;
                }
                else
                {
                    string CreateSignatureCardXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                      + "<INPUT><SIGNATURE_CARD>"
                      + "<ENTITY_ID>" + entityID + "</ENTITY_ID><TITLE>Mr.</TITLE>"
                      + "<FIRST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME + "</FIRST_NAME><LAST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME + "</LAST_NAME>"
                      + "<INITIAL></INITIAL><AUTHORIZED_SIGNATORY>1</AUTHORIZED_SIGNATORY>"
                      + "<AUTHORIZED_TO_TRANSACT>1</AUTHORIZED_TO_TRANSACT><OCCUPATION>" + PAY_TRANSACTION_DETAILObj.MAS_PERSON_FILLING.TITLE + "</OCCUPATION>"
                      + "<DATE_OF_BIRTH></DATE_OF_BIRTH><COPY_FROM_SOURCE></COPY_FROM_SOURCE>"
                      + "<COPY_FROM_SOURCE_ID></COPY_FROM_SOURCE_ID></SIGNATURE_CARD>"
                      + "<ADDRESS><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY><PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE>"
                      + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                      + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                      + "</ADDRESS>"
                      + "<DETAIL><ID></ID>"
                      + "<IDENTIFICATION_TYPE_ID><ID>98</ID></IDENTIFICATION_TYPE_ID>"
                      + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.STUDENT_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                      + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                      + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_FILE + "</DESTINATION_PATH>"
                      + "<ACTUAL_FILE_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_NAME + "</ACTUAL_FILE_NAME>"
                      + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.PAYEE_DOCUMENT_EXPIRY + "</EXPIRY_DATE></DETAIL></INPUT>";

                    string responseContact = svcClient.CreateSignatureCard(us, CreateSignatureCardXML);
                    int a = 1;
                }

            }
            else
            {
                if (PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_TYPE.ToLower() == "passport")
                {
                    string CreateSignatureCardXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                      + "<INPUT><SIGNATURE_CARD>"
                      + "<ENTITY_ID>" + entityID + "</ENTITY_ID><TITLE>Mr.</TITLE>"
                      + "<FIRST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME + "</FIRST_NAME><LAST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME + "</LAST_NAME>"
                      + "<INITIAL></INITIAL><AUTHORIZED_SIGNATORY>1</AUTHORIZED_SIGNATORY>"
                      + "<AUTHORIZED_TO_TRANSACT>1</AUTHORIZED_TO_TRANSACT><OCCUPATION>" + PAY_TRANSACTION_DETAILObj.MAS_PERSON_FILLING.TITLE + "</OCCUPATION>"
                      + "<DATE_OF_BIRTH>" + PAY_TRANSACTION_DETAILObj.STUDENT_DATE_OF_BIRTH + "</DATE_OF_BIRTH><COPY_FROM_SOURCE></COPY_FROM_SOURCE>"
                      + "<COPY_FROM_SOURCE_ID></COPY_FROM_SOURCE_ID></SIGNATURE_CARD>"
                      + "<ADDRESS><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY><PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE>"
                      + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                      + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                      + "</ADDRESS>"
                       + "<DETAIL><ID></ID>"
                      + "<IDENTIFICATION_TYPE_ID><ID>102</ID></IDENTIFICATION_TYPE_ID>"
                      + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.STUDENT_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                      + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                      + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE + "</DESTINATION_PATH>"
                      + "<ACTUAL_FILE_NAME>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_NAME + "</ACTUAL_FILE_NAME>"
                      + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_EXPIRY + "</EXPIRY_DATE></DETAIL></INPUT>";

                    string responseContact = svcClient.CreateSignatureCard(us, CreateSignatureCardXML);
                    int a = 1;
                }
                else
                {
                    string CreateSignatureCardXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                      + "<INPUT><SIGNATURE_CARD>"
                      + "<ENTITY_ID>" + entityID + "</ENTITY_ID><TITLE>Mr.</TITLE>"
                      + "<FIRST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_FIRST_NAME + "</FIRST_NAME><LAST_NAME>" + PAY_TRANSACTION_DETAILObj.PAYEE_LAST_NAME + "</LAST_NAME>"
                      + "<INITIAL></INITIAL><AUTHORIZED_SIGNATORY>1</AUTHORIZED_SIGNATORY>"
                      + "<AUTHORIZED_TO_TRANSACT>1</AUTHORIZED_TO_TRANSACT><OCCUPATION>" + PAY_TRANSACTION_DETAILObj.MAS_PERSON_FILLING.TITLE + "</OCCUPATION>"
                      + "<DATE_OF_BIRTH>" + PAY_TRANSACTION_DETAILObj.STUDENT_DATE_OF_BIRTH + "</DATE_OF_BIRTH><COPY_FROM_SOURCE></COPY_FROM_SOURCE>"
                      + "<COPY_FROM_SOURCE_ID></COPY_FROM_SOURCE_ID></SIGNATURE_CARD>"
                      + "<ADDRESS><CITY>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_CITY + "</CITY><PROVINCE>" + PAY_TRANSACTION_DETAILObj.PAYEE_MAS_STATE + "</PROVINCE>"
                      + "<COUNTRY><Value>" + PAY_TRANSACTION_DETAILObj.MAS_COUNTRY.COUNTRY_NAME + "</Value></COUNTRY>"
                      + "<POST_CODE>" + PAY_TRANSACTION_DETAILObj.PAYEE_POSTAL_CODE + "</POST_CODE><STREET_ADDRESS>" + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE1 + " " + PAY_TRANSACTION_DETAILObj.PAYEE_ADDRESS_LINE2 + "</STREET_ADDRESS>"
                      + "</ADDRESS>"
                       + "<DETAIL><ID></ID>"
                      + "<IDENTIFICATION_TYPE_ID><ID>98</ID></IDENTIFICATION_TYPE_ID>"
                      + "<IDENTIFICATION_NUMBER>" + PAY_TRANSACTION_DETAILObj.STUDENT_SENDER_IDENTIFICATION_NUMBER + "</IDENTIFICATION_NUMBER>"
                      + "<PLACE_OF_ISSUE></PLACE_OF_ISSUE>"
                      + "<DESTINATION_PATH>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_FILE + "</DESTINATION_PATH>"
                      + "<ACTUAL_FILE_NAME>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_NAME + "</ACTUAL_FILE_NAME>"
                      + "<EXPIRY_DATE>" + PAY_TRANSACTION_DETAILObj.STUDENT_DOCUMENT_EXPIRY + "</EXPIRY_DATE></DETAIL></INPUT>";

                    string responseContact = svcClient.CreateSignatureCard(us, CreateSignatureCardXML);
                    int a = 1;
                }
            }

            //string CreateSignatureCardXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
            //        + "<INPUT><SIGNATURE_CARD>"
            //        + "<TITLE>Mr.</TITLE><INITIAL>testini</INITIAL>"
            //        + "<DATE_OF_BIRTH>2004-07-01 00:00:00.000</DATE_OF_BIRTH></SIGNATURE_CARD></INPUT>";

            //svcClient.CreateSignatureCard(us, CreateSignatureCardXML);

            tranObj.STATUS = status;
            tranObj.UPDATED_BY = User.Identity.GetUserId();
            tranObj.UPDATED_ON = DateTime.Now;
            db.Entry(tranObj).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(tranObj);
        }

        // PUT api/Student/5
        //public IHttpActionResult PutMAS_STUDENT(int id, MAS_STUDENT mas_student)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != mas_student.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(mas_student).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!MAS_STUDENTExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST api/Student
        //[ResponseType(typeof(MAS_STUDENT))]
        //public IHttpActionResult PostMAS_STUDENT(MAS_STUDENT mas_student)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.MAS_STUDENT.Add(mas_student);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = mas_student.ID }, mas_student);
        //}

        //// DELETE api/Student/5
        //[ResponseType(typeof(MAS_STUDENT))]
        //public IHttpActionResult DeleteMAS_STUDENT(int id)
        //{
        //    MAS_STUDENT mas_student = db.MAS_STUDENT.Find(id);
        //    if (mas_student == null)
        //    {
        //        return NotFound();
        //    }

        //    db.MAS_STUDENT.Remove(mas_student);
        //    db.SaveChanges();

        //    return Ok(mas_student);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_STUDENTExists(int id)
        {
            return db.MAS_STUDENT.Count(e => e.ID == id) > 0;
        }
    }
}