﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/semester")]
    public class SemesterController : ApiController
    {
        private SemesterContext db = new SemesterContext();
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("semesters")]
        public IHttpActionResult GetCountries()
        {
            return Ok(db.MAS_SEMESTER);
        }

        [ResponseType(typeof(MAS_SEMESTER))]
        public IHttpActionResult GetMAS_SEMESTER(int id)
        {
            MAS_SEMESTER MAS_SEMESTERMethod = db.MAS_SEMESTER.Find(id);
            if (MAS_SEMESTERMethod == null)
            {
                return NotFound();
            }

            return Ok(MAS_SEMESTERMethod);
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [ResponseType(typeof(MAS_SEMESTER))]
        public IHttpActionResult PostMAS_SEMESTER(MAS_SEMESTER MAS_SEMESTERMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(MAS_SEMESTERMethod).State = EntityState.Added;
            db.MAS_SEMESTER.Add(MAS_SEMESTERMethod);
            db.SaveChanges();

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        public IHttpActionResult PutMAS_SEMESTER(int id, MAS_SEMESTER MAS_SEMESTERMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != MAS_SEMESTERMethod.ID)
            {
                return BadRequest();
            }

            db.Entry(MAS_SEMESTERMethod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CMN_COUNTRYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE api/Default1/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(MAS_SEMESTER))]
        public IHttpActionResult DeleteMAS_SEMESTER(int id)
        {
            MAS_SEMESTER MAS_SEMESTERMethod = db.MAS_SEMESTER.Find(id);
            if (MAS_SEMESTERMethod == null)
            {
                return NotFound();
            }

            db.MAS_SEMESTER.Remove(MAS_SEMESTERMethod);
            db.SaveChanges();

            return Ok(MAS_SEMESTERMethod);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.MAS_SEMESTER.Count(e => e.ID == id) > 0;
        }
    }
}