﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetIdentity.Models;

namespace AspNetIdentity.Controllers
{
    public class LanguageController : ApiController
    {
        private AspNetIdentityContext db = new AspNetIdentityContext();

        [HttpGet]
        [Route("api/Language/GetLanguages")]
        public List<MAS_LANGUAGE> GetLanguages()
        {
            return db.MAS_LANGUAGE.ToList();
        }

        // GET: api/Language/5
        [ResponseType(typeof(MAS_LANGUAGE))]
        public IHttpActionResult GetMAS_LANGUAGE(int id)
        {
            MAS_LANGUAGE mAS_LANGUAGE = db.MAS_LANGUAGE.Find(id);
            if (mAS_LANGUAGE == null)
            {
                return NotFound();
            }

            return Ok(mAS_LANGUAGE);
        }

        // PUT: api/Language/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutMAS_LANGUAGE(int id, MAS_LANGUAGE mAS_LANGUAGE)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != mAS_LANGUAGE.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(mAS_LANGUAGE).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!MAS_LANGUAGEExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Language
        //[ResponseType(typeof(MAS_LANGUAGE))]
        //public IHttpActionResult PostMAS_LANGUAGE(MAS_LANGUAGE mAS_LANGUAGE)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.MAS_LANGUAGE.Add(mAS_LANGUAGE);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = mAS_LANGUAGE.ID }, mAS_LANGUAGE);
        //}

        //// DELETE: api/Language/5
        //[ResponseType(typeof(MAS_LANGUAGE))]
        //public IHttpActionResult DeleteMAS_LANGUAGE(int id)
        //{
        //    MAS_LANGUAGE mAS_LANGUAGE = db.MAS_LANGUAGE.Find(id);
        //    if (mAS_LANGUAGE == null)
        //    {
        //        return NotFound();
        //    }

        //    db.MAS_LANGUAGE.Remove(mAS_LANGUAGE);
        //    db.SaveChanges();

        //    return Ok(mAS_LANGUAGE);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAS_LANGUAGEExists(int id)
        {
            return db.MAS_LANGUAGE.Count(e => e.ID == id) > 0;
        }
    }
}