﻿using AspNetIdentity.Infrastructure;
using AspNetIdentity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace AspNetIdentity.Controllers
{
    [RoutePrefix("api/currency")]
    public class CurrencyController : ApiController
    {
        private CurrencyContext db = new CurrencyContext();

        [Authorize]
        [Route("currencies")]
        public IHttpActionResult GetCountries()
        {
            return Ok(db.CMN_CURRENCY);
        }

        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(CMN_CURRENCY))]
        public IHttpActionResult GetCMN_CURRENCY(int id)
        {
            CMN_CURRENCY cmn_CMN_CURRENCY = db.CMN_CURRENCY.Find(id);
            if (cmn_CMN_CURRENCY == null)
            {
                return NotFound();
            }

            return Ok(cmn_CMN_CURRENCY);
        }

        //[Authorize(Roles = "Admin")]
        //[Route("create")]
        //[ResponseType(typeof(CMN_CURRENCY))]
        //public IHttpActionResult PostCMN_CURRENCY(CMN_CURRENCY cmn_CMN_CURRENCY)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Entry(cmn_CMN_CURRENCY).State = EntityState.Added;
        //    db.CMN_CURRENCY.Add(cmn_CMN_CURRENCY);
        //    db.SaveChanges();

        //    return Ok();
        //}

        //[Authorize(Roles = "Admin")]
        //public IHttpActionResult PutCMN_CURRENCY(int id, CMN_CURRENCY cmn_CMN_CURRENCY)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != cmn_CMN_CURRENCY.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(cmn_CMN_CURRENCY).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CMN_COUNTRYExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// DELETE api/Default1/5
        //[Authorize(Roles = "Admin")]
        //[ResponseType(typeof(CMN_CURRENCY))]
        //public IHttpActionResult DeleteCMN_CURRENCY(int id)
        //{
        //    CMN_CURRENCY cmn_CMN_CURRENCY = db.CMN_CURRENCY.Find(id);
        //    if (cmn_CMN_CURRENCY == null)
        //    {
        //        return NotFound();
        //    }

        //    db.CMN_CURRENCY.Remove(cmn_CMN_CURRENCY);
        //    db.SaveChanges();

        //    return Ok(cmn_CMN_CURRENCY);
        //}


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CMN_COUNTRYExists(int id)
        {
            return db.CMN_CURRENCY.Count(e => e.ID == id) > 0;
        }
    }
}