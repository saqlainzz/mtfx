﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspNetIdentity.Infrastructure
{
    public class TransactionDetailContext : DbContext
    {
        public TransactionDetailContext()
            : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_TRANSACTION_DETAIL> PAY_TRANSACTION_DETAIL { get; set; }
    
    }
}