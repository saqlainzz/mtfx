﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspNetIdentity.Infrastructure
{
    public class PaymentMethodContext : DbContext
    {
        public PaymentMethodContext()
            : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_PAYMENT_METHOD> CMN_PAYMENT_METHOD { get; set; }
    
    }
}