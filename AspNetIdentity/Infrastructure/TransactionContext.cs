﻿using Amazon.S3.Model;
using AspNetIdentity.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AspNetIdentity.Infrastructure
{
    public class TransactionContext : DbContext
    {
        public TransactionContext()
            : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.PAY_TRANSACTION> PAY_TRANSACTION { get; set; }
    }
}