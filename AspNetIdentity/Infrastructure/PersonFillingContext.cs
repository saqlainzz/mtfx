﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspNetIdentity.Infrastructure
{
    public class PersonFillingContext: DbContext
    {
        public PersonFillingContext()
            : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.MAS_PERSON_FILLING> MAS_PERSON_FILLING { get; set; }
    
    }
}