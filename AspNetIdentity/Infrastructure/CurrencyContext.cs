﻿using AspNetIdentity.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspNetIdentity.Infrastructure
{
    public class CurrencyContext : DbContext
    {
        public CurrencyContext()
            : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_CURRENCY> CMN_CURRENCY { get; set; }
    
    }
}