﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspNetIdentity.Infrastructure
{
    public class InstituteContext: DbContext
    {
        public InstituteContext()
            : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public System.Data.Entity.DbSet<AspNetIdentity.Models.CMN_INSTITUTE> CMN_INSTITUTE { get; set; }
    
    }
}