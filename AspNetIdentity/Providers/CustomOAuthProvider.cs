﻿using AspNetIdentity.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using AspNetIdentity.Models;

namespace AspNetIdentity.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        String InstituteID = "";
        String LoginType = "";
        // 0 for university accounts

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            InstituteID = context.Parameters.ToList()[3].Value[0];
            LoginType = context.Parameters.ToList()[4].Value[0];

            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            var allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            var roleManager = context.OwinContext.GetUserManager<ApplicationRoleManager>();

            int intInstitute = Convert.ToInt32(InstituteID);

            ApplicationUser userTemp = null;
            
            if (LoginType.ToLower() == "admin")
            {
                var roles = roleManager.Roles.Where(s => s.Name == "Admin").FirstOrDefault();
                userTemp = userManager.Users.Where(s => s.Email == context.UserName && s.Roles.Where(j => j.RoleId == roles.Id).FirstOrDefault() != null).FirstOrDefault();
            }
            else if (LoginType.ToLower() == "university")
            {
                var roles = roleManager.Roles.Where(s => s.Name == "University").FirstOrDefault();
                userTemp = userManager.Users.Where(s => s.Email == context.UserName && s.Roles.Where(j => j.RoleId == roles.Id).FirstOrDefault() != null).FirstOrDefault();

                if (userTemp == null)
                {
                    var rolesSub = roleManager.Roles.Where(s => s.Name == "Sub_Admin").FirstOrDefault();
                    userTemp = userManager.Users.Where(s => s.Email == context.UserName && s.Roles.Where(j => j.RoleId == rolesSub.Id).FirstOrDefault() != null).FirstOrDefault();
                }
            }
            else if (LoginType.ToLower() == "sub_admin")
            {
                var roles = roleManager.Roles.Where(s => s.Name == "Sub_Admin").FirstOrDefault();
                userTemp = userManager.Users.Where(s => s.Email == context.UserName && s.Roles.Where(j => j.RoleId == roles.Id).FirstOrDefault() != null).FirstOrDefault();
            }
            else if (LoginType.ToLower() == "loginpage")
            {
                userTemp = userManager.Users.Where(s => s.Email == context.UserName && s.InstituteID == intInstitute).FirstOrDefault();
            }
            else
            {
                userTemp = userManager.Users.Where(s => s.Email == context.UserName).FirstOrDefault();
            }

            if (userTemp == null)
            {
                context.SetError("invalid_grant", "The Email does not exist.");
                return;
            }

            ApplicationUser user = await userManager.FindAsync(userTemp.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "Password is incorrect.");
                return;
            }

            if (!user.EmailConfirmed)
            {
                context.SetError("invalid_grant", "User did not confirm email.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");

            oAuthIdentity.AddClaims(ExtendedClaimsProvider.GetClaims(user));

            oAuthIdentity.AddClaims(RolesFromClaims.CreateRolesBasedOnClaims(oAuthIdentity));

            InstituteContext db = new InstituteContext();
            CMN_INSTITUTE CMN_INSTITUTEMethod = db.CMN_INSTITUTE.Where(s => s.ID == user.InstituteID).FirstOrDefault();
            

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("id", user.Id);
            dictionary.Add("firstname", user.FirstName);
            dictionary.Add("lastname", user.LastName);
            dictionary.Add("email", user.Email);
            dictionary.Add("joinDate", user.JoinDate.ToString());
            dictionary.Add("instituteID", user.InstituteID.ToString());
            if (CMN_INSTITUTEMethod != null)
            {
                dictionary.Add("slug", CMN_INSTITUTEMethod.INSTITUTE_SLUG);
            }
            else
            {
                dictionary.Add("slug", "");
            }

            if (user.PhoneNumber == null)
            {
                dictionary.Add("phonenumber", "");
            }
            else
            {
                dictionary.Add("phonenumber", user.PhoneNumber.ToString());
            }

            string roleID = user.Roles.ToList()[0].RoleId;
            var roleSub = roleManager.Roles.Where(s => s.Id == roleID).FirstOrDefault();
            dictionary.Add("role", roleSub.Name);

            var props = new AuthenticationProperties(dictionary);

            var ticket = new AuthenticationTicket(oAuthIdentity, props);

            context.Validated(ticket);
        }

        public class InvalidAuthenticationMiddleware : OwinMiddleware
        {
            public InvalidAuthenticationMiddleware(OwinMiddleware next) : base(next) { }

            public override async Task Invoke(IOwinContext context)
            {
                context.Response.OnSendingHeaders(state =>
                {
                    var response = (OwinResponse)state;

                    if (!response.Headers.ContainsKey("AuthorizationResponse") && response.StatusCode != 400) return;

                    response.Headers.Remove("AuthorizationResponse");
                    response.StatusCode = 401;

                }, context.Response);

                await Next.Invoke(context);
            }
        }
    }
}