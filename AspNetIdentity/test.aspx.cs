﻿using AspNetIdentity.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspNetIdentity
{
    public partial class test : System.Web.UI.Page
    { 
        //Instance created for logger
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            string[] keys = Request.Form.AllKeys;
            string[] arr = Request.Form["request_id"].ToString().Split('_');

            text.InnerHtml = arr[0];

            AspNetIdentity.Infrastructure.TransactionContext db = new AspNetIdentity.Infrastructure.TransactionContext();

            try
            {
                int eyeDee = Convert.ToInt32(arr[0]);

                PAY_TRANSACTION tranObj = db.PAY_TRANSACTION.Where(s => s.ID == eyeDee).FirstOrDefault();

                tranObj.STATUS = "RECEIVED";
                tranObj.UPDATED_ON = DateTime.Now;
                db.Entry(tranObj).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
            }

            Response.Redirect(ConfigurationManager.AppSettings["mainlink"].ToString() + "geoswift/" + Request.Form["request_id"].ToString());
        }
    }
}